package net.novagamestudios.kaffeekasse

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.compose.runtime.Composable
import androidx.credentials.CredentialManager
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.runBlocking
import net.novagamestudios.common_utils.compose.application
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.common_utils.core.toastShort
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.HiwiTrackerAPI
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.HiwiTrackerScraper
import net.novagamestudios.kaffeekasse.api.kaffeekasse.KaffeekasseAPI
import net.novagamestudios.kaffeekasse.api.kaffeekasse.KaffeekasseScraper
import net.novagamestudios.kaffeekasse.api.portal.PortalClient
import net.novagamestudios.kaffeekasse.gitlab.GitLab
import net.novagamestudios.kaffeekasse.repositories.Credentials
import net.novagamestudios.kaffeekasse.repositories.LoginRepository
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.SettingsRepository
import net.novagamestudios.kaffeekasse.repositories.UpdateController
import net.novagamestudios.kaffeekasse.repositories.i11.HiwiTrackerRepository
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.repositories.i11.PortalRepository
import net.novagamestudios.kaffeekasse.repositories.newSettingsStore
import net.novagamestudios.kaffeekasse.repositories.releases.GitLabReleases
import net.novagamestudios.kaffeekasse.ui.util.derived


class App : Application(), RepositoryProvider, CoroutineScope by MainScope() + CoroutineName(App::class.simpleName!!), Logger {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        CrashHandling.init()
    }

    override fun onCreate() {
        super.onCreate()
        info { "App instance available" }
        runBlocking { loadInitialSettings() }
        launch { CrashHandling.syncInformation() }
        launch { tryAutoLoginDevice() }
        launch { fetchNewerReleases() }
    }

    private suspend fun loadInitialSettings() {
        try {
            settingsRepository.loadInitial()
        } catch (e: Throwable) {
            error(e) { "Failed to load initial settings" }
            toastShort("Failed to load settings: ${e.message}")
            throw e
        }
    }

    private suspend fun tryAutoLoginDevice() {
        try {
            loginRepository.tryAutoLoginDevice()
        } catch (e: Throwable) {
            warn(e) { "Failed to auto-login device" }
            toastShort("Failed to auto-login device: ${e.message}")
        }
    }

    private suspend fun fetchNewerReleases() {
        try {
            releases.fetchNewerReleases()
        } catch (e: Throwable) {
            warn(e) { "Failed to fetch newer releases" }
            toastShort("Failed to check for updates: ${e.message}")
        }
    }

    companion object {
        @Composable fun settings(): SettingsRepository = app().settingsRepository

        val developerMode: Boolean @Composable get() = settings().derived { developerMode }.value
    }


    // Settings stuff
    override val settingsRepository = SettingsRepository(
        repositoryProvider = this,
        settingsStore = newSettingsStore(this)
    )
    override val credentials by lazy {
        Credentials(
            credentialManager = CredentialManager.create(this),
            settingsStore = settingsRepository
        )
    }

    // Update stuff
    val gitLab = GitLab(
        instanceUrl = "https://git.rwth-aachen.de",
        projectPath = "jonas.broeckmann/kaffeekasse",
        projectId = 95637
    )
    override val releases = GitLabReleases(gitLab)
    override val updateController = UpdateController(this)

    // I11 stuff
    private val portalClient = PortalClient(this)
    override val kaffeekasseRepository = KaffeekasseRepository(
        coroutineScope = this,
        api = KaffeekasseAPI(portalClient),
        scraper = KaffeekasseScraper(portalClient)
    )
    override val hiwiTrackerRepository = HiwiTrackerRepository(
        coroutineScope = this,
        api = HiwiTrackerAPI(portalClient),
        scraper = HiwiTrackerScraper(portalClient)
    )
    override val portalRepository = PortalRepository(
        portalClient,
        kaffeekasseRepository,
        hiwiTrackerRepository
    )
    override val loginRepository = LoginRepository(
        credentialsRepository = credentials,
        settings = settingsRepository,
        portal = portalRepository,
        kaffeekasse = kaffeekasseRepository
    )

    // Active app modules
    override val modules = AppModules(
        KaffeekasseModule(),
        HiwiTrackerModule()
    )
}

val Activity.app get() = application as App

@Composable
fun app() = application<App>()

