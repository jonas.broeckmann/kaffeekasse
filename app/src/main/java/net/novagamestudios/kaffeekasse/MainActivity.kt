package net.novagamestudios.kaffeekasse

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.WindowInsets
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.CompositionLocalProvider
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.logging.LocalLogger
import net.novagamestudios.kaffeekasse.repositories.SettingsRepository
import net.novagamestudios.kaffeekasse.ui.App
import net.novagamestudios.kaffeekasse.ui.util.removeScrollableTabRowMinimumTabWidth

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        syncFullscreenWithSettings(app.settingsRepository)
        removeScrollableTabRowMinimumTabWidth()
        setContent {
            CompositionLocalProvider(LocalLogger provides app()) {
                App()
            }
        }
    }

    @SuppressLint("WrongConstant")
    private fun syncFullscreenWithSettings(settingsRepository: SettingsRepository): Job {
        val windowInsetsController = WindowCompat.getInsetsController(window, window.decorView)
        windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

        fun hideSystemUI() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                windowInsetsController.hide(WindowInsets.Type.systemBars())
            }
        }

        fun showSystemUI() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                windowInsetsController.show(WindowInsets.Type.systemBars())
            }
        }

        return lifecycleScope.launch {
            settingsRepository.values
                .map { it.fullscreen }
                .distinctUntilChanged()
                .collect { fullscreen ->
                    if (fullscreen) {
                        hideSystemUI()
                    } else {
                        showSystemUI()
                    }
                }
        }
    }
}
