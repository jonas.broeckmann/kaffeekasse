package net.novagamestudios.kaffeekasse.repositories

import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import kotlinx.coroutines.flow.StateFlow
import net.novagamestudios.common_utils.compose.state.MutableDataStoreState
import net.novagamestudios.common_utils.core.collection.mapState
import net.novagamestudios.kaffeekasse.model.session.realUserOrNull

class SettingsRepository(
    private val repositoryProvider: RepositoryProvider,
    private val settingsStore: MutableSettingsStore
) : MutableSettingsStore by settingsStore {
    private val userSettingsStores = mutableMapOf<String, UserSettingsStore>()
    val userSettings: State<MutableDataStoreState<UserSettings>?> = derivedStateOf {
        val session = repositoryProvider.portalRepository.session.value
        val key = session.realUserOrNull?.user ?: return@derivedStateOf null
        userSettingsStores.getOrPut(key) { UserSettingsStore(key, settingsStore) }
    }
}


private class UserSettingsStore(
    private val userKey: String,
    private val settingsStore: MutableSettingsStore
) : MutableDataStoreState<UserSettings> {
    private fun Settings.map() = userSettings.getOrElse(userKey) { UserSettings.Empty }
    private fun Updater<UserSettings>.wrapped(): Updater<Settings> = { settings ->
        val new = this(settings.map())
        if (new == UserSettings.Empty) {
            settings.copy(userSettings = settings.userSettings - userKey)
        } else {
            settings.copy(userSettings = settings.userSettings + (userKey to new))
        }
    }

    override val value: UserSettings get() = settingsStore.value.map()
    override val values: StateFlow<UserSettings> get() = settingsStore.values.mapState { settings -> settings.map() }

    override suspend fun loadInitial() = throw UnsupportedOperationException()
    override fun provideInitial(value: UserSettings) = throw UnsupportedOperationException()

    override fun tryUpdate(updater: suspend (UserSettings) -> UserSettings) = settingsStore.tryUpdate(updater.wrapped())
    override suspend fun update(updater: suspend (UserSettings) -> UserSettings) = settingsStore.update(updater.wrapped())
}

private typealias Updater<T> = suspend (T) -> T
