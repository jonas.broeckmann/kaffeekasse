package net.novagamestudios.kaffeekasse.repositories

import android.content.Context
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.sync.Mutex
import net.novagamestudios.common_utils.core.collection.mapState
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.verbose
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.common_utils.core.toastShort
import net.novagamestudios.common_utils.core.withReentrantLock
import net.novagamestudios.kaffeekasse.api.kaffeekasse.KaffeekasseAPI
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.BasicUserInfo
import net.novagamestudios.kaffeekasse.model.credentials.DeviceCredentials
import net.novagamestudios.kaffeekasse.model.credentials.Login
import net.novagamestudios.kaffeekasse.model.credentials.isValid
import net.novagamestudios.kaffeekasse.model.kaffeekasse.UserAuthCredentials
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.repositories.i11.PortalRepository

class LoginRepository(
    private val credentialsRepository: Credentials,
    private val settings: SettingsRepository,
    private val portal: PortalRepository,
    private val kaffeekasse: KaffeekasseRepository
) : Logger {

    private val mutex = Mutex()
    private val _isPerformingAction = MutableStateFlow(false)
    val isPerformingAction = _isPerformingAction.asStateFlow()

    private suspend fun <R> action(block: suspend () -> R) = mutex.withReentrantLock {
        val prev = _isPerformingAction.value
        verbose { "Locking action: ${!prev}" }
        _isPerformingAction.value = true
        try {
            block()
        } finally {
            verbose { "Unlocking action: ${!prev}" }
            _isPerformingAction.value = prev
        }
    }



    private val _errors = MutableStateFlow<List<Exception>?>(null)
    val errors = _errors.asStateFlow()


    suspend fun forceUpdateSession() = action {
        portal.forceUpdateSession()
    }


    val autoLogin by lazy { settings.values.mapState { it.autoLogin } }
    private var autoLoginAttemptAvailable = true

    private suspend fun performUserLogin(login: Login): Boolean = action {
        _errors.value = null
        info { "Logging in user: ${login.username}" }
        val success = try {
            portal.loginUser(login)
            debug { "User logged in" }
            true
        } catch (e: Exception) {
            warn(e) { "Failed to login" }
            _errors.value = listOf(e)
            false
        }
        autoLoginAttemptAvailable = success
        return@action success
    }

    suspend fun tryAutoLogin(activityContext: Context): Login? = action {
        if (portal.session.value !is Session.Empty) return@action null
        if (!autoLoginAttemptAvailable) return@action null

        debug { "Auto login enabled: ${autoLogin.value}" }
        if (!autoLogin.value) return@action null

        val sessionData = credentialsRepository.sessionData()
        debug { "Auto login session data: $sessionData" }
        if (sessionData != null) {
            portal.forceUpdateSession(sessionData)
            if (portal.session.value !is Session.Empty) return@action null
        }

        val login = with(activityContext) {
            credentialsRepository.userLogin()
                ?.takeIf { it.isValid }
                ?: run {
                    autoLoginAttemptAvailable = false
                    return@action null
                }
        }
        autoLoginAttemptAvailable = false
        info { "Trying to auto login user: ${login.username}" }
        performUserLogin(login)
        credentialsRepository.storeSessionData(portal.session.value.data)
        return@action login
    }

    suspend fun login(login: Login, autoLogin: Boolean, activityContext: Context) = action {
        val cleanedLogin = login.copy(username = login.username.trim())
        if (!performUserLogin(cleanedLogin)) return@action
        with(activityContext) {
            updateAutoLogin(cleanedLogin, autoLogin)
        }
    }


    context (Context)
    private suspend fun updateAutoLogin(login: Login, autoLogin: Boolean) {
        settings.update { it.copy(autoLogin = autoLogin) }
        if (autoLogin) {
            credentialsRepository.storeSessionData(portal.session.value.data)
            when (val result = credentialsRepository.storeUserLogin(login)) {
                Credentials.StoreResult.Success -> { }
                Credentials.StoreResult.Cancelled -> { }
                Credentials.StoreResult.Unsupported -> {
                    toastShort("Credential storage not supported")
                    settings.tryUpdate { it.copy(autoLogin = false) }
                }

                is Credentials.StoreResult.Error -> {
                    toastShort("Failed to store credentials: ${result.message}")
                }
            }
        }
    }








    suspend fun login(user: BasicUserInfo, auth: UserAuthCredentials = UserAuthCredentials.Empty): LoginResult = action {
        try {
            when (portal.loginUser(
                user.id,
                pin = auth.pin,
                rwthId = auth.rwthId,
                key = auth.key
            )) {
                KaffeekasseAPI.UserLoginResult.Failure.PrivateDevice -> LoginResult.Failure("Private device")
                is KaffeekasseAPI.UserLoginResult.Failure.UnknownError -> LoginResult.Failure("Unknown error")
                KaffeekasseAPI.UserLoginResult.Failure.UserAuthenticationFailure -> LoginResult.Failure("User authentication failure")
                is KaffeekasseAPI.UserLoginResult.LoggedIn -> LoginResult.Success(portal.session.value)
            }
        } catch (e: Exception) {
            warn(e) { "Failed to login user" }
            LoginResult.Failure(e.message ?: e::class.simpleName ?: "Unknown error")
        }
    }

    sealed interface LoginResult {
        data class Success(val session: Session) : LoginResult
        data class Failure(val error: String) : LoginResult
    }


    suspend fun logoutUser() = action {
        credentialsRepository.storeSessionData(null)
        portal.logoutUser()
    }



    private suspend fun performDeviceLogin(credentials: DeviceCredentials): Boolean = action {
        info { "Logging in device: ${credentials.deviceId}" }
        val success = kaffeekasse.loginDevice(credentials)
        debug { "Device logged in" }
        return@action success
    }

    suspend fun tryAutoLoginDevice(): Unit = action {
        if (portal.session.value !is Session.Empty) return@action
        info { "Trying to auto login device" }
        val credentials = credentialsRepository.deviceCredentials()
            ?.takeIf { it.isValid }
            ?: return@action
        performDeviceLogin(credentials)
    }

    suspend fun loginDevice(credentials: DeviceCredentials): Unit = action {
        if (!credentials.isValid) return@action
        val cleanedCredentials = credentials.copy(
            deviceId = credentials.deviceId.trim(),
            apiKey = credentials.apiKey.trim()
        )
        if (!performDeviceLogin(cleanedCredentials)) return@action
        credentialsRepository.storeDeviceCredentials(cleanedCredentials)
    }

    suspend fun logoutDevice(): Unit = action {
        credentialsRepository.storeDeviceCredentials(null)
        kaffeekasse.logoutDevice()
    }
}