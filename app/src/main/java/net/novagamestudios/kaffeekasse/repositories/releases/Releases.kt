package net.novagamestudios.kaffeekasse.repositories.releases

import kotlinx.coroutines.flow.MutableStateFlow
import net.novagamestudios.kaffeekasse.model.app.AppRelease

interface Releases {
    val newerReleases: MutableStateFlow<List<AppRelease>?>

    suspend fun fetchNewerReleases()
}
