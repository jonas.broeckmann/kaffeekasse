package net.novagamestudios.kaffeekasse.repositories

import android.content.Context
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.datastore.core.MultiProcessDataStoreFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.serializer
import net.novagamestudios.common_utils.compose.state.DataStoreState
import net.novagamestudios.common_utils.compose.state.MutableDataStoreState
import net.novagamestudios.common_utils.compose.state.stateIn
import net.novagamestudios.common_utils.compose.JsonToDataStore
import net.novagamestudios.kaffeekasse.model.credentials.DeviceCredentials
import net.novagamestudios.kaffeekasse.ui.util.derived
import java.io.File

@Serializable
data class Settings(
    val themeMode: ThemeMode = ThemeMode.Dark,
    val fullscreen: Boolean = false,
    val animationsEnabled: Boolean = true,
    val autoLogin: Boolean = false,
    val savedSessionData: String? = null,
    val deviceCredentials: DeviceCredentials? = null,
    val developerMode: Boolean = false,
    val userSettings: Map<String, UserSettings> = emptyMap()
) {

    enum class ThemeMode {
        Unspecified, Dark, Light
    }

    companion object {
        val State<Settings>.isDarkMode: Boolean @Composable get() = when(derived { themeMode }.value) {
            ThemeMode.Unspecified -> isSystemInDarkTheme()
            ThemeMode.Dark -> true
            ThemeMode.Light -> false
        }

        internal val serializersModule = SerializersModule { }
    }
}

@Serializable
data class UserSettings(
    val favoriteItemIds: List<Int> = emptyList(),
    val lastSelectedModule: String? = null
) {
    companion object {
        val Empty = UserSettings()
    }
}


typealias MutableSettingsStore = MutableDataStoreState<Settings>
typealias SettingsStore = DataStoreState<Settings>

@OptIn(ExperimentalSerializationApi::class)
fun Context.newSettingsStore(
    coroutineScope: CoroutineScope
): MutableSettingsStore = MultiProcessDataStoreFactory.create(
    serializer = JsonToDataStore(
        Settings(),
        serializer<Settings>(),
        Json {
            allowStructuredMapKeys = true
            ignoreUnknownKeys = true
            explicitNulls = false
            serializersModule = Settings.serializersModule
        }
    ),
    produceFile = {
        File("${this.filesDir.path}/kaffeekasse.settings")
    }
).stateIn(coroutineScope, settingsValidator)

private val settingsValidator: Settings.(Settings?) -> Settings = {
    this
}
