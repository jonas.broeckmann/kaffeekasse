package net.novagamestudios.kaffeekasse.repositories

import android.content.Context
import androidx.credentials.CreatePasswordRequest
import androidx.credentials.CredentialManager
import androidx.credentials.GetCredentialRequest
import androidx.credentials.GetPasswordOption
import androidx.credentials.PasswordCredential
import androidx.credentials.exceptions.CreateCredentialCancellationException
import androidx.credentials.exceptions.CreateCredentialException
import androidx.credentials.exceptions.CreateCredentialUnsupportedException
import androidx.credentials.exceptions.GetCredentialCancellationException
import androidx.credentials.exceptions.GetCredentialException
import net.novagamestudios.common_utils.core.toastShort
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.verbose
import net.novagamestudios.kaffeekasse.BuildConfig
import net.novagamestudios.kaffeekasse.model.credentials.DeviceCredentials
import net.novagamestudios.kaffeekasse.model.credentials.Login

class Credentials(
    private val credentialManager: CredentialManager,
    private val settingsStore: MutableSettingsStore
) : Logger {

    @Suppress("RedundantIf")
    private fun verifyDebug(): Boolean {
        if (!BuildConfig.DEBUG) return false
        return true
    }

    private fun getDebugUserLogin(): Login? {
        if (!verifyDebug()) return null
        if (!EnableDebugUserCredentials) return null
        verbose { "Searching for debug user credentials" }
        @Suppress("USELESS_ELVIS")
        val username = BuildConfig.I11_PORTAL_DEBUG_USERNAME ?: return null
        @Suppress("USELESS_ELVIS")
        val password = BuildConfig.I11_PORTAL_DEBUG_PASSWORD ?: return null
        info { "Using debug credentials for $username" }
        return Login(username, password)
    }

    private fun getDebugDeviceCredentials(): DeviceCredentials? {
        if (!verifyDebug()) return null
        if (!EnableDebugDeviceCredentials) return null
        verbose { "Searching for debug device credentials" }
        @Suppress("USELESS_ELVIS")
        val deviceId = BuildConfig.I11_KAFFEEKASSE_DEBUG_DEVICEID ?: return null
        @Suppress("USELESS_ELVIS")
        val apiKey = BuildConfig.I11_KAFFEEKASSE_DEBUG_APIKEY ?: return null
        info { "Using debug credentials for $deviceId" }
        return DeviceCredentials(deviceId, apiKey)
    }


    suspend fun sessionData(): String? {
        return settingsStore.value.savedSessionData
    }

    suspend fun storeSessionData(data: String?) {
        settingsStore.update { it.copy(savedSessionData = data) }
    }


    context (Context)
    suspend fun userLogin(): Login? {
        getDebugUserLogin()?.let { return it }
        val request = GetCredentialRequest(
            listOf(GetPasswordOption())
        )
        val response = try {
            credentialManager.getCredential(this@Context, request)
        } catch (e: GetCredentialCancellationException) {
            return null
        } catch (e: GetCredentialException) {
            error(e) { "Failed to get credentials" }
            toastShort("Failed to get credentials: ${e.message}")
            return null
        }
        val passwordCredential = response.credential as? PasswordCredential ?: return null
        debug { "Got credentials for ${passwordCredential.id}" }
        return Login(passwordCredential.id, passwordCredential.password)
    }

    context (Context)
    suspend fun storeUserLogin(login: Login): StoreResult {
        val passwordRequest = CreatePasswordRequest(login.username, login.password)
        val response = try {
            credentialManager.createCredential(this@Context, passwordRequest)
        } catch (e: CreateCredentialCancellationException) {
            return StoreResult.Cancelled
        } catch (e: CreateCredentialUnsupportedException) {
            error(e) { "Failed to store credentials (unsupported)" }
            return StoreResult.Unsupported
        } catch (e: CreateCredentialException) {
            error(e) { "Failed to store credentials" }
            return StoreResult.Error(e.message)
        }
        debug { "Stored credentials for ${login.username}: ${response.data}" }
        return StoreResult.Success
    }

    sealed interface StoreResult {
        data object Success : StoreResult
        data object Cancelled : StoreResult
        data object Unsupported : StoreResult
        data class Error(val message: String?) : StoreResult
    }


    suspend fun deviceCredentials(): DeviceCredentials? {
        getDebugDeviceCredentials()?.let { return it }
        return settingsStore.value.deviceCredentials
    }

    suspend fun storeDeviceCredentials(credentials: DeviceCredentials?) {
        settingsStore.update { it.copy(deviceCredentials = credentials) }
    }

    companion object {
        private const val EnableDebugUserCredentials = true
        private const val EnableDebugDeviceCredentials = true
    }
}