package net.novagamestudios.kaffeekasse.repositories

import net.novagamestudios.kaffeekasse.AppModules
import net.novagamestudios.kaffeekasse.repositories.i11.HiwiTrackerRepository
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.repositories.i11.PortalRepository
import net.novagamestudios.kaffeekasse.repositories.releases.GitLabReleases

interface RepositoryProvider {

    // Settings stuff
    val settingsRepository: SettingsRepository
    val credentials: Credentials

    // Update stuff
    val releases: GitLabReleases
    val updateController: UpdateController

    // I11 stuff
    val kaffeekasseRepository: KaffeekasseRepository
    val hiwiTrackerRepository: HiwiTrackerRepository
    val portalRepository: PortalRepository
    val loginRepository: LoginRepository

    // Active app modules
    val modules: AppModules

}
