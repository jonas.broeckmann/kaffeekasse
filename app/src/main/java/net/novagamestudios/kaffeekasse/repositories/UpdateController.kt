package net.novagamestudios.kaffeekasse.repositories

import android.app.Application
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageInstaller
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.prepareGet
import io.ktor.http.contentLength
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.jvm.javaio.copyTo
import kotlinx.coroutines.flow.MutableStateFlow
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.compose.components.Progress
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.kaffeekasse.MainActivity
import net.novagamestudios.kaffeekasse.UpdateReceiver
import net.novagamestudios.kaffeekasse.model.app.AppRelease
import java.io.File
import java.io.FileInputStream

class UpdateController(
    private val application: Application
) : Logger {
    private val client by lazy { HttpClient() }


    suspend fun updateApp(release: AppRelease) {
        info { "Updating to release ${release.version}" }
        installStatus.value = InstallStatus.InProgress.Installing(Progress.Indeterminate)
        val apk = File(application.cacheDir, "release-${release.version}.apk")
        if (!apk.exists()) try {
            release.downloadTo(apk)
        } catch (e: Throwable) {
            if (apk.exists()) apk.delete()
        }
        installApk(apk)
    }

    private suspend fun AppRelease.downloadTo(destination: File) {
        info { "Downloading $downloadUrl to $destination" }
        installStatus.value = InstallStatus.InProgress.Downloading(Progress.Indeterminate)
        if (destination.exists()) destination.delete()
        client.prepareGet(downloadUrl).execute {
            val channel = it.body<ByteReadChannel>()
            val total = it.contentLength()?.toInt()
            var done = 0L
            val bufferSize = (1024 * 100).toLong()
            destination.outputStream().use { output ->
                while (true) {
                    val new = channel.copyTo(output, bufferSize)
                    done += new
                    val progress = if(total == null) Progress.Indeterminate else Progress(done / total.toFloat())
                    installStatus.value = InstallStatus.InProgress.Downloading(progress)
                    if (new <= 0) break
                }
            }
        }
    }

    private fun installApk(apk: File) {
        info { "Installing $apk" }
        installStatus.value = InstallStatus.InProgress.Installing(Progress.Indeterminate)
        val packageName = application.applicationInfo.packageName
        val packageInstaller = application.packageManager.packageInstaller
        val sessionParams =
            PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL).apply {
                setAppPackageName(packageName)
            }
        val sessionId = packageInstaller.createSession(sessionParams)
        val session = packageInstaller.openSession(sessionId)
        session.addApk(
            packageName,
            apk,
            onProgress = { installStatus.value = InstallStatus.InProgress.Installing(Progress(it)) }
        )
        val intent = Intent(application, UpdateReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            application,
            sessionId,
            intent,
            PendingIntent.FLAG_MUTABLE
        )
        session.commit(pendingIntent.intentSender)
        session.close()
    }


    val installStatus = MutableStateFlow<InstallStatus?>(null)

    fun handleInstallStatus(intent: Intent) {
        val extras = intent.extras!!
        val status = extras.getInt(PackageInstaller.EXTRA_STATUS)

        debug { "Status: $status" }
        when (status) {
            PackageInstaller.STATUS_PENDING_USER_ACTION -> {
                val statusMessage = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                debug { "Pending user action: $statusMessage" }
                installStatus.value = InstallStatus.InProgress.PendingUserAction
                @Suppress("DEPRECATION")
                val confirmIntent = (extras[Intent.EXTRA_INTENT] as Intent).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                application.startActivity(confirmIntent)
            }
            PackageInstaller.STATUS_SUCCESS -> {
                debug { "Install succeeded" }
                installStatus.value = InstallStatus.Success
                // TODO fix auto-restart
                val serviceIntent = Intent(application, MainActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
                application.startActivity(serviceIntent)
            }
            PackageInstaller.STATUS_FAILURE_ABORTED -> {
                val failureReason = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                debug { "Install aborted. Possible permission problem?" }
                debug { "Failure reason: $failureReason" }
                installStatus.value = InstallStatus.Failure.Aborted(failureReason)
            }
            PackageInstaller.STATUS_FAILURE_BLOCKED -> {
                val failureReason = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                error { "Install failed: $failureReason" }
                installStatus.value = InstallStatus.Failure.Blocked(failureReason)
            }
            PackageInstaller.STATUS_FAILURE_CONFLICT -> {
                val failureReason = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                error { "Install failed: $failureReason" }
                installStatus.value = InstallStatus.Failure.Conflict(failureReason)
            }
            PackageInstaller.STATUS_FAILURE_INCOMPATIBLE -> {
                val failureReason = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                error { "Install failed: $failureReason" }
                installStatus.value = InstallStatus.Failure.Incompatible(failureReason)
            }
            PackageInstaller.STATUS_FAILURE_INVALID -> {
                val failureReason = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                error { "Install failed: $failureReason" }
                installStatus.value = InstallStatus.Failure.Invalid(failureReason)
            }
            PackageInstaller.STATUS_FAILURE_STORAGE -> {
                val failureReason = extras.getString(PackageInstaller.EXTRA_STATUS_MESSAGE)
                error { "Install failed: $failureReason" }
                installStatus.value = InstallStatus.Failure.Storage(failureReason)
            }
            else -> {
                error { "Install failed" }
                installStatus.value = InstallStatus.Failure.Generic
            }
        }
    }




    companion object {
        private fun PackageInstaller.Session.addApk(
            packageName: String,
            apk: File,
            // TODO
            @Suppress("UNUSED_PARAMETER") onProgress: (Float) -> Unit = {}
        ) {
            openWrite(packageName, 0, -1).use { output ->
                FileInputStream(apk).use { input ->
                    val buffer = ByteArray(16384)
                    while (true) {
                        val bytes = input.read(buffer)
                        if (bytes <= 0) break
                        output.write(buffer, 0, bytes)
                    }
                }
                fsync(output)
            }
        }
    }
}

sealed interface InstallStatus {
    sealed interface InProgress : InstallStatus {
        data class Installing(
            val progress: Progress
        ) : InProgress
        data class Downloading(
            val progress: Progress
        ) : InProgress
        data object PendingUserAction : InProgress
    }
    data object Success : InstallStatus
    sealed interface Failure : InstallStatus {
        val reason: String?
        data object Generic : Failure {
            override val reason: String? = null
        }
        data class Aborted(override val reason: String?) : Failure
        data class Blocked(override val reason: String?) : Failure
        data class Conflict(override val reason: String?) : Failure
        data class Incompatible(override val reason: String?) : Failure
        data class Invalid(override val reason: String?) : Failure
        data class Storage(override val reason: String?) : Failure
    }
}
