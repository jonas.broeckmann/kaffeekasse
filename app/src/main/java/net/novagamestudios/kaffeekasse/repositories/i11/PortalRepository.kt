package net.novagamestudios.kaffeekasse.repositories.i11

import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.kaffeekasse.api.kaffeekasse.KaffeekasseAPI
import net.novagamestudios.kaffeekasse.api.portal.PortalClient
import net.novagamestudios.kaffeekasse.model.credentials.Login
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.model.session.User
import net.novagamestudios.kaffeekasse.model.session.realUserOrNull


class PortalRepository(
    private val client: PortalClient,
    internal val kaffeekasse: KaffeekasseRepository,
    vararg otherModules: PortalRepositoryModule
) : PortalRepositoryModule(), Logger {
    private val modules = listOf(this, kaffeekasse, *otherModules)

    init {
        modules.forEach { it.portal = this }
    }

    public override val session: State<Session> = derivedStateOf {
        val session = client.session.value
        val device = kaffeekasse.currentDevice.value
        val response = session.response
        Session(
            device = device,
            user = if (response.isLoggedIn) User(response.username, response.displayName) else null,
            data = session.data
        )
    }

    suspend fun forceUpdateSession(restore: String? = null) {
        info { "Updating session" }
        client.forceUpdateSession(restore)
        debug { "Session updated" }
    }

    suspend fun loginUser(login: Login) {
        if (session.value is Session.WithRealUser) throw IllegalStateException("User already logged in")
        client.login(login)
        onLoggedInUserChanged()
    }

    suspend fun loginUser(
        userId: Int,
        pin: String? = null,
        rwthId: String? = null,
        key: String? = null
    ): KaffeekasseAPI.UserLoginResult {
        if (session.value is Session.WithRealUser) throw IllegalStateException("User already logged in")
        if (session.value !is Session.WithDevice) throw IllegalStateException("No permissions to login user")
        val result = kaffeekasse.api.performUserLogin(userId, pin, rwthId, key)
        info { "User login result: $result" }
        onLoggedInUserChanged()
        return result
    }

    suspend fun logoutUser() {
//        requireAnyLoggedInUser()
        if (session.value !is Session.WithRealUser) warn { "Trying to log out user without user session" }
        if (session.value is Session.WithDevice) kaffeekasse.api.logoutUser()
        else client.logoutAll()
        onLoggedInUserChanged()
    }

    override fun onLoggedInUserChanged() {
        modules.forEach { if (it != this) it.onLoggedInUserChanged() }
    }
}

abstract class PortalRepositoryModule internal constructor() {
    internal lateinit var portal: PortalRepository
    protected open val session get() = portal.session
    protected fun requireAnyLoggedInUser() = require(session.value is Session.WithRealUser) { "Logged in user required" }
    protected fun requireLoggedIn(user: User) = require(session.value.realUserOrNull == user) { "User \"${user.user}\" not logged in" }
    protected fun requireLoggedInDevice() = require(session.value is Session.WithDevice) { "Logged in device required" }
    internal abstract fun onLoggedInUserChanged()
}

