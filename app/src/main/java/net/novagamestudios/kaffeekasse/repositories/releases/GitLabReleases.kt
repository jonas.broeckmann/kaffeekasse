package net.novagamestudios.kaffeekasse.repositories.releases

import kotlinx.coroutines.flow.MutableStateFlow
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.kaffeekasse.gitlab.GitLab
import net.novagamestudios.kaffeekasse.model.app.AppRelease
import net.novagamestudios.kaffeekasse.model.app.AppVersion

class GitLabReleases(
    private val gitLab: GitLab
) : Releases, Logger {

    override val newerReleases: MutableStateFlow<List<AppRelease>?> = MutableStateFlow(null)

    override suspend fun fetchNewerReleases() {
        info { "Checking for newer releases ..." }
        newerReleases.value = gitLab.releases.list()
            .also { debug { "Found releases: $it" } }
            .asSequence()
            .filter { !it.upcomingRelease }
            .mapNotNull { release -> release.version?.let { it to release } }
            .filter { (version, _) -> version > AppVersion.Current && version.type == AppVersion.Type.Stable }
            .sortedByDescending { (version, _) -> version }
            .mapNotNull { (version, release) ->
                val links = release.assets.links
                val link = links.firstOrNull { it.name.endsWith(".apk") }
                    ?: links.firstOrNull()
                    ?: return@mapNotNull null
                AppRelease(version, link.url, release.createdAt, release.description)
            }
            .toList()
            .also { debug { "Found newer releases than ${AppVersion.Current}: $it" } }
    }
}


