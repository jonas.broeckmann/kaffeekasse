package net.novagamestudios.kaffeekasse.repositories.i11

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.kaffeekasse.api.kaffeekasse.KaffeekasseAPI
import net.novagamestudios.kaffeekasse.api.kaffeekasse.KaffeekasseScraper
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.APIPurchaseAccount
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.ExtendedUserInfo
import net.novagamestudios.kaffeekasse.data.category
import net.novagamestudios.kaffeekasse.data.cleanName
import net.novagamestudios.kaffeekasse.data.cleanProductName
import net.novagamestudios.kaffeekasse.data.cleanVariantName
import net.novagamestudios.kaffeekasse.data.drawableResource
import net.novagamestudios.kaffeekasse.model.credentials.DeviceCredentials
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Account
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Cart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Item
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemCategory
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemGroup
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItemGroup
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ManualBillDetails
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ScraperPurchaseAccount
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Stock
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Transaction
import net.novagamestudios.kaffeekasse.model.kaffeekasse.isEmpty
import net.novagamestudios.kaffeekasse.model.session.Device
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.model.session.User
import net.novagamestudios.kaffeekasse.util.richdata.KeyedMultiDataSource
import net.novagamestudios.kaffeekasse.util.richdata.RichData
import net.novagamestudios.kaffeekasse.util.richdata.RichDataSource
import net.novagamestudios.kaffeekasse.util.richdata.RichDataSource.Companion.RichDataSource
import net.novagamestudios.kaffeekasse.util.richdata.progress

class KaffeekasseRepository(
    coroutineScope: CoroutineScope,
    internal val api: KaffeekasseAPI,
    internal val scraper: KaffeekasseScraper
) : PortalRepositoryModule(), CoroutineScope by coroutineScope, Logger {
    private val client get() = api.client

    private val mutableCurrentDevice = mutableStateOf<Device?>(null)
    internal val currentDevice: State<Device?> get() = mutableCurrentDevice



    suspend fun loginDevice(credentials: DeviceCredentials): Boolean {
        val result = api.performDeviceLogin(credentials.deviceId.uppercase(), credentials.apiKey)
        if (result is KaffeekasseAPI.DeviceLoginResult.LoggedIn) {
            mutableCurrentDevice.value = Device(
                name = result.response.name,
                itemTypeId = result.response.itemTypeId
            )
            return true
        }
        return false
    }

    suspend fun logoutDevice() {
        if (session.value !is Session.WithDevice) return
        mutableCurrentDevice.value = null
        client.logoutAll()
    }


    val account: KeyedMultiDataSource<User, Account> = KeyedMultiDataSource { user ->
        requireLoggedIn(user)
        api.loggedInUser().mapToRichDataState {
            Account(
                firstName = balance.myBalance.firstName,
                lastName = balance.myBalance.lastName,
                total = balance.myBalance.total,
                paid = balance.myBalance.paid,
                deposited = balance.myBalance.deposited
            )
        }
    }

    val transactions: KeyedMultiDataSource<User, List<Transaction>> = KeyedMultiDataSource { user ->
        requireLoggedIn(user)
        scraper.transactions().let {
            RichData.Data(it)
        }
    }

    val manualBillAccounts: KeyedMultiDataSource<User, List<ManualBillDetails.PurchaseAccount>> = KeyedMultiDataSource { user ->
        requireLoggedIn(user)
        scraper.manualBillDetails().accounts.let {
            RichData.Data(it)
        }
    }



    val stock: RichDataSource<Stock> = RichDataSource {
        requireAnyLoggedInUser()
        progress(0f / 3f)

        val baseItemGroups = scraper.manualBillDetails().itemGroups
        progress(1f / 3f)

        if (session.value !is Session.WithDevice) return@RichDataSource RichData.Data(
            StockImpl(itemGroups = baseItemGroups)
        )

        val itemGroupsById: Map<Int?, MutableItemGroup> = baseItemGroups.map {
            MutableItemGroup(
                knownItemGroup = KnownItemGroup.byOriginalName[it.originalName],
                items = mutableListOf(),
                id = it.id,
                originalName = it.originalName
            )
        }.associateBy { it.id }

        api.itemList().mapToRichDataState {
            progress(2f / 3f)
            itemList.forEach { item ->
                val knownItems = KnownItem.byId[item.id]?.takeUnless { it.isEmpty() }
                if (knownItems == null) {
                    // Unknown item
                    itemGroupsById[item.itemTypeId]?.items?.add(
                        ItemImpl(
                            id = item.id,
                            originalName = item.originalName,
                            category = ItemCategory.Other,
                            cleanProductName = item.originalName,
                            cleanVariantName = null,
                            price = item.price,
                            estimatedPrice = item.price,
                            imageDrawable = null,
                            imageUrl = item.imageUrl
                        )
                    )
                } else knownItems.forEach { known ->
                    // Combine with known values
                    itemGroupsById[item.itemTypeId]?.items?.add(
                        ItemImpl(
                            id = item.id,
                            originalName = item.originalName,
                            category = known.category,
                            cleanProductName = known.cleanProductName,
                            cleanVariantName = known.cleanVariantName,
                            price = item.price,
                            estimatedPrice = item.price,
                            imageDrawable = known.drawableResource,
                            imageUrl = item.imageUrl
                        )
                    )
                }
            }
            progress(3f / 3f)
            StockImpl(
                itemGroups = itemGroupsById.values.toList()
            )
        }
    }



    val basicUserInfoList = RichDataSource {
        requireLoggedInDevice()
        api.userList().mapToRichDataState { userList }
    }

    private val extendedUserInfoById = mutableMapOf<Int, RichDataSource<ExtendedUserInfo>>()
    fun getExtendedUserInfo(userId: Int): RichDataSource<ExtendedUserInfo> = extendedUserInfoById.getOrPut(userId) {
        RichDataSource {
            requireLoggedInDevice()
            api.userInfo(userId).mapToRichDataState { this }
        }
    }


    suspend fun purchase(asUser: User, cart: Cart, account: ScraperPurchaseAccount) {
        if (cart.isEmpty()) return
        requireLoggedIn(asUser)
        scraper.submitCart(account, cart)
        markPurchaseDataDirty(asUser)
    }

    suspend fun purchase(asUser: User, cart: Cart, targetAccount: APIPurchaseAccount? = null) {
        if (cart.isEmpty()) return
        requireLoggedIn(asUser)
        requireLoggedInDevice()
        cart.forEach { (item, count) ->
            val result = api.purchase(
                itemId = item.id,
                count = count,
                targetUserId = targetAccount?.id
            ).mapToRichDataState { this }
            if (result is RichData.Error) throw IllegalStateException(result.messages.joinToString("\n"))
        }
        markPurchaseDataDirty(asUser)
    }


    private fun markPurchaseDataDirty(asUser: User) {
        stock.markDirty()
        account.getOrNull(asUser)?.markDirty()
        transactions.getOrNull(asUser)?.markDirty()
        manualBillAccounts.getOrNull(asUser)?.markDirty()
        extendedUserInfoById.values.forEach { it.markDirty() }
    }

    override fun onLoggedInUserChanged() {
//        extendedUserInfoById.values.forEach { it.markDirty() }
    }

    private inline fun <A, T : Any>  KaffeekasseAPI.Result<A>.mapToRichDataState(
        transform: A.() -> T
    ) : RichData<T> = when (this) {
        is KaffeekasseAPI.Result.Error -> KaffeekasseRichDataError(
            listOfNotNull(response.error?.string) + response.errors
        )
        is KaffeekasseAPI.Result.NotLoggedIn -> KaffeekasseRichDataError(listOf("Not logged in"))
        is KaffeekasseAPI.Result.Success -> RichData.Data(result.transform())
    }
}

data class KaffeekasseRichDataError<T : Any>(
    val errors: List<String>
) : RichData.Error<T> {
    override val messages: List<String> get() = errors
}


private data class ItemImpl(
    override val id: Int,
    override val originalName: String,
    override val category: ItemCategory,
    override val cleanProductName: String,
    override val cleanVariantName: String?,
    override val price: Double?,
    override val estimatedPrice: Double?,
    override val imageDrawable: Int?,
    override val imageUrl: String?
) : Item


private class MutableItemGroup(
    knownItemGroup: KnownItemGroup?,
    override val items: MutableList<Item>,
    id: Int? = null,
    originalName: String? = null
) : ItemGroup {
    override val id: Int? = knownItemGroup?.id ?: id
    override val originalName: String? = knownItemGroup?.originalName ?: originalName
    override val name: String = knownItemGroup?.cleanName ?: this.originalName ?: "$id"
}

private data class StockImpl(
    override val itemGroups: List<ItemGroup>
) : Stock


