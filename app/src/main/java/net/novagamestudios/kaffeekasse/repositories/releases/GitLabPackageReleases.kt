package net.novagamestudios.kaffeekasse.repositories.releases

import kotlinx.coroutines.flow.MutableStateFlow
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.kaffeekasse.gitlab.GitLab
import net.novagamestudios.kaffeekasse.gitlab.GitLabPackage
import net.novagamestudios.kaffeekasse.gitlab.GitLabPackageFile
import net.novagamestudios.kaffeekasse.model.app.AppRelease
import net.novagamestudios.kaffeekasse.model.app.AppVersion


class GitLabPackageReleases(
    private val gitLab: GitLab
) : Logger, Releases {

    override val newerReleases = MutableStateFlow<List<AppRelease>?>(null)

    override suspend fun fetchNewerReleases() {
        info { "Checking for newer releases ..." }
        newerReleases.value = gitLab.packages.list()
            .also { debug { "Found packages: $it" } }
            .filter { it.version > AppVersion.Current }
            .sortedByDescending { it.version }
            .mapNotNull { pkg ->
                val file = gitLab.packages.packageFiles(pkg.id)
                    .also { debug { "Found package files for ${pkg.id}: $it" } }
                    .firstOrNull { it.filename.endsWith(".apk") }
                    ?: return@mapNotNull null
                AppRelease(gitLab, pkg, file)
            }
            .also { debug { "Found newer releases: $it" } }
    }

    companion object {
        private fun AppRelease(gitLab: GitLab, pkg: GitLabPackage, file: GitLabPackageFile) = AppRelease(
            version = pkg.version,
            downloadUrl = file.downloadUrl("${gitLab.instanceUrl}/${pkg.links.projectPath}"),
            createdAt = pkg.createdAt
        )
    }
}
