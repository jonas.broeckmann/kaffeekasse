package net.novagamestudios.kaffeekasse.repositories.i11

import kotlinx.coroutines.CoroutineScope
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.HiwiTrackerAPI
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.HiwiTrackerScraper
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.model.MonthDataResponse
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.MonthKey
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.MonthKey.Companion.toMonthKey
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.isValid
import net.novagamestudios.kaffeekasse.util.richdata.RichData
import net.novagamestudios.kaffeekasse.util.richdata.RichDataSource
import net.novagamestudios.kaffeekasse.util.richdata.RichDataSource.Companion.RichDataSource

class HiwiTrackerRepository(
    coroutineScope: CoroutineScope,
    private val api: HiwiTrackerAPI,
    private val scraper: HiwiTrackerScraper
) : PortalRepositoryModule(), CoroutineScope by coroutineScope, Logger {


    private val dataByMonth = mutableMapOf<MonthKey, RichDataSource<MonthDataResponse>>()


    fun getDataForMonth(month: MonthKey) = dataByMonth.getOrPut(month) {
        RichDataSource {
            debug { "Fetching data for month $month" }
            val data = api.fetchDataForMonth(month)
            RichData.Data(data)
        }
    }

    override fun onLoggedInUserChanged() {
        dataByMonth.clear()
    }

    suspend fun submitWorkEntry(workEntry: WorkEntry) {
        if (!workEntry.isValid) return
        scraper.submitWorkEntry(workEntry)
        markFutureMonthsDirty(workEntry.date.toMonthKey())
    }

    suspend fun deleteWorkEntry(workEntryId: Int) {
        scraper.deleteWorkEntry(workEntryId)
        dataByMonth.values.forEach { it.markDirty() }
    }

    private fun markFutureMonthsDirty(month: MonthKey) {
        dataByMonth.entries.forEach { (key, data) ->
            if (key >= month) data.markDirty()
        }
    }
}