package net.novagamestudios.kaffeekasse.ui.kaffeekasse

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Receipt
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRowDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.compose.components.CircularProgressIndicator
import net.novagamestudios.common_utils.compose.components.RowCenter
import net.novagamestudios.common_utils.compose.tabIndicatorOffset
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.KaffeekasseModule.Companion.kaffeekasseCartProvider
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Item
import net.novagamestudios.kaffeekasse.model.kaffeekasse.MutableCart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.isEmpty
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.model.session.deviceOrNull
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.ui.AppModuleSelection
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.ManualBillScreenModel.Companion.backNavigationHandler
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.CategorizedItems
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.CategorizedItemsState
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.Checkout
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.CheckoutState
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.CustomItems
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.CustomItemsState
import net.novagamestudios.kaffeekasse.ui.login.LogoutTopBarAction
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.AppSubpageTitle
import net.novagamestudios.kaffeekasse.ui.navigation.KaffeekasseNavigation
import net.novagamestudios.kaffeekasse.ui.util.FailureRetryScreen
import net.novagamestudios.kaffeekasse.ui.util.RichDataContent
import net.novagamestudios.kaffeekasse.ui.util.TopBarSearchAction
import net.novagamestudios.kaffeekasse.ui.util.TopBarSearchField
import net.novagamestudios.kaffeekasse.ui.util.TopBarSearchFieldState
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.common_utils.voyager.BackNavigationHandler.Companion.then
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichStateHere


class ManualBillScreenModel private constructor(
    repositoryProvider: RepositoryProvider,
    val session: Session.WithRealUser,
    private val kaffeekasse: KaffeekasseRepository,
    val cart: MutableCart
) : ScreenModel, Logger {
    val accountName = when (session) {
        is Session.WithDevice -> session.realUser.displayName ?: "Unknown User"
        else -> null
    }

//    private var _showSearch by mutableStateOf(false)
//    var showSearch
//        get() = _showSearch
//        set(value) {
//            _showSearch = value
//            if (!value) searchQuery = ""
//        }
//    var searchQuery by mutableStateOf("")
    val searchState = TopBarSearchFieldState()

    val stock = kaffeekasse.stock.collectAsRichStateHere()

    val itemGroups get() = stock.dataOrNull?.itemGroups ?: emptyList()

    fun Sequence<Item>.filtered(): Sequence<Item> {
        val query = searchState.queryOrNull
        return if (query != null) filter { item ->
            item.cleanFullName.contains(query, ignoreCase = true)
        } else this
    }

    val searchItems by derivedStateOf {
        if (searchState.show) CategorizedItemsState(
            itemGroups
                .asSequence()
                .flatMap { it.items }
                .filtered()
                .toList()
        ) else null
    }

    val categorizedItemsByGroup by derivedStateOf {
        itemGroups.map { group -> CategorizedItemsState(group.items.asSequence().filtered().toList()) }
    }

    val customItemsState by lazy {
        with(repositoryProvider) {
            CustomItemsState.create(
                user = session.realUser,
                computationScope = screenModelScope
            )
        }
    }

    val checkoutState by derivedStateOf {
        with(repositoryProvider) {
            CheckoutState.create(
                session = session,
                coroutineScope = screenModelScope,
                onSubmitted = { categorizedItemsByGroup.forEach { it.reset() } }
            )
        }
    }

    var currentPagerIndex by mutableStateOf(0)
    val pagerState = object : PagerState(currentPagerIndex) {
        override val pageCount: Int get() = itemGroups.size + 1
    }
    var currentGroupIndex
        get() = currentPagerIndex - 1
        set(value) { currentPagerIndex = value + 1 }

    @Suppress("unused", "RedundantSuspendModifier")
    suspend fun autoScrollToDeviceItemGroup() {
        val initialItemGroupIndex = session.deviceOrNull?.itemTypeId
            ?.let { id -> itemGroups.indexOfFirst { it.id == id }.takeIf { it >= 0 } }
        if (initialItemGroupIndex != null) {
            currentGroupIndex = initialItemGroupIndex
//            pagerState.scrollToPage(currentPagerIndex)
        }
    }

    companion object : ScreenModelFactory<RepositoryProvider, KaffeekasseNavigation.ManualBillScreen, ManualBillScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: KaffeekasseNavigation.ManualBillScreen) = ManualBillScreenModel(
            repositoryProvider = this@RepositoryProvider,
            session = screen.session,
            kaffeekasse = kaffeekasseRepository,
            cart = kaffeekasseCartProvider[screen.session.realUser]
        )

        @Composable
        internal fun ManualBillScreenModel.backNavigationHandler(): BackNavigationHandler {
            val categorizedItems = if (currentPagerIndex == 0) {
                searchItems
            } else {
                categorizedItemsByGroup.getOrNull(currentGroupIndex)
            }
            return remember(categorizedItems, cart) {
                categorizedItems?.backNavigation then searchState.backNavigation then CartBackNavigationHandler(cart)
            }
        }
    }


    private class CartBackNavigationHandler(
        private val cart: MutableCart
    ) : BackNavigationHandler {
        override fun canNavigateBack() = false
        override fun onNavigateBack(): Boolean {
            if (cart.isEmpty()) return false
            cart.clear()
            return true
        }
    }
}


class ManualBillContent(provider: ScreenModelProvider<ManualBillScreenModel>) : AppScaffoldContentWithModel<ManualBillScreenModel>(provider) {
    @Composable
    override fun TopAppBarTitle(navigator: Navigator) {
        RowCenter {
            if (!model.searchState.show) {
                val name = model.accountName
                if (name != null) AppSubpageTitle("$name")
                else AppModuleSelection()
            }
            TopBarSearchField(model.searchState)
        }
    }

    @Composable
    override fun TopAppBarActions(navigator: Navigator) {
        TopBarSearchAction(model.searchState)
        AccountButton(model, navigator)
        TransactionsButton(model, navigator)
        LogoutTopBarAction(model.session)
    }

    @Composable
    override fun backNavigationHandler(navigator: Navigator): BackNavigationHandler {
        return model.backNavigationHandler()
    }
}


@Composable
fun DynamicManualBill(
    model: ManualBillScreenModel,
    modifier: Modifier = Modifier,
    postTabsContent: (@Composable () -> Unit)? = null
) = Box(modifier.fillMaxSize()) {
    RichDataContent(
        state = model.stock,
        errorContent = { error ->
            FailureRetryScreen(
                error = error,
                message = "Failed to fetch details",
                Modifier.fillMaxSize()
            )
        },
        loadingContent = { progress ->
            CircularProgressIndicator(progress, Modifier.align(Alignment.Center))
        }
    ) {
        TabbedItemGroups(
            model = model,
            postTabsContent = postTabsContent
        )
        Checkout(
            state = model.checkoutState,
            Modifier.align(Alignment.BottomEnd)
        )
    }
}

@Composable
private fun TabbedItemGroups(
    model: ManualBillScreenModel,
    modifier: Modifier = Modifier,
    postTabsContent: (@Composable () -> Unit)? = null
) = Column(modifier.fillMaxSize()) {
    key(Unit) {
        LaunchedEffect(model.currentPagerIndex) {
            model.pagerState.animateScrollToPage(model.currentPagerIndex)
        }
        LaunchedEffect(model.pagerState.targetPage) {
            model.currentPagerIndex = model.pagerState.targetPage
        }
//        LaunchedEffect(vm.stock != null) {
//            if (vm.stock != null) vm.autoScrollToDeviceItemGroup()
//        }
    }
    Box {
        ScrollableTabRow(
            selectedTabIndex = model.currentPagerIndex,
            if (postTabsContent != null) Modifier.padding(end = 48.dp) else Modifier,
            edgePadding = 16.dp,
            indicator = { TabRowDefaults.PrimaryIndicator(Modifier.tabIndicatorOffset(model.pagerState, it)) },
            divider = { }
        ) {
            Tab(
                selected = model.currentPagerIndex == 0,
                onClick = { model.currentPagerIndex = 0 },
                Modifier
                    .clip(RoundedCornerShape(8.dp))
                    .width(48.dp),
            ) {
                Icon(Icons.Default.Favorite, "Favoriten")
            }
            model.itemGroups.forEachIndexed { index, itemGroup ->
                Tab(
                    selected = model.currentGroupIndex == index,
                    onClick = {
                        if (model.currentGroupIndex == index) {
                            model.categorizedItemsByGroup[index].reset()
                        } else {
                            model.currentGroupIndex = index
                        }
                    },
                    Modifier.clip(RoundedCornerShape(8.dp)),
                    text = { Text(itemGroup.name) }
                )
            }
        }
        if (postTabsContent != null) Box(
            Modifier
                .requiredSize(48.dp)
                .align(Alignment.CenterEnd)
        ) {
            postTabsContent()
        }
    }
    HorizontalDivider()
    HorizontalPager(
        model.pagerState,
        beyondViewportPageCount = 4
    ) { index ->
        if (index == 0) CustomItemsTab(
            model = model,
            Modifier.fillMaxHeight()
        ) else ItemGroupTab(
            model = model,
            groupIndex = index - 1,
            Modifier.fillMaxHeight()
        )
    }
}


@Composable
private fun CustomItemsTab(
    model: ManualBillScreenModel,
    modifier: Modifier = Modifier
) {
    val state = model.searchItems
    if (state != null) {
        CategorizedItems(
            state = state,
            cart = model.cart,
            modifier
        )
    } else CustomItems(
        state = model.customItemsState,
        cart = model.cart,
        modifier
    )
}

@Composable
private fun ItemGroupTab(
    model: ManualBillScreenModel,
    groupIndex: Int,
    modifier: Modifier = Modifier
) {
    CategorizedItems(
        state = model.categorizedItemsByGroup[groupIndex],
        cart = model.cart,
        modifier
    )
}


@Composable
private fun AccountButton(
    model: ManualBillScreenModel,
    navigator: Navigator,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = { navigator.push(KaffeekasseNavigation.AccountScreen(model.session)) }, modifier) {
        Icon(Icons.Default.Person, "Konto")
    }
}

@Composable
private fun TransactionsButton(
    model: ManualBillScreenModel,
    navigator: Navigator,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = { navigator.push(KaffeekasseNavigation.TransactionsScreen(model.session)) }, modifier) {
        Icon(Icons.Default.Receipt, "Übersicht")
    }
}



