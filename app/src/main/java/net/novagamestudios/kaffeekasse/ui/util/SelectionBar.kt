package net.novagamestudios.kaffeekasse.ui.util

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.detectVerticalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import net.novagamestudios.kaffeekasse.ui.theme.disabled

@Composable
fun VerticalSelectionBar(
    itemCount: Int,
    onSelect: (Int) -> Unit,
    modifier: Modifier = Modifier,
    itemContent: @Composable (Int) -> Unit
) = Column(
    modifier
        .fillMaxHeight()
        .verticalSelectionBarModifier(
            itemCount = itemCount,
            onSelect = onSelect
        ),
    verticalArrangement = Arrangement.SpaceAround,
    horizontalAlignment = Alignment.CenterHorizontally
) {
    for (item in 0 until itemCount) key(item) {
        itemContent(item)
    }
}

@Composable
fun HorizontalSelectionBar(
    itemCount: Int,
    onSelect: (Int) -> Unit,
    modifier: Modifier = Modifier,
    itemContent: @Composable (Int) -> Unit
) = Row(
    modifier
        .fillMaxWidth()
        .horizontalSelectionBarModifier(
            itemCount = itemCount,
            onSelect = onSelect
        ),
    horizontalArrangement = Arrangement.SpaceAround,
    verticalAlignment = Alignment.CenterVertically
) {
    for (item in 0 until itemCount) key(item) {
        itemContent(item)
    }
}

private fun Modifier.verticalSelectionBarModifier(
    itemCount: Int,
    onSelect: (Int) -> Unit
): Modifier {
    fun selectAtRatio(ratio: Float) = onSelect((ratio * itemCount).toInt())
    return this
        .pointerInput(Unit) {
            detectTapGestures { offset ->
                selectAtRatio(offset.y / size.height)
            }
        }
        .pointerInput(Unit) {
            detectVerticalDragGestures { change, _ ->
                selectAtRatio(change.position.y / size.height)
            }
        }
        .padding(4.dp)
}

private fun Modifier.horizontalSelectionBarModifier(
    itemCount: Int,
    onSelect: (Int) -> Unit
): Modifier {
    fun selectAtRatio(ratio: Float) = onSelect((ratio * itemCount).toInt())
    return this
        .pointerInput(Unit) {
            detectTapGestures { offset ->
                selectAtRatio(offset.x / size.width)
            }
        }
        .pointerInput(Unit) {
            detectHorizontalDragGestures { change, _ ->
                selectAtRatio(change.position.x / size.width)
            }
        }
        .padding(4.dp)
}

@Composable
fun AlphabetSelectionChar(
    char: Char,
    modifier: Modifier = Modifier,
    highlighted: Boolean = false,
    enabled: Boolean = true,
) {
    val scale by animateFloatAsState(
        if (highlighted) 1f else 0.7f,
        label = "CharScaleAnimation"
    )
    Text(
        "$char",
        modifier.graphicsLayer {
            scaleX = scale
            scaleY = scale
        },
        color = when {
            enabled && highlighted -> MaterialTheme.colorScheme.primary
            enabled -> LocalContentColor.current
            else -> LocalContentColor.current.disabled()
        },
        fontSize = 24.sp,
        fontWeight = if (highlighted) FontWeight.Bold else FontWeight.Normal
    )
}