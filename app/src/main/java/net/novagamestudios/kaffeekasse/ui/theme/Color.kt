package net.novagamestudios.kaffeekasse.ui.theme

import androidx.compose.ui.graphics.Color


val AppYellow = Color(0xFFFBD302)
val AppBlue = Color(0xff009ffd)
val AppDarkBlue = Color(0xff07263e)

