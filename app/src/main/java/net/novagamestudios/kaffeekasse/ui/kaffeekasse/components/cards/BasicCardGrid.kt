package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyGridItemScope
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.woong.compose.grid.SimpleGridCells
import io.woong.compose.grid.VerticalGrid

@Composable
inline fun <T> LazyBasicCardGrid(
    items: List<T>,
    modifier: Modifier = Modifier,
    scrollable: Boolean = true,
    spacerForFAB: Boolean = false,
    crossinline itemContent: @Composable LazyGridItemScope.(T) -> Unit
) = LazyVerticalGrid(
    columns = GridCells.Adaptive(170.dp),
    modifier,
    contentPadding = PaddingValues(8.dp),
    horizontalArrangement = Arrangement.spacedBy(8.dp),
    verticalArrangement = Arrangement.spacedBy(8.dp),
    userScrollEnabled = scrollable
) {
    items(items, itemContent = itemContent)
    // Spacer for the FAB to not obstruct the last item
    if (spacerForFAB) item { Spacer(Modifier.height(56.dp + 16.dp)) }
}

@Composable
inline fun <T> BasicCardGrid(
    items: List<T>,
    modifier: Modifier = Modifier,
    spacerForFAB: Boolean = false,
    crossinline itemContent: @Composable (T) -> Unit
) = VerticalGrid(
    columns = SimpleGridCells.Adaptive(170.dp),
    modifier
        .padding(8.dp)
        .fillMaxWidth(),
    horizontalArrangement = Arrangement.spacedBy(8.dp),
    verticalArrangement = Arrangement.spacedBy(8.dp)
) {
    items.forEach { itemContent(it) }
    // Spacer for the FAB to not obstruct the last item
    if (spacerForFAB) Spacer(Modifier.height(56.dp + 16.dp))
}