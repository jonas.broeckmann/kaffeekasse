package net.novagamestudios.kaffeekasse.ui.util

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.SearchBarDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import net.novagamestudios.common_utils.voyager.BackNavigationHandler


class TopBarSearchFieldState {
    private var _show by mutableStateOf(false)
    var show
        get() = _show
        set(value) {
            _show = value
            if (!value) query = ""
        }
    var query by mutableStateOf("")
    val queryOrBlank get() = query.takeIf { show } ?: ""
    val queryOrNull get() = query.takeIf { show }?.takeUnless { it.isBlank() }

    val backNavigation by lazy {
        object : BackNavigationHandler {
            override fun canNavigateBack() = show
            override fun onNavigateBack(): Boolean {
                if (show) {
                    show = false
                    return true
                }
                return false
            }
        }
    }
}


@Composable
fun TopBarSearchField(
    state: TopBarSearchFieldState,
    modifier: Modifier = Modifier,
    onSearch: () -> Unit = { }
) {
    if (state.show) {
        val focusRequester = remember { FocusRequester() }
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
        SearchBarDefaults.InputField(
            query = state.query,
            onQueryChange = { state.query = it },
            onSearch = { onSearch() },
            expanded = false,
            onExpandedChange = { },
            modifier = Modifier.focusRequester(focusRequester),
            placeholder = { Text("Suchen") },
            trailingIcon = {
                if (state.query.isNotEmpty()) IconButton(onClick = { state.query = "" }) {
                    Icon(Icons.Default.Clear, "Clear")
                }
            }
        )
    }
}


@Composable
fun TopBarSearchAction(
    state: TopBarSearchFieldState,
    modifier: Modifier = Modifier,
    alwaysShow: Boolean = false
) {
    if (alwaysShow || !state.show) IconButton(
        onClick = { state.show = !state.show },
        modifier
    ) {
        Icon(Icons.Default.Search, "Suchen")
    }
}

