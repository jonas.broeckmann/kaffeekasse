package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Image
import androidx.compose.material.icons.filled.Remove
import androidx.compose.material.icons.rounded.Warning
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.SubcomposeAsyncImage
import coil.request.ImageRequest
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.compose.logging.LocalLogger
import net.novagamestudios.common_utils.compose.maskedCircleIcon
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.app
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Item
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Transaction
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.ui.theme.ifAnimationsEnabled
import net.novagamestudios.kaffeekasse.ui.util.derived
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichState

@Composable
fun ItemCard(
    item: Item,
    highlighted: Boolean,
    count: Int,
    onClick: () -> Unit,
    onAdd: () -> Unit,
    onRemove: () -> Unit,
    modifier: Modifier = Modifier,
    onLongClick: (() -> Unit)? = null
) = BasicCard(
    modifier
        .clip(CardDefaults.elevatedShape)
        .combinedClickable(
            onLongClick = onLongClick,
            onClick = onClick
        ),
    highlighted = highlighted
) {
    BoxCenter(
        Modifier
            .weight(1f)
            .fillMaxWidth()
    ) {
        ItemImage(
            item,
            Modifier
                .matchParentSize()
                .padding(8.dp)
                .aspectRatio(1f, matchHeightConstraintsFirst = true)
                .clip(RoundedCornerShape(8.dp))
        )
    }
    ItemTitle(item)
    ItemInformation(item)

    fun <T> buttonsAnimationSpec() = spring<T>(stiffness = Spring.StiffnessMedium)
    AnimatedVisibility(
        visible = count > 0,
        Modifier.align(Alignment.CenterHorizontally),
        enter = (fadeIn(buttonsAnimationSpec()) + expandVertically(buttonsAnimationSpec())).ifAnimationsEnabled(),
        exit = (fadeOut(buttonsAnimationSpec()) + shrinkVertically(buttonsAnimationSpec())).ifAnimationsEnabled()
    ) {
        ItemAmountControls(
            count = count,
            onAdd = onAdd,
            onRemove = onRemove
        )
    }
    if (App.developerMode) ProvideTextStyle(MaterialTheme.typography.labelSmall) {
        Row {
            Text("${item.id}")
            Spacer(Modifier.weight(1f))
            Text(item.originalName, textAlign = TextAlign.End)
        }
    }
}

@Composable
private fun ItemTitle(
    item: Item,
    modifier: Modifier = Modifier
) = Text(
    item.cleanProductName,
    modifier,
    style = MaterialTheme.typography.titleMedium
)

@Composable
private fun ItemInformation(
    item: Item,
    modifier: Modifier = Modifier
) = Row(modifier.heightIn(min = 20.dp)) {
    item.cleanVariantName?.let {
        Text(
            it,
            style = MaterialTheme.typography.bodyMedium,
        )
    }
    Spacer(Modifier.weight(1f))

    val exactPrice = item.price
    if (exactPrice != null) ItemPrice(
        price = exactPrice,
        estimated = false,
        highlighted = false
    ) else EstimatedItemPrice(item)
}


@Composable
private fun ItemPrice(
    price: Double,
    estimated: Boolean,
    highlighted: Boolean,
    modifier: Modifier = Modifier
) = Text(
    remember(price, estimated) {
        listOfNotNull(
            if (estimated) "≈" else null,
            String.format("%.2f€", price)
        ).joinToString(" ")
    },
    modifier,
    color = if (highlighted) Color.Yellow else Color.Unspecified,
    style = MaterialTheme.typography.bodyMedium
)

@Composable
private fun EstimatedItemPrice(
    item: Item,
    modifier: Modifier = Modifier
) {
    // TODO Bad: Don't do logic in UI
    val app = app()
    val user = app.portalRepository.session.derived { (this as? Session.WithRealUser)?.realUser }.value
    if (user == null) return
    val transactionsState = remember { app.kaffeekasseRepository.transactions[user] }.collectAsRichState()
    val lastUnitPrice by remember(transactionsState) {
        derivedStateOf {
            transactionsState.dataOrNull?.findLastUnitPrice(item)
        }
    }
    val estimatedPrice = lastUnitPrice ?: item.estimatedPrice ?: return
    val priceDiverges = lastUnitPrice != item.estimatedPrice
    ItemPrice(
        price = estimatedPrice,
        estimated = true,
        highlighted = priceDiverges && App.developerMode,
        modifier = modifier
    )
}


// TODO move
private fun List<Transaction>.findLastUnitPrice(item: Item): Double? {
    val lastPurchase = this
        .asSequence()
        .take(100)
        .map { it.purpose }
        .filterIsInstance<Transaction.Purpose.Purchase>()
        .firstOrNull {
            val knownId = it.knownId
            if (knownId != null) knownId == item.id else it.itemName == item.originalName
        }
    return lastPurchase?.unitPrice
}

@Composable
fun ItemImage(
    item: Item,
    modifier: Modifier = Modifier
) {
    // Special case for milk
    if (item.id == KnownItem.MilkMachine.id) {
        Box(modifier.background(Color.White))
        return
    }
    // Known items with images
    remember(item) { item.imageDrawable }?.let {
        Image(
            painterResource(it),
            item.cleanFullName,
            modifier,
            contentScale = ContentScale.Fit,
        )
        return
    }
    // Custom images from API
    item.imageUrl?.let {
        val logger = LocalLogger.current
        SubcomposeAsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .listener(
                    onStart = { logger.info { "Loading image for $item" } },
                    onError = { _, error -> logger.warn(error.throwable) { "Failed to load image for $item" } }
                )
                .data(remember(it) {
                    URLBuilder(it).apply {
                        if (protocol == URLProtocol.HTTP) protocol = URLProtocol.HTTPS
                    }.build().toString()
                })
                .crossfade(true)
                .build(),
            contentDescription = item.cleanFullName,
            modifier,
            loading = {
                BoxCenter {
                    CircularProgressIndicator()
                }
            },
            error = {
                Image(
                    Icons.Rounded.Warning,
                    item.cleanFullName,
                    modifier,
                    contentScale = ContentScale.Fit,
                    colorFilter = ColorFilter.tint(LocalContentColor.current.copy(alpha = 0.1f))
                )
            },
            contentScale = ContentScale.Fit
        )
        return
    }
    // Default image
    Image(
        Icons.Default.Image,
        item.cleanFullName,
        modifier,
        contentScale = ContentScale.Fit,
        colorFilter = ColorFilter.tint(LocalContentColor.current.copy(alpha = 0.1f))
    )
}

@Composable
private fun ItemAmountControls(
    count: Int,
    onAdd: () -> Unit,
    onRemove: () -> Unit,
    modifier: Modifier = Modifier
) = Row(
    modifier,
    horizontalArrangement = Arrangement.spacedBy(8.dp),
    verticalAlignment = Alignment.CenterVertically
) {
    IconButton(onClick = onRemove, enabled = count > 0) {
        Icon(Icons.Default.Remove, "Remove")
    }
    BoxCenter(Modifier.maskedCircleIcon(LocalContentColor.current)) {
        Text("$count", fontWeight = FontWeight.Bold)
    }
    IconButton(onClick = onAdd) {
        Icon(Icons.Default.Add, "Add")
    }
}