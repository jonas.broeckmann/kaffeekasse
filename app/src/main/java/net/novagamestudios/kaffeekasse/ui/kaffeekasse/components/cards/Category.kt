package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Coffee
import androidx.compose.material.icons.filled.Fastfood
import androidx.compose.material.icons.filled.Icecream
import androidx.compose.material.icons.filled.LocalOffer
import androidx.compose.material.icons.filled.LocalPizza
import androidx.compose.material.icons.filled.NoFood
import androidx.compose.material.icons.filled.WineBar
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.kaffeekasse.data.cleanName
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemCategory

@Composable
fun CategoryCard(
    category: ItemCategory,
    highlighted: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) = BasicCard(
    modifier
        .clip(CardDefaults.elevatedShape)
        .clickable(onClick = onClick),
    highlighted = highlighted
) {
    val name = category.cleanName
    Text(
        name,
        style = MaterialTheme.typography.titleMedium,
    )
    BoxCenter(
        Modifier
            .weight(1f)
            .fillMaxWidth()
    ) {
        CategoryIcon(category, Modifier.fillMaxSize(0.4f))
    }
}

@Composable
fun CategoryIcon(
    category: ItemCategory,
    modifier: Modifier = Modifier,
    tint: Color = LocalContentColor.current
) = Icon(
    when (category) {
        ItemCategory.ColdBeverage -> Icons.Default.WineBar
        ItemCategory.HotBeverage -> Icons.Default.Coffee
        ItemCategory.Snack -> Icons.Default.Fastfood
        ItemCategory.IceCream -> Icons.Default.Icecream
        ItemCategory.Food -> Icons.Default.LocalPizza
        ItemCategory.Fruit -> Icons.Default.NoFood
        ItemCategory.Other -> Icons.Default.LocalOffer
    },
    contentDescription = category.cleanName,
    modifier,
    tint = tint
)

