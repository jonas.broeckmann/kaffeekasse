package net.novagamestudios.kaffeekasse.ui.hiwi_tracker

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.pulltorefresh.PullToRefreshBox
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import cafe.adriel.voyager.navigator.Navigator
import io.woong.compose.grid.SimpleGridCells
import io.woong.compose.grid.VerticalGrid
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.format
import kotlinx.datetime.format.DayOfWeekNames
import kotlinx.datetime.format.MonthNames
import kotlinx.datetime.format.Padding
import kotlinx.datetime.format.char
import kotlinx.datetime.toKotlinLocalTime
import kotlinx.datetime.todayIn
import net.novagamestudios.common_utils.compose.DashedShape
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.compose.components.CircularProgressIndicator
import net.novagamestudios.common_utils.compose.components.RowCenter
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.verbose
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.model.MonthDataResponse
import net.novagamestudios.kaffeekasse.model.date_time.format
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.MonthKey
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.MonthKey.Companion.toMonthKey
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.MutableSettingsStore
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.HiwiTrackerRepository
import net.novagamestudios.kaffeekasse.ui.AppInfoTopBarAction
import net.novagamestudios.kaffeekasse.ui.AppModuleSelection
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.FailureRetryScreen
import net.novagamestudios.kaffeekasse.ui.login.LogoutTopBarAction
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.HiwiTrackerNavigation
import net.novagamestudios.kaffeekasse.ui.theme.disabled
import net.novagamestudios.kaffeekasse.ui.util.HorizontalKeyedPager
import net.novagamestudios.kaffeekasse.ui.util.HorizontalPagedLayout
import net.novagamestudios.kaffeekasse.ui.util.KeyedPagerState
import net.novagamestudios.kaffeekasse.ui.util.onClickComingSoon
import net.novagamestudios.kaffeekasse.ui.util.synchronizePagerState
import net.novagamestudios.kaffeekasse.util.WrappedJsonSerializationException
import net.novagamestudios.kaffeekasse.util.richdata.RichData
import net.novagamestudios.kaffeekasse.util.richdata.RichDataState
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichStateHere
import net.novagamestudios.kaffeekasse.util.richdata.exceptions
import java.time.format.DateTimeFormatter
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.time.Duration

class OverviewScreenModel private constructor(
    val session: Session,
    @Suppress("UNUSED_PARAMETER") mutableSettingsStore: MutableSettingsStore,
    private val hiwiTracker: HiwiTrackerRepository
) : ScreenModel, Logger {

    val initialMonth = Clock.System.todayIn(TimeZone.currentSystemDefault()).toMonthKey()
    var currentMonth by mutableStateOf(initialMonth)

    private val dataForMonthStates = mutableMapOf<MonthKey, RichDataState<MonthDataResponse>>()

    val pagerState = object : KeyedPagerState<MonthKey>(initialMonth.monthIndex) {
        override fun keyToIndex(key: MonthKey): Int {
            return key.monthIndex
        }

        override fun indexToKey(index: Int): MonthKey {
            return MonthKey(index)
        }

        override val pageCount = initialMonth.monthIndex + 100
    }

    fun dataForMonthState(month: MonthKey) = dataForMonthStates.getOrPut(month) {
        hiwiTracker.getDataForMonth(month).collectAsRichStateHere()
    }

    suspend fun keepMonthDataUpToDate() {
        snapshotFlow {
            listOf(
                currentMonth,
                currentMonth - 1,
                currentMonth + 1
            )
        }.collectLatest { toFetch ->
            toFetch.forEach {
                ensureDataForMonth(it)
            }
        }
    }


    private suspend fun ensureDataForMonth(month: MonthKey, reFetch: Boolean = false) {
        debug { "Ensuring data for month $month" }
        hiwiTracker.getDataForMonth(month).apply {
            if (reFetch) {
                verbose { "Refresh month: $month" }
                refresh()
            } else {
                verbose { "Ensure clean data for month: $month" }
                ensureCleanData()
            }
        }
    }
    fun refreshMonth(month: MonthKey) {
        screenModelScope.launch {
            ensureDataForMonth(month, reFetch = true)
        }
    }


    var enterWorkingHoursState by mutableStateOf<EnterWorkingHoursState?>(null)
    var enterDone by mutableStateOf(false)


    fun onClickDate(date: LocalDate) {
        enterDone = false
        enterWorkingHoursState = EnterWorkingHoursState(
            coroutineScope = screenModelScope,
            hiwiTracker = hiwiTracker,
            date = date,
            onSubmitted = { enterDone = true }
        )
    }

    fun onDeleteEntry(entry: MonthDataResponse.Entry) {
        screenModelScope.launch {
            hiwiTracker.deleteWorkEntry(entry.id)
        }
    }

    companion object : ScreenModelFactory<RepositoryProvider, HiwiTrackerNavigation.OverviewScreen, OverviewScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: HiwiTrackerNavigation.OverviewScreen) = OverviewScreenModel(
            session = screen.session,
            mutableSettingsStore = settingsRepository,
            hiwiTracker = hiwiTrackerRepository
        )
    }
}


class OverviewContent(provider: ScreenModelProvider<OverviewScreenModel>) : AppScaffoldContentWithModel<OverviewScreenModel>(provider) {
    @Composable
    override fun TopAppBarTitle(navigator: Navigator) {
        AppModuleSelection()
    }

    @Composable
    override fun TopAppBarActions(navigator: Navigator) {
        AppInfoTopBarAction()
        LogoutTopBarAction(model.session)
    }

    @Composable
    override fun backNavigationHandler(navigator: Navigator): BackNavigationHandler {
        return BackNavigationHandler.default()
    }
}


@Composable
fun Overview(
    model: OverviewScreenModel,
    modifier: Modifier = Modifier
) {
    // Bad fix because "isRefreshing = false" results in indicator not disappearing
    var isRefreshingQuickFix by remember { mutableStateOf(false) }
    PullToRefreshBox(
        isRefreshing = isRefreshingQuickFix.also { isRefreshingQuickFix = false },
        onRefresh = {
            isRefreshingQuickFix = true
            model.refreshMonth(model.currentMonth)
        },
        modifier.fillMaxSize()
    ) {
        LaunchedEffect(Unit) {
            model.keepMonthDataUpToDate()
        }
        Column {
            MonthSelection(
                model,
                Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(horizontal = 16.dp, vertical = 8.dp)
            )
            when (val data = model.dataForMonthState(model.currentMonth).value) {
                is RichData.Loading -> BoxCenter(Modifier.fillMaxSize()) {
                    CircularProgressIndicator(data.progress)
                }

                is RichData.Data -> Column {
                    synchronizePagerState(
                        pagerState = model.pagerState,
                        property = model::currentMonth
                    )
                    HorizontalKeyedPager(state = model.pagerState) { key ->
                        BoxCenter(Modifier.fillMaxWidth()) {
                            val pagerData = model.dataForMonthState(key).dataOrNull
                            if (pagerData != null) CalendarForMonth(
                                pagerData,
                                onClick = model::onClickDate,
                                Modifier
                                    .padding(horizontal = 32.dp)
                                    .padding(bottom = 8.dp)
                            )
                        }
                    }
                    HorizontalDivider()
                    val scrollState = rememberScrollState()
                    MonthContent(
                        model,
                        Modifier
                            .weight(1f)
                            .verticalScroll(scrollState)
                    )
                    LaunchedEffect(model.currentMonth) {
                        scrollState.animateScrollTo(0)
                    }
                }

                is RichData.Error -> FailureRetryScreen(
                    message = "Failed to fetch data",
                    Modifier.fillMaxSize(),
                    exceptions = data.exceptions,
                    additionalDetails = data.exceptions.mapNotNull { exception ->
                        when (exception) {
                            is WrappedJsonSerializationException -> exception.json
                            else -> null
                        }
                    },
                    onRetry = { model.refreshMonth(model.currentMonth) }
                )

                else -> {}
            }
        }
        model.enterWorkingHoursState?.let { subVM ->
            val sheetState = rememberModalBottomSheetState(skipPartiallyExpanded = true)
            ModalBottomSheet(
                onDismissRequest = { model.enterWorkingHoursState = null },
                sheetState = sheetState
            ) {
                EnterWorkingHoursForm(subVM)
                if (model.enterDone) LaunchedEffect(Unit) {
                    launch { sheetState.hide() }.invokeOnCompletion { model.enterWorkingHoursState = null }
                }
            }
        }
    }
}

@Composable
private fun MonthSelection(
    model: OverviewScreenModel,
    modifier: Modifier = Modifier
) = RowCenter(
    modifier,
    horizontalArrangement = Arrangement.spacedBy(8.dp)
) {
    IconButton(onClick = { model.currentMonth = model.currentMonth - 1 }) {
        Icon(Icons.Default.ChevronLeft, "Previous")
    }
    HorizontalPagedLayout(
        state = model.pagerState,
        Modifier
            .clip(RoundedCornerShape(8.dp))
            .width(200.dp)
    ) { index ->
        Text(
            remember(index) {
                MonthKey(index).toLocalDate().format(LocalDate.Format {
                    monthName(MonthNames.GERMAN_FULL)
                    chars(" ")
                    year(Padding.NONE)
                })
            },
            Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.titleLarge
        )
    }
    IconButton(onClick = { model.currentMonth = model.currentMonth + 1 }) {
        Icon(Icons.Default.ChevronRight, "Next")
    }
}


@Composable
private fun CalendarForMonth(
    data: MonthDataResponse,
    onClick: (LocalDate) -> Unit,
    modifier: Modifier = Modifier
) {
    val calendarDays by remember { derivedStateOf { data.calendar.toSortedMap() } }
    val today = remember { LocalDate.now() }
    VerticalGrid(
        columns = SimpleGridCells.Fixed(count = 7, fill = true),
        modifier.widthIn(max = 400.dp),
        horizontalArrangement = Arrangement.spacedBy(4.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {
        (1..7).forEach { isoDayNumber ->
            Text(
                remember(isoDayNumber) {
                    DayOfWeekNames.GERMAN_FULL.names[isoDayNumber - 1].take(2)
                },
                Modifier.fillMaxWidth(),
                color = LocalContentColor.current.disabled(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.labelSmall
            )
        }
        calendarDays.forEach { (day, data) ->
            OutlinedCard(
                onClick = { onClick(day) },
                modifier = Modifier
                    .aspectRatio(1.4f)
                    .fillMaxSize(),
                shape = RoundedCornerShape(8.dp),
                border = when (day) {
                    today -> CardDefaults.outlinedCardBorder().copy(
                        width = 2.dp,
                        brush = SolidColor(MaterialTheme.colorScheme.primary)
                    )
                    else -> CardDefaults.outlinedCardBorder()
                }
            ) {
                BoxCenter(Modifier.fillMaxSize()) {
                    val localStyle = LocalTextStyle.current
                    val style = when {
                        data.disabled -> localStyle.copy(color = LocalContentColor.current.disabled())
                        data.hasEntry -> localStyle.copy(
                            color = MaterialTheme.colorScheme.primary,
                            fontWeight = FontWeight.Bold
                        )
                        else -> localStyle
                    }
                    Text(
                        day.dayOfMonth.toString(),
                        style = style
                    )
                }
            }
        }
    }
}

@Composable
private fun MonthContent(
    model: OverviewScreenModel,
    modifier: Modifier = Modifier
) = Column(modifier) {
    val data = model.dataForMonthState(model.currentMonth).dataOrNull ?: return

    MonthlyTimeDiagram(
        data,
        Modifier
            .padding(8.dp)
            .fillMaxWidth()
    )
    HorizontalDivider(Modifier.clip(DashedShape(6.dp)))
    Column(Modifier.padding(8.dp)) {
        val done = data.total.hoursBalance + data.totalMonth.hoursWorked
        val todo = data.totalMonth.hoursThisMonth - done
        ProvideTextStyle(MaterialTheme.typography.labelLarge) {
            Row(Modifier.padding(horizontal = 8.dp)) {
                Text("Erbracht:")
                Spacer(Modifier.weight(1f))
                Text(done.fHT())
            }
            Row(Modifier.padding(horizontal = 8.dp)) {
                Text("Verbleibend:")
                Spacer(Modifier.weight(1f))
                Text(todo.fHT())
            }
        }
    }
    HorizontalDivider()
    data.entries.forEach { entry ->
        WorkEntry(
            entry,
            onClick = onClickComingSoon(),
            onDelete = { model.onDeleteEntry(entry) },
        )
    }
}

@Composable
private fun WorkEntry(
    entry: WorkEntry,
    onClick: () -> Unit,
    onDelete: () -> Unit,
    modifier: Modifier = Modifier
) {
    var confirmDelete by remember { mutableStateOf(false) }
    ListItem(
        headlineContent = {
            Row(
                horizontalArrangement = Arrangement.spacedBy(12.dp),
                verticalAlignment = Alignment.Bottom
            ) {
                val date = remember(entry) {
                    entry.date.format(DateTimeFormatter.ofPattern("dd.MM."))
                }
                Text(date)
                Text("${entry.begin.fHT()} - ${entry.end.fHT()}", fontWeight = FontWeight.Bold)
                entry.breakDurationOrNull?.takeIf { it.isPositive() }?.let {
                    Text("Pause: ${it.fHT()}", fontSize = LocalTextStyle.current.fontSize * 0.8)
                }
            }
        },
        modifier.combinedClickable(
            onLongClick = { confirmDelete = true },
            onClick = onClick
        ),
        supportingContent = {
            Text(entry.note, overflow = TextOverflow.Ellipsis, maxLines = 1)
        },
        trailingContent = {
            Text(entry.workedDuration.fHT(), style = MaterialTheme.typography.bodyLarge)
        }
    )
    if (confirmDelete) AlertDialog(
        onDismissRequest = { confirmDelete = false },
        confirmButton = {
            Button(
                onClick = onDelete,
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.errorContainer,
                    contentColor = MaterialTheme.colorScheme.onErrorContainer
                )
            ) { Text("Löschen") }
        },
        dismissButton = {
            TextButton(onClick = { confirmDelete = false }) { Text("Abbrechen") }
        },
        icon = {
            Icon(Icons.Default.DeleteForever, "Delete", tint = MaterialTheme.colorScheme.error)
        },
        title = { Text("Eintrag löschen?") }
    )
}

@Composable
private fun MonthlyTimeDiagram(
    data: MonthDataResponse,
    modifier: Modifier = Modifier
) {
    val workedAtStartOfMonth by animateFloatAsState(
        data.total.hoursBalance.inWholeSeconds.toFloat(),
        label = "workedAtStartOfMonth"
    )
    val workedThisMonth by animateFloatAsState(
        data.totalMonth.hoursWorked.inWholeSeconds.toFloat(),
        label = "workedThisMonth"
    )
    val totalThisMonth by animateFloatAsState(
        data.totalMonth.hoursThisMonth.inWholeSeconds.toFloat(),
        label = "totalThisMonth"
    )

    val atStartOfMonthColor by animateColorAsState(
        if (workedAtStartOfMonth >= 0) Color.Green else Color.Red,
        label = "atStartOfMonthColor"
    )
    val thisMonthColor by rememberUpdatedState(LocalContentColor.current)

    val lineColor by rememberUpdatedState(MaterialTheme.colorScheme.outlineVariant)

    val xMin = minOf(workedAtStartOfMonth, 0f)
    val xMax = maxOf(workedAtStartOfMonth + workedThisMonth, totalThisMonth)
    val range = xMax - xMin

    val xZeroFraction = -xMin / range
    val xMonthFraction = (totalThisMonth - xMin) / range
    Layout(
        content = {
            Canvas(Modifier) {
                val factor = size.width / range

                val xStart = 0f
                val xZero = xZeroFraction * size.width
                val xMonth = xMonthFraction * size.width
                val xEnd = size.width

                val x1 = 0f
                val s1 = workedAtStartOfMonth.absoluteValue * factor

                val x2 = (xZeroFraction + workedAtStartOfMonth / range) * size.width
                val s2 = workedThisMonth.absoluteValue * factor

                val hp = 6.dp.toPx()
                val l = 1.dp.toPx()
                val h = (size.height - l - 2 * hp) / 2f

                drawLine(
                    lineColor,
                    Offset(xStart, hp + h + l / 2f),
                    Offset(xEnd, hp + h + l / 2f),
                    l
                )
                drawLine(lineColor, Offset(xZero, 0f), Offset(xZero, size.height), l)
                drawLine(lineColor, Offset(xMonth, 0f), Offset(xMonth, size.height), l)

                drawRoundRect(
                    color = atStartOfMonthColor,
                    topLeft = Offset(x1 + l, hp),
                    size = Size(s1 - l, h),
                    cornerRadius = CornerRadius(h / 2f),
                    //                style = Stroke(
                    //                    width = 1.dp.toPx(),
                    //                    pathEffect = PathEffect.dashPathEffect(floatArrayOf(8f, 8f))
                    //                )
                )
                if (data.totalMonth.hoursWorked > Duration.ZERO) drawRoundRect(
                    thisMonthColor,
                    Offset(x2 + l, hp + l + h),
                    Size(s2 - l, h),
                    cornerRadius = CornerRadius(h / 2f)
                )
            }
            val style = MaterialTheme.typography.labelLarge
            Text(data.total.hoursBalance.fHT(), style = style)
            Text(data.totalMonth.hoursWorked.fHT(), style = style)
            Text(Duration.ZERO.fHT(), style = style)
            Text(data.totalMonth.hoursThisMonth.fHT(), style = style)
        },
        modifier
    ) { measurables, constraints ->
        check(measurables.size == 5)
        val (
            chartMeasurable,
            firstLabelMeasurable,
            secondLabelMeasurable,
            zeroLabelMeasurable,
            monthLabelMeasurable
        ) = measurables

        val firstLabel = firstLabelMeasurable.measure(Constraints())
        val secondLabel = secondLabelMeasurable.measure(Constraints())
        val zeroLabel = zeroLabelMeasurable.measure(Constraints())
        val monthLabel = monthLabelMeasurable.measure(Constraints())

        val yLabelWidth = 56.dp.toPx().roundToInt()
        val yLabelHeight = firstLabel.height + secondLabel.height
        val xLabelHeight = max(zeroLabel.height, monthLabel.height)

        val chart = chartMeasurable.measure(
            constraints.copy(
                minWidth = constraints.minWidth - yLabelWidth,
                maxWidth = constraints.maxWidth - yLabelWidth,
                minHeight = yLabelHeight,
                maxHeight = yLabelHeight
            )
        )

        layout(
            width = constraints.maxWidth,
            height = yLabelHeight + xLabelHeight
        ) {
            chart.placeRelative(yLabelWidth, 0)

            val yLabelSpacing = 4.dp.toPx().roundToInt()
            firstLabel.placeRelative(yLabelWidth - firstLabel.width - yLabelSpacing, 0)
            secondLabel.placeRelative(
                yLabelWidth - secondLabel.width - yLabelSpacing,
                firstLabel.height
            )

            fun Placeable.placeXLabelAt(fraction: Float) {
                val x = yLabelWidth + fraction * chart.width - width / 2f
                val xRange = coordinates?.let {
                    0..(it.size.width - width)
                } ?: Int.MIN_VALUE..Int.MAX_VALUE
                placeRelative(
                    x.roundToInt().coerceIn(xRange),
                    chart.height
                )
            }
            zeroLabel.placeXLabelAt(xZeroFraction)
            monthLabel.placeXLabelAt(xMonthFraction)
        }
    }
}


private val HTTimeFormat by lazy {
    LocalTime.Format {
        hour()
        char(':')
        minute()
    }
}


@Composable
internal fun LocalTime.fHT() = remember(this) { formatHiwiTracker() }
internal fun LocalTime.formatHiwiTracker() = if (this == java.time.LocalTime.MAX.toKotlinLocalTime()) "24:00" else format(HTTimeFormat)

@Composable
internal fun Duration.fHT() = remember(this) { formatHiwiTracker() }
internal fun Duration.formatHiwiTracker(): String {
    val hours = inWholeHours.absoluteValue
    val minutes = (inWholeMinutes % 60).absoluteValue
//    val sign = if (hours < 0) "-" else ""
//    val h = if (hours > 0) "${hours}h" else ""
//    val m = if (hours > 0 || minutes > 0) "${minutes}m" else ""
    return listOfNotNull(
        hours.takeIf { it != 0L || minutes == 0L }?.let { "${it}h" },
        minutes.takeIf { it != 0L }?.let { "${it.toString().padStart(2, '0')}m" }
    ).joinToString("", prefix = if (inWholeHours < 0) "-" else "")
}

