package net.novagamestudios.kaffeekasse.ui.kaffeekasse

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.KaffeekasseModule.Companion.kaffeekasseCartProvider
import net.novagamestudios.kaffeekasse.model.kaffeekasse.MutableCart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.isNotEmpty
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.KaffeekasseNavigation
import net.novagamestudios.kaffeekasse.ui.theme.ifAnimationsEnabled


class KaffeekasseModuleScreenModel private constructor(
    val cart: MutableCart
) : ScreenModel, Logger {
    companion object : ScreenModelFactory<RepositoryProvider, KaffeekasseNavigation.Tab, KaffeekasseModuleScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: KaffeekasseNavigation.Tab) = KaffeekasseModuleScreenModel(
            cart = kaffeekasseCartProvider[screen.session.realUser]
        )
    }
}

class KaffeekasseModuleContent(provider: ScreenModelProvider<KaffeekasseModuleScreenModel>) : AppScaffoldContentWithModel<KaffeekasseModuleScreenModel>(provider) {
    @Composable
    override fun TopAppBarNavigationIcon(navigator: Navigator) {
        CartEmptyNavigation(model, backNavigationHandler(navigator))
    }
}


@Composable
private fun CartEmptyNavigation(
    model: KaffeekasseModuleScreenModel,
    backNavigationHandler: BackNavigationHandler
) {
    AnimatedVisibility(
        visible = !backNavigationHandler.canNavigateBack() && model.cart.isNotEmpty(),
        enter = expandHorizontally().ifAnimationsEnabled(),
        exit = shrinkHorizontally().ifAnimationsEnabled()
    ) {
        IconButton(onClick = { model.cart.clear() }) {
            Icon(Icons.Default.Delete, "Empty cart")
        }
    }
}

