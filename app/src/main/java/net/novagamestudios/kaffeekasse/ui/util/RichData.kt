package net.novagamestudios.kaffeekasse.ui.util

import androidx.compose.foundation.layout.BoxScope
import androidx.compose.material3.pulltorefresh.PullToRefreshBox
import androidx.compose.material3.pulltorefresh.PullToRefreshDefaults
import androidx.compose.material3.pulltorefresh.PullToRefreshState
import androidx.compose.material3.pulltorefresh.pullToRefresh
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.components.Progress
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.FailureRetryScreen
import net.novagamestudios.kaffeekasse.util.richdata.RichData
import net.novagamestudios.kaffeekasse.util.richdata.RichDataFunctions
import net.novagamestudios.kaffeekasse.util.richdata.RichDataSource
import net.novagamestudios.kaffeekasse.util.richdata.RichDataStateWithFunctions
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichState
import net.novagamestudios.kaffeekasse.util.richdata.exceptions


@Composable
fun <T : Any> PullToRefreshBox(
    data: RichDataStateWithFunctions<T>,
    modifier: Modifier = Modifier,
    state: PullToRefreshState = rememberPullToRefreshState(),
    indicator: @Composable (BoxScope.() -> Unit) = {
        PullToRefreshDefaults.Indicator(
            modifier = Modifier.align(Alignment.TopCenter),
            isRefreshing = data.isLoading,
            state = state
        )
    },
    content: @Composable BoxScope.() -> Unit
) {
    val coroutineScope = rememberCoroutineScope()
    PullToRefreshBox(
        isRefreshing = data.isLoading,
        onRefresh = { coroutineScope.launch { data.refresh() } },
        modifier,
        state = state,
        indicator = indicator,
        content = content
    )
}

fun <T : Any> Modifier.pullToRefresh(
    data: RichDataSource<T>,
    state: PullToRefreshState,
    enabled: () -> Boolean = { true },
    threshold: Dp = PullToRefreshDefaults.PositionalThreshold
): Modifier = composed {
    val dataState = data.collectAsRichState()
    val coroutineScope = rememberCoroutineScope()
    pullToRefresh(
        isRefreshing = dataState.isLoading,
        state = state,
        enabled = enabled(),
        threshold = threshold,
        onRefresh = { coroutineScope.launch { data.refresh() } }
    )
}

@Composable
fun <T : Any> PullToRefreshIndicator(
    data: RichDataSource<T>,
    state: PullToRefreshState,
    modifier: Modifier = Modifier,
    containerColor: Color = PullToRefreshDefaults.containerColor,
    color: Color = PullToRefreshDefaults.indicatorColor,
    threshold: Dp = PullToRefreshDefaults.PositionalThreshold
) {
    val dataState = data.collectAsRichState()
    PullToRefreshDefaults.Indicator(
        state = state,
        isRefreshing = dataState.isLoading,
        modifier = modifier,
        containerColor = containerColor,
        color = color,
        threshold = threshold
    )
}

@Composable
inline fun <T : Any> RichDataContent(
    source: RichDataSource<T>,
    ensureClean: Boolean = true,
    errorContent: @Composable RichDataContentScope<T>.(RichData.Error<T>) -> Unit = { },
    loadingContent: @Composable RichDataContentScope<T>.(Progress) -> Unit = { },
    noneContent: @Composable RichDataContentScope<T>.() -> Unit = { },
    dataContent: @Composable RichDataContentScope<T>.(T) -> Unit
) = RichDataContent(
    state = source.collectAsRichState(),
    ensureClean = ensureClean,
    errorContent = errorContent,
    loadingContent = loadingContent,
    noneContent = noneContent,
    dataContent = dataContent
)


@Composable
inline fun <T : Any> RichDataContent(
    state: RichDataStateWithFunctions<T>,
    ensureClean: Boolean = true,
    errorContent: @Composable RichDataContentScope<T>.(RichData.Error<T>) -> Unit = { },
    loadingContent: @Composable RichDataContentScope<T>.(Progress) -> Unit = { },
    noneContent: @Composable RichDataContentScope<T>.() -> Unit = { },
    dataContent: @Composable RichDataContentScope<T>.(T) -> Unit
) {
    if (ensureClean) LaunchedEffect(Unit) { state.ensureCleanData() }
    val scope = remember(state) { object : RichDataContentScope<T> { override val functions = state } }
    when (val value = state.value) {
        is RichData.None<T> -> scope.noneContent()
        is RichData.Loading<T> -> scope.loadingContent(value.progress)
        is RichData.Data<T> -> scope.dataContent(value.data)
        is RichData.Error<T> -> scope.errorContent(value)
    }
}

interface RichDataContentScope<T : Any> {
    val functions: RichDataFunctions<T>
}


context (RichDataContentScope<T>)
@Composable
fun <T : Any> FailureRetryScreen(
    error: RichData.Error<T>,
    message: String,
    modifier: Modifier = Modifier,
    additionalDetails: Any? = null
) {
    val coroutineScope = rememberCoroutineScope()
    FailureRetryScreen(
        message = message,
        modifier = modifier,
        errorsShort = error.messages,
        exceptions = error.exceptions,
        additionalDetails = additionalDetails,
        onRetry = { coroutineScope.launch { functions.refresh() } }
    )
}


