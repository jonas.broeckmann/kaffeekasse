package net.novagamestudios.kaffeekasse.ui.navigation

import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.compose.nestedScroll
import net.novagamestudios.common_utils.voyager.AnimatedBackNavigationIcon
import net.novagamestudios.common_utils.voyager.debugNavigation
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.common_utils.voyager.scaffold.TopAppBar
import net.novagamestudios.common_utils.voyager.scaffold.TopAppBarContent
import net.novagamestudios.kaffeekasse.CrashHandling
import net.novagamestudios.kaffeekasse.ui.theme.LocalAnimationSwitch
import net.novagamestudios.kaffeekasse.ui.theme.ifAnimationsEnabled


interface AppScaffoldContent : TopAppBarContent

abstract class AppScaffoldContentWithModel<out T : ScreenModel>(
    provider: ScreenModelProvider<T>
) : AppScaffoldContent, ScreenModelProvider<T> by provider


@Composable
fun AppScaffoldNavigator(
    key: String,
    initialRoute: List<Screen>,
    scaffoldContent: AppScaffoldContent,
    topAppBarScrollBehavior: TopAppBarScrollBehavior? = TopAppBarDefaults.enterAlwaysScrollBehavior().takeIf { LocalAnimationSwitch.current },
    content: @Composable Navigator.() -> Unit = { AppScaffoldNavigatorDefaultContent("$key-content") }
) = Navigator(
    screens = initialRoute,
    onBackPressed = null,
    key = key
) { navigator ->
    debugNavigation()
    CrashHandling.updateNavigation(navigator)
    Scaffold(
        Modifier.nestedScroll(topAppBarScrollBehavior?.nestedScrollConnection),
        topBar = {
            TopAppBar(
                content = scaffoldContent,
                navigator = navigator,
                backNavigationIcon = {
                    AnimatedBackNavigationIcon(
                        handler = it,
                        enter = expandHorizontally().ifAnimationsEnabled(),
                        exit = shrinkHorizontally().ifAnimationsEnabled()
                    )
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    scrolledContainerColor = MaterialTheme.colorScheme.surface
                ),
                scrollBehavior = topAppBarScrollBehavior
            )
        }
    ) { paddingValues ->
        Box(
            Modifier
                .padding(paddingValues)
                .fillMaxSize(),
            propagateMinConstraints = true
        ) {
            navigator.content()
            scaffoldContent.backNavigationHandler(navigator).BackHandler()
        }
    }
}

@Composable
fun Navigator.AppScaffoldNavigatorDefaultContent(
    key: String
) {
    AppScreenTransition(this, key = key) { screen ->
        screen.Content()
    }
}


@Composable
fun AppSubpageTitle(text: String) = Text(text, Modifier.padding(start = 8.dp))
