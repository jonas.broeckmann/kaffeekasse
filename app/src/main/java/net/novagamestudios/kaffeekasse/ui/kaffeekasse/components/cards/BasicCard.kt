package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import net.novagamestudios.common_utils.compose.thenIf

@Composable
fun BasicCard(
    modifier: Modifier = Modifier,
    highlighted: Boolean = false,
    content: @Composable ColumnScope.() -> Unit
) = ElevatedCard(
    modifier
        .aspectRatio(1f)
        .thenIf(highlighted) {
            border(2.dp, MaterialTheme.colorScheme.primary, CardDefaults.elevatedShape)
        }
) {
    Column(
        Modifier
            .thenIf(highlighted) {
                background(MaterialTheme.colorScheme.primary.copy(alpha = 0.2f))
            }
            .fillMaxSize()
            .padding(12.dp)
    ) {
        content()
    }
}