package net.novagamestudios.kaffeekasse.ui.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.TwoWayConverter
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateValueAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisallowComposableCalls
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import io.ktor.http.Url
import net.novagamestudios.common_utils.core.logging.LoggerForFun
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.core.toastShort


@Composable
fun onClickComingSoon(): () -> Unit {
    val context = LocalContext.current
    return { context.toastShort("Coming soon") }
}


@Composable
fun ColorFilter.Companion.monochrome(tint: Color) = remember(tint) {
    colorMatrix(
        ColorMatrix().apply {
            this *= ColorMatrix().apply { setToTint(tint) }
            this *= ColorMatrix().apply { setToSaturation(0f) }
        }
    )
}

private fun ColorMatrix.setToTint(tint: Color) {
    reset()
    this[0, 0] = tint.red
    this[1, 1] = tint.green
    this[2, 2] = tint.blue
    this[3, 3] = tint.alpha
}


@Composable
fun animateTextUnitAsState(
    targetValue: TextUnit,
    animationSpec: AnimationSpec<TextUnit> = spring(),
    label: String = "TextUnitAnimation",
    finishedListener: ((TextUnit) -> Unit)? = null
): State<TextUnit> {
    val density = LocalDensity.current
    return animateValueAsState(
        targetValue = targetValue,
        typeConverter = remember(density) {
            val dpConverter = Dp.VectorConverter
            with(density) {
                TwoWayConverter(
                    convertToVector = { dpConverter.convertToVector( it.toDp()) },
                    convertFromVector = { dpConverter.convertFromVector(it).toSp() }
                )
            }
        },
        animationSpec = animationSpec,
        label = label,
        finishedListener = finishedListener
    )
}


@Composable
fun <T, R> State<T>.derived(
    calculation: @DisallowComposableCalls T.() -> R
) = remember(this) {
    derivedStateOf {
        value.calculation()
    }
}

fun Context.openInBrowser(url: String) = openInBrowser(Url(url))
fun Context.openInBrowser(url: Url) {
    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url.toString()))
    startActivity(browserIntent)
}


/**
 * See [https://issuetracker.google.com/issues/226665301](https://issuetracker.google.com/issues/226665301)
 */
fun removeScrollableTabRowMinimumTabWidth() = with(LoggerForFun()) {
    try {
        Class
            .forName("androidx.compose.material3.TabRowKt")
            .getDeclaredField("ScrollableTabRowMinimumTabWidth")
            .apply { isAccessible = true }
            .set(this, 0f)
    } catch (e: Exception) {
        error(e) { "Failed to remove ScrollableTabRowMinimumTabWidth" }
    }
}

@Composable
fun DebugDataText(
    data: Any?,
    modifier: Modifier = Modifier,
    style: TextStyle = LocalTextStyle.current.copy(
        fontFamily = FontFamily.Monospace
    )
) = SelectionContainer {
    Text("$data", modifier = modifier, style = style)
}
