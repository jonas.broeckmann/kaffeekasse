package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.novagamestudios.common_utils.compose.DashedShape
import net.novagamestudios.common_utils.compose.components.ColumnCenter
import net.novagamestudios.common_utils.compose.state.ReentrantActionState
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.verbose
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Item
import net.novagamestudios.kaffeekasse.model.kaffeekasse.MutableCart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Transaction
import net.novagamestudios.kaffeekasse.model.session.User
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.SettingsRepository
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.ui.theme.disabled
import net.novagamestudios.kaffeekasse.util.richdata.asRichDataFlow
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichStateIn
import net.novagamestudios.kaffeekasse.util.richdata.combineRich
import net.novagamestudios.kaffeekasse.util.richdata.dataOrNull
import net.novagamestudios.kaffeekasse.util.richdata.mapRich
import net.novagamestudios.kaffeekasse.util.richdata.stateIn
import java.time.LocalDateTime

class CustomItemsState private constructor(
    private val user: User,
    private val computationScope: CoroutineScope,
    settingsRepository: SettingsRepository,
    private val kaffeekasse: KaffeekasseRepository
) : Logger {

    private val allItems = kaffeekasse.stock
        .mapRich { details -> details.itemGroups.flatMap { it.items } }
        .stateIn(computationScope, SharingStarted.Eagerly)
    private val allItemsById = allItems
        .mapRich { items -> items.associateBy { it.id } }
        .stateIn(computationScope, SharingStarted.Eagerly)
    private val allItemsByName = allItems
        .mapRich { items -> items.associateBy { it.originalName } }
        .stateIn(computationScope, SharingStarted.Eagerly)

    @OptIn(ExperimentalCoroutinesApi::class)
    private val currentUserFavoriteItemIds = snapshotFlow { settingsRepository.userSettings.value }
        .flatMapLatest { it?.values ?: emptyFlow() }
        .map { it.favoriteItemIds }
        .distinctUntilChanged()
        .asRichDataFlow()

    val favoriteItems = combineRich(
        currentUserFavoriteItemIds,
        allItemsById
    ) { favoriteItems, itemsById ->
        favoriteItems.mapNotNull { itemsById[it] }
    }.collectAsRichStateIn(computationScope)

    private val suggestedItems = mutableStateListOf<Item>()
    val displayedSuggestedItems by derivedStateOf {
        val displayed = suggestedItems.toMutableList()
        displayed -= favoriteItems.dataOrNull.orEmpty().toSet()
        displayed.take(SuggestedItemsCount)
    }

    private val userTransactions = kaffeekasse.transactions[user]
    private val userTransactionsState = userTransactions.collectAsRichStateIn(computationScope)

    private val suggestionsMutex = ReentrantActionState()
    val isLoadingSuggestions by derivedStateOf {
        suggestionsMutex.value || userTransactionsState.isLoading
    }


    init {
        verbose { "${CustomItemsState::class.simpleName} initialized" }
        computationScope.launch {
            userTransactions
                .map { it.dataOrNull }
                .filterNotNull()
                .collectLatest { transactions ->
                    suggestionsMutex.trueWhile {
                        val favourites = favoriteItems.dataOrNull.orEmpty().toSet()
                        val calculated = withContext(Dispatchers.IO) {
                            calculateSuggestions(allItemsByName.value.dataOrNull.orEmpty(), transactions)
                                .filter { it !in favourites }
                                .toList()
                        }
                        suggestedItems.clear()
                        suggestedItems += calculated
                    }
                }
        }
    }

    fun fetch() {
        computationScope.launch {
            userTransactions.ensureCleanData()
        }
    }

    companion object {

        // calculate suggestions based on recency and frequency of transactions
        private fun calculateSuggestions(
            allItemsByName: Map<String, Item>,
            transactions: List<Transaction>
        ): Sequence<Item> {
            val statsByItem: Map<Item, ItemStats> = transactions.asSequence()
                .take(TransactionsLimit)
                .mapNotNull {
                    val purpose = it.purpose as? Transaction.Purpose.Purchase ?: return@mapNotNull null
                    val item = allItemsByName[purpose.itemName] ?: return@mapNotNull null
                    item to ItemStats(it.date, purpose.itemCount.toInt())
                }
                .groupBy({ it.first }) { it.second }
                .mapValues { (_, stats) ->
                    stats.reduce { a, b ->
                        ItemStats(
                            lastPurchase = maxOf(a.lastPurchase, b.lastPurchase),
                            purchaseCount = a.purchaseCount + b.purchaseCount
                        )
                    }
                }

            val recentPurchases = statsByItem
                .entries
                .sortedByDescending { it.value.lastPurchase }
            val frequentPurchases = recentPurchases
                .sortedByDescending { it.value.purchaseCount }

            val pointsByItem = mutableMapOf<Item, Double>()

            recentPurchases.asSequence().take(RecentItemsConsidered).forEachIndexed { i, (item, _) ->
                pointsByItem[item] = (pointsByItem[item] ?: 0.0) + (1.0 - i / RecentItemsConsidered.toDouble()) * RecentItemsWeight
            }
            frequentPurchases.asSequence().take(FrequentItemsConsidered).forEachIndexed { i, (item, _) ->
                pointsByItem[item] = (pointsByItem[item] ?: 0.0) + (1.0 - i / FrequentItemsConsidered.toDouble()) * FrequentItemsWeight
            }

            return pointsByItem
                .entries
                .asSequence()
                .sortedByDescending { it.value }
                .map { it.key }
        }
        private data class ItemStats(
            val lastPurchase: LocalDateTime,
            val purchaseCount: Int
        )

        private const val TransactionsLimit = 100
        private const val RecentItemsConsidered = 10
        private const val FrequentItemsConsidered = 10
        private const val RecentItemsWeight = 1.0
        private const val FrequentItemsWeight = 2.0

        private const val SuggestedItemsCount = 6


        context (RepositoryProvider)
        fun create(
            user: User,
            computationScope: CoroutineScope
        ) = CustomItemsState(
            user = user,
            computationScope = computationScope,
            settingsRepository = settingsRepository,
            kaffeekasse = kaffeekasseRepository
        )
    }
}

@Composable
fun CustomItems(
    state: CustomItemsState,
    cart: MutableCart,
    modifier: Modifier = Modifier
) = Column(modifier.verticalScroll(rememberScrollState())) {
    ColumnCenter(Modifier.fillMaxWidth()) {
        if (state.favoriteItems.dataOrNull.orEmpty().isEmpty()) Text(
            "Lange drücken, um Waren den Favoriten hinzuzufügen",
            Modifier.padding(horizontal = 16.dp, vertical = 64.dp),
            color = LocalContentColor.current.disabled(),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.labelLarge
        ) else ItemCardGrid(
            items = state.favoriteItems.dataOrNull.orEmpty(),
            cart = cart
        )
    }
    HorizontalDivider(
        Modifier
            .padding(horizontal = 8.dp)
            .padding(bottom = 8.dp)
            .clip(DashedShape(8.dp))
    )
    ColumnCenter(Modifier.fillMaxWidth()) {
        LaunchedEffect(Unit) {
            if (state.displayedSuggestedItems.isEmpty()) state.fetch()
        }
        Text(
            "Vorschläge".uppercase(),
            Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
            style = MaterialTheme.typography.labelLarge
        )
        if (state.isLoadingSuggestions) {
            CircularProgressIndicator(Modifier.padding(vertical = 64.dp))
        } else if (state.displayedSuggestedItems.isEmpty()) {
            Text(
                "Keine Vorschläge",
                Modifier.padding(horizontal = 16.dp, vertical = 64.dp),
                color = LocalContentColor.current.disabled(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.labelLarge
            )
        } else {
            ItemCardGrid(
                items = state.displayedSuggestedItems,
                cart = cart,
                spacerForFAB = true
            )
        }
    }
}