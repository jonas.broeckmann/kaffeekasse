package net.novagamestudios.kaffeekasse.ui.login

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Key
import androidx.compose.material.icons.filled.PhonelinkErase
import androidx.compose.material.icons.filled.ScreenLockLandscape
import androidx.compose.material.icons.filled.SmartScreen
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.components.CircularLoadingBox
import net.novagamestudios.common_utils.compose.state.collectAsStateIn
import net.novagamestudios.common_utils.voyager.nearestScreen
import net.novagamestudios.kaffeekasse.model.credentials.DeviceCredentials
import net.novagamestudios.kaffeekasse.model.credentials.isValid
import net.novagamestudios.kaffeekasse.repositories.LoginRepository
import net.novagamestudios.kaffeekasse.ui.navigation.LoginNavigation
import net.novagamestudios.kaffeekasse.ui.util.rememberSerializableState


class LoginDeviceState(
    private val coroutineScope: CoroutineScope,
    private val loginRepository: LoginRepository
) {
    val isLoading by loginRepository.isPerformingAction.collectAsStateIn(coroutineScope)

    var showDialog by mutableStateOf(false)

    fun login(deviceCredentials: DeviceCredentials) {
        coroutineScope.launch {
            loginRepository.loginDevice(deviceCredentials)
            showDialog = false
        }
    }

    fun logout() {
        coroutineScope.launch {
            loginRepository.logoutDevice()
        }
    }
}

@Composable
private fun findState(): LoginDeviceState {
    return nearestScreen<LoginNavigation.Companion>().model.loginDeviceState
}

@Composable
fun LoginDeviceAdditional(
    state: LoginDeviceState,
    modifier: Modifier = Modifier
) {
    if (state.showDialog) LoginDeviceDialog(
        onDismiss = { state.showDialog = false },
        onLogin = { state.login(it) },
        isLoading = state.isLoading,
        modifier
    )
}

@Composable
private fun LoginDeviceDialog(
    onDismiss: () -> Unit,
    onLogin: (DeviceCredentials) -> Unit,
    isLoading: Boolean,
    modifier: Modifier = Modifier
) {
    val inputState = rememberSerializableState {
        mutableStateOf(DeviceCredentials.Empty)
    }
    val input by inputState
    AlertDialog(
        onDismissRequest = onDismiss,
        confirmButton = {
            Button(
                onClick = { onLogin(input) },
                enabled = !isLoading && input.isValid
            ) {
                Text("Einloggen")
            }
        },
        modifier,
        dismissButton = {
            TextButton(
                onClick = onDismiss,
                enabled = !isLoading
            ) {
                Text("Abbrechen")
            }
        },
        text = {
            CircularLoadingBox(loading = isLoading) {
                LoginDeviceForm(
                    inputState = inputState,
                    onDone = { onLogin(input) }
                )
            }
        }
    )
}

@Composable
private fun LoginDeviceForm(
    inputState: MutableState<DeviceCredentials>,
    onDone: () -> Unit,
    modifier: Modifier = Modifier
) {
    var input by inputState
    Column(
        modifier.width(IntrinsicSize.Min),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val focusManager = LocalFocusManager.current
        OutlinedTextField(
            value = input.deviceId,
            onValueChange = { input = input.copy(deviceId = it.uppercase()) },
            label = { Text("Geräte-Id") },
            placeholder = { Text("XX:XX:XX:XX:XX:XX") },
            leadingIcon = { Icon(Icons.Default.SmartScreen, "Geräte-Id") },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            ),
            keyboardActions = KeyboardActions(onNext = { focusManager.moveFocus(FocusDirection.Next) }),
            singleLine = true
        )
        OutlinedTextField(
            value = input.apiKey,
            onValueChange = { input = input.copy(apiKey = it) },
            label = { Text("API-Schlüssel") },
            leadingIcon = { Icon(Icons.Default.Key, "API-Schlüssel") },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(onDone = { onDone() }),
            singleLine = true
        )
    }
}


@Composable
fun LoginDeviceButton(
    modifier: Modifier = Modifier
) {
    val state = findState()
    IconButton(
        onClick = { state.showDialog = true },
        modifier,
        enabled = !state.isLoading
    ) {
        Icon(Icons.Default.ScreenLockLandscape, "Gerät einloggen")
    }
}

@Composable
fun LogoutDeviceButton(
    modifier: Modifier = Modifier
) {
    val state = findState()
    var confirmLogout by remember { mutableStateOf(false) }
    IconButton(
        onClick = { confirmLogout = true },
        modifier,
        enabled = !state.isLoading
    ) {
        Icon(Icons.Default.PhonelinkErase, "Gerät ausloggen")
    }
    if (confirmLogout) ConfirmLogoutDeviceDialog(
        onDismiss = { confirmLogout = false },
        onConfirm = { state.logout() }
    )
}

@Composable
private fun ConfirmLogoutDeviceDialog(
    onDismiss: () -> Unit,
    onConfirm: () -> Unit,
    modifier: Modifier = Modifier
) = AlertDialog(
    onDismissRequest = onDismiss,
    confirmButton = {
        Button(onClick = onConfirm) { Text("Ausloggen") }
    },
    modifier,
    dismissButton = {
        TextButton(onClick = onDismiss) { Text("Abbrechen") }
    },
    text = {
        Text("Möchtest du das Gerät wirklich ausloggen?")
    }
)



