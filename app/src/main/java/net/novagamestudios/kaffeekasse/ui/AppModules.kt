package net.novagamestudios.kaffeekasse.ui

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.navigator.tab.LocalTabNavigator
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.nearestScreen
import net.novagamestudios.kaffeekasse.AppModule
import net.novagamestudios.kaffeekasse.AppModules
import net.novagamestudios.kaffeekasse.HiwiTrackerModule
import net.novagamestudios.kaffeekasse.KaffeekasseModule
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.SettingsRepository
import net.novagamestudios.kaffeekasse.ui.navigation.AppModulesScreen
import net.novagamestudios.kaffeekasse.ui.navigation.HiwiTrackerNavigation
import net.novagamestudios.kaffeekasse.ui.navigation.KaffeekasseNavigation
import net.novagamestudios.kaffeekasse.ui.navigation.ModuleTab


class AppModulesScreenModel private constructor(
    val session: Session.WithRealUser,
    private val settingsRepository: SettingsRepository,
    val allModules: AppModules
) : ScreenModel {


    private val userSettings by settingsRepository.userSettings
    val modules: AppModules get() = when {
        settingsRepository.value.developerMode -> allModules
        session is Session.WithDevice -> AppModules(allModules.filterIsInstance<KaffeekasseModule>())
        else -> allModules
    }
    private val initialModule get() = modules
        .find { it.id == userSettings?.value?.lastSelectedModule }
        ?: modules.first()


    val AppModule.moduleTab: ModuleTab get() = when (this) {
        is KaffeekasseModule -> KaffeekasseNavigation.Tab(session)
        is HiwiTrackerModule -> HiwiTrackerNavigation.Tab(session)
    }
    val ModuleTab.module: AppModule get() = when (this) {
        is KaffeekasseNavigation.Tab -> allModules.require<KaffeekasseModule>()
        is HiwiTrackerNavigation.Tab -> allModules.require<HiwiTrackerModule>()
    }

    fun initialModuleTab() = initialModule.moduleTab

    fun onNavigateModuleTab(tab: ModuleTab) {
        userSettings?.tryUpdate {
            it.copy(lastSelectedModule = tab.module.id)
        }
    }

    companion object : ScreenModelFactory<RepositoryProvider, AppModulesScreen, AppModulesScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: AppModulesScreen) = AppModulesScreenModel(
            session = screen.session,
            settingsRepository = settingsRepository,
            allModules = modules
        )
    }
}



@Composable
fun AppModuleSelection(
    modifier: Modifier = Modifier,
    model: AppModulesScreenModel = nearestScreen<AppModulesScreen>().model
) {
    val tabNavigator = LocalTabNavigator.current

    val currentModule = with(model) { (tabNavigator.current as? ModuleTab)?.module }
    val hasMultiple = model.modules.size > 1

    var showModuleDropdown by remember { mutableStateOf(false) }

    Row(
        modifier
            .clip(RoundedCornerShape(20.dp))
            .clickable(enabled = hasMultiple) { showModuleDropdown = !showModuleDropdown }
            .padding(start = 8.dp, end = 6.dp)
            .height(40.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        if (currentModule != null) AppModuleLabel(currentModule)
        else Text("Select Module")

        if (hasMultiple) {
            val rotation by animateFloatAsState(
                if (showModuleDropdown) 180f else 0f,
                label = "rotation"
            )
            Icon(
                Icons.Default.ArrowDropDown,
                contentDescription = null,
                Modifier.graphicsLayer { rotationZ = rotation }
            )
        }
    }

    val dropdownItemStyle = LocalTextStyle.current // Take text style from here
    DropdownMenu(
        expanded = showModuleDropdown,
        onDismissRequest = { showModuleDropdown = false },
        Modifier.clip(RoundedCornerShape(20.dp)),
        offset = DpOffset(0.dp, 8.dp)
    ) {
        model.modules.filter { it != currentModule }.forEach { module ->
            DropdownMenuItem(
                text = { AppModuleLabel(module, style = dropdownItemStyle.copy(fontSize = dropdownItemStyle.fontSize * 0.8)) },
                onClick = {
                    tabNavigator.current = with(model) { module.moduleTab }
                    showModuleDropdown = false
                }
            )
        }
    }
}

@Composable
private fun AppModuleLabel(
    module: AppModule,
    modifier: Modifier = Modifier,
    style: TextStyle = LocalTextStyle.current
) = when (module) {
    is KaffeekasseModule -> Text("Kaffeekasse", modifier, style = style)
    is HiwiTrackerModule -> Text("Hiwi Tracker (beta)", modifier, style = style)
}

