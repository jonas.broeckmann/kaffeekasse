package net.novagamestudios.kaffeekasse.ui.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.SaverScope
import androidx.compose.runtime.saveable.rememberSaveable
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer


class SerializableSaver<T>(
    private val serializer: KSerializer<T>,
    private val json: Json = DefaultJson
) : Saver<T, String> {
    override fun restore(value: String): T? = runCatching<T?> {
        json.decodeFromString(serializer, value)
    }.getOrNull()
    override fun SaverScope.save(value: T): String? = runCatching {
        json.encodeToString(serializer, value)
    }.getOrNull()
    companion object {
        val DefaultJson by lazy { Json }
        inline operator fun <reified T> invoke(json: Json = DefaultJson) = SerializableSaver(
            serializer<T>(), json)
    }
}

@Composable
inline fun <reified T : Any> rememberSerializable(
    vararg inputs: Any?,
    key: String? = null,
    noinline init: () -> T
): T = rememberSaveable(
    *inputs,
    saver = SerializableSaver<T>(),
    key = key,
    init = init
)

@Composable
inline fun <reified T> rememberSerializableState(
    vararg inputs: Any?,
    key: String? = null,
    noinline init: () -> MutableState<T>
): MutableState<T> = rememberSaveable(
    *inputs,
    stateSaver = SerializableSaver<T>(),
    key = key,
    init = init
)
