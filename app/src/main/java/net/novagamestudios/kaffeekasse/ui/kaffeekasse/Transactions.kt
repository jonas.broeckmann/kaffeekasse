package net.novagamestudios.kaffeekasse.ui.kaffeekasse

import android.content.Context
import androidx.compose.animation.AnimatedContent
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowRight
import androidx.compose.material.icons.automirrored.filled.List
import androidx.compose.material.icons.filled.BarChart
import androidx.compose.material.icons.filled.OpenInBrowser
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.data.category
import net.novagamestudios.kaffeekasse.data.cleanFullName
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Transaction
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards.CategoryIcon
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.AppSubpageTitle
import net.novagamestudios.kaffeekasse.ui.navigation.KaffeekasseNavigation
import net.novagamestudios.kaffeekasse.ui.util.FailureRetryScreen
import net.novagamestudios.kaffeekasse.ui.util.PullToRefreshBox
import net.novagamestudios.kaffeekasse.ui.util.RichDataContent
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.kaffeekasse.ui.util.openInBrowser
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichStateHere
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class TransactionsScreenModel private constructor(
    private val session: Session.WithRealUser,
    private val kaffeekasse: KaffeekasseRepository
) : ScreenModel, Logger {

    val transactions = kaffeekasse.transactions[session.realUser].collectAsRichStateHere()

    val chartsState by lazy { TransactionsChartsState(screenModelScope, snapshotFlow { transactions.dataOrNull }) }

    var showCharts by mutableStateOf(false)

    fun openInBrowser(context: Context) {
        context.openInBrowser(kaffeekasse.scraper.transactionsUrl)
    }

    companion object : ScreenModelFactory<RepositoryProvider, KaffeekasseNavigation.TransactionsScreen, TransactionsScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: KaffeekasseNavigation.TransactionsScreen) = TransactionsScreenModel(
            session = screen.session,
            kaffeekasse = kaffeekasseRepository
        )
    }
}


class TransactionsContent(provider: ScreenModelProvider<TransactionsScreenModel>) : AppScaffoldContentWithModel<TransactionsScreenModel>(provider) {
    @Composable
    override fun TopAppBarTitle(navigator: Navigator) {
        AppSubpageTitle("Übersicht")
    }

    @Composable
    override fun TopAppBarActions(navigator: Navigator) {
        if (model.showCharts) {
            TransactionsChartsFilter(model.chartsState)
        } else {
            OpenInBrowserButton(model)
        }
        ListChartsToggleButton(model)
    }

    @Composable
    override fun backNavigationHandler(navigator: Navigator): BackNavigationHandler {
        return BackNavigationHandler.default()
    }
}


@Composable
fun Transactions(
    model: TransactionsScreenModel,
    modifier: Modifier = Modifier
) = PullToRefreshBox(
    data = model.transactions,
    modifier.fillMaxSize()
) {
    RichDataContent(
        state = model.transactions,
        errorContent = { error ->
            FailureRetryScreen(
                error = error,
                message = "Failed to fetch transactions",
                Modifier.fillMaxSize()
            )
        }
    ) { transactions ->
        val state1 = rememberLazyListState()
        val state2 = rememberLazyListState()
        AnimatedContent(
            targetState = model.showCharts,
            label = "Transactions",
        ) { showCharts ->
            val lazyListState = if (model.showCharts) state2 else state1
            when {
                showCharts -> TransactionsCharts(
                    state = model.chartsState,
                    lazyListState = lazyListState
                )
                else -> TransactionsList(
                    transactions = transactions,
                    state = lazyListState
                )
            }
        }
        key(Unit) {
            val lazyListState = if (model.showCharts) state2 else state1
            if (lazyListState.canScrollBackward) HorizontalDivider(Modifier.align(Alignment.TopCenter))
        }
    }
}

@Composable
private fun ListChartsToggleButton(
    model: TransactionsScreenModel,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = { model.showCharts = !model.showCharts }, modifier) {
        when (model.showCharts) {
            true -> Icon(Icons.AutoMirrored.Filled.List, "List")
            false -> Icon(Icons.Default.BarChart, "Charts")
        }
    }
}

@Composable
private fun OpenInBrowserButton(
    model: TransactionsScreenModel,
    modifier: Modifier = Modifier
) {
    val context = LocalContext.current
    IconButton(onClick = { model.openInBrowser(context) }, modifier) {
        Icon(Icons.Default.OpenInBrowser, "Open in browser")
    }
}

@Composable
private fun TransactionsList(
    transactions: List<Transaction>,
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState()
) = LazyColumn(modifier, state = state) {
    var currentDay: LocalDate? = null
    transactions.forEach { transaction ->
        val day = transaction.date.toLocalDate()
        val first = currentDay == null
        if (currentDay != day) item {
            if (!first) HorizontalDivider(Modifier.padding(horizontal = 16.dp, vertical = 8.dp))
            TransactionsListDayHeading(day, transaction.newBalance)
        }
        currentDay = day
        item {
            TransactionListItem(transaction)
        }
    }
}

@Composable
private fun TransactionsListDayHeading(
    day: LocalDate,
    newBalance: Double,
    modifier: Modifier = Modifier
) = Row(
    modifier.padding(horizontal = 16.dp, vertical = 12.dp),
    verticalAlignment = Alignment.CenterVertically
) {
    Text(
        remember(day) { day.format(DateTimeFormatter.ofPattern("EEEE, d. MMMM uuuu")) },
        style = MaterialTheme.typography.titleMedium,
    )
    Spacer(
        Modifier
            .weight(1f)
            .width(16.dp))
    Text(remember(newBalance) { String.format("%.2f€", newBalance) })
}

@Composable
private fun TransactionListItem(
    transaction: Transaction,
    modifier: Modifier = Modifier
) = ListItem(
    headlineContent = {
        val style = LocalTextStyle.current
        when (val purpose = transaction.purpose) {
            is Transaction.Purpose.Purchase -> Row(verticalAlignment = Alignment.CenterVertically) {
                val knownItems = purpose.knownItems
                Text("${purpose.itemCount}x", Modifier.padding(end = 8.dp))
                if (knownItems.isNotEmpty()) {
                    CategoryIcon(knownItems.first().category,
                        Modifier
                            .padding(end = 4.dp)
                            .scale(0.8f))
                }
                if (knownItems.isEmpty()) {
                    Text(purpose.itemName, style = style)
                } else {
                    Text(knownItems.joinToString("/") { it.cleanFullName }, style = style)
                }
                if (App.developerMode) ProvideTextStyle(MaterialTheme.typography.labelSmall) {
                    if (knownItems.isEmpty()) Text(
                        "Unknown",
                        Modifier.padding(start = 4.dp),
                        color = Color.Yellow
                    )
                }
            }
            is Transaction.Purpose.Deposit -> Text("Einzahlung", style = style)
            is Transaction.Purpose.Other -> Text(purpose.description, style = style)
        }
    },
    modifier,
    supportingContent = {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                remember(transaction.date) {
                    transaction.date.format(DateTimeFormatter.ofPattern("HH:mm"))
                },
                Modifier.padding(end = 8.dp)
            )
            if (transaction.payer != null) Text(transaction.payer)
            Icon(Icons.AutoMirrored.Filled.ArrowRight, null)
            Text(transaction.payee)
        }
    },
    trailingContent = {
        Text(
            text = remember(transaction.total) { String.format("%+.2f€", transaction.total) },
            color = when {
                transaction.total < 0 -> Color.Red
                transaction.total > 0 -> Color.Green
                else -> Color.Unspecified
            },
            fontSize = MaterialTheme.typography.bodyLarge.fontSize
        )
    }
)



