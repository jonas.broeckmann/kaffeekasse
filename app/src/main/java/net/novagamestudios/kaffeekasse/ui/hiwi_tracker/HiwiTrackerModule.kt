package net.novagamestudios.kaffeekasse.ui.hiwi_tracker

import cafe.adriel.voyager.core.model.ScreenModel
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.HiwiTrackerNavigation


class HiwiTrackerModuleScreenModel private constructor(
    val session: Session
) : ScreenModel, Logger {

    companion object : ScreenModelFactory<RepositoryProvider, HiwiTrackerNavigation.Tab, HiwiTrackerModuleScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: HiwiTrackerNavigation.Tab) = HiwiTrackerModuleScreenModel(
            session = screen.session
        )
    }
}

class HiwiTrackerModuleContent(provider: ScreenModelProvider<HiwiTrackerModuleScreenModel>) : AppScaffoldContentWithModel<HiwiTrackerModuleScreenModel>(provider)
