package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.model.kaffeekasse.PurchaseAccount


class AccountSelectionState<out PA : PurchaseAccount>(
    val accounts: List<PA>,
    initialIndex: Int
) {
    var selectedIndex by mutableIntStateOf(initialIndex)
    val selectedAccount: PA? get() = accounts.getOrNull(selectedIndex)
}


@Composable
fun AccountSelection(
    state: AccountSelectionState<*>,
    modifier: Modifier = Modifier
) {
    var expanded by remember { mutableStateOf(false) }
    ExposedDropdownMenuBox(
        expanded,
        onExpandedChange = { expanded = it },
        modifier
    ) {
        OutlinedTextField(
            state.selectedAccount?.name?.let { "$it" } ?: "",
            onValueChange = { },
            Modifier
                .menuAnchor(MenuAnchorType.PrimaryNotEditable),
            readOnly = true,
            label = { Text("Konto") },
            leadingIcon = { Icon(Icons.Default.Person, "Konto") },
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded) }
        )
        ExposedDropdownMenu(
            expanded,
            onDismissRequest = { expanded = false }
        ) {
            state.accounts.forEachIndexed { index, account ->
                DropdownMenuItem(
                    text = {
                        val str = listOfNotNull(
                            "${account.name}",
                            if (App.developerMode) listOfNotNull(
                                "${account.id}",
                                when {
                                    account is PurchaseAccount.WithDefault && account.isDefault -> "default"
                                    account is PurchaseAccount.WithDefault && !account.isDefault -> "not default"
                                    else -> "unknown default"
                                }
                            ).joinToString(";", "(", ")") else null
                        ).joinToString(" ")
                        Text(str)
                    },
                    onClick = {
                        state.selectedIndex = index
                        expanded = false
                    }
                )
            }
        }
    }
}
