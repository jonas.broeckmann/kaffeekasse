package net.novagamestudios.kaffeekasse.ui.login

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Logout
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import cafe.adriel.voyager.navigator.Navigator
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.components.CircularLoadingBox
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.common_utils.voyager.model.collectAsStateHere
import net.novagamestudios.common_utils.voyager.scaffold.currentScaffoldContent
import net.novagamestudios.kaffeekasse.app
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.LoginRepository
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.PortalRepository
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.LoginNavigation


class LoginScreenModel private constructor(
    private val loginRepository: LoginRepository,
    private val portal: PortalRepository
) : ScreenModel, Logger {

    val session by portal.session

    val isLoading by loginRepository.isPerformingAction.collectAsStateHere()

    val loginDeviceState = LoginDeviceState(screenModelScope, loginRepository)

    companion object : ScreenModelFactory<RepositoryProvider, LoginNavigation.Companion, LoginScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: LoginNavigation.Companion) = LoginScreenModel(
            loginRepository = loginRepository,
            portal = portalRepository
        )
    }
}

class LoginContent(provider: ScreenModelProvider<LoginScreenModel>) : AppScaffoldContentWithModel<LoginScreenModel>(provider) {
    @Composable
    override fun TopAppBarNavigationIcon(navigator: Navigator) { }

    @Composable
    override fun backNavigationHandler(navigator: Navigator): BackNavigationHandler {
        return navigator.currentScaffoldContent()?.backNavigationHandler(navigator) ?: BackNavigationHandler.default()
    }
}

@Composable
fun LoginAdditional(model: LoginScreenModel, navigator: Navigator) {
    when (navigator.lastItem) {
        is LoginNavigation.FormScreen -> {
            val session = model.session
            if (session is Session.WithDevice) navigator.replace(LoginNavigation.UserSelectionScreen(session.device))
        }
        is LoginNavigation.UserSelectionScreen -> {
            val session = model.session
            if (session !is Session.WithDevice) navigator.replace(LoginNavigation.FormScreen)
        }
    }
    LoginDeviceAdditional(model.loginDeviceState)
}


@Composable
fun LogoutTopBarAction(session: Session) = with(app()) {
    val isLoading by loginRepository.isPerformingAction.collectAsState()
    CircularLoadingBox(loading = isLoading) {
        IconButton(
            onClick = { launch { loginRepository.logoutUser() } },
            enabled = session !is Session.Empty
        ) {
            Icon(
                Icons.AutoMirrored.Filled.Logout,
                contentDescription = "Ausloggen",
                tint = MaterialTheme.colorScheme.primary
            )
        }
    }
}

