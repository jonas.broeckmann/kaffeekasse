package net.novagamestudios.kaffeekasse.ui.hiwi_tracker

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.navigationBars
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowRight
import androidx.compose.material.icons.automirrored.filled.Send
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.SegmentedButton
import androidx.compose.material3.SegmentedButtonDefaults
import androidx.compose.material3.SingleChoiceSegmentedButtonRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalTime
import kotlinx.datetime.format
import kotlinx.datetime.format.DayOfWeekNames
import kotlinx.datetime.format.MonthNames
import kotlinx.datetime.format.Padding
import kotlinx.datetime.toKotlinLocalDate
import kotlinx.datetime.toKotlinLocalTime
import net.novagamestudios.common_utils.compose.Toasts
import net.novagamestudios.common_utils.compose.ToastsState
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.compose.components.CircularLoadingBox
import net.novagamestudios.common_utils.compose.components.ColumnCenter
import net.novagamestudios.common_utils.compose.components.RowCenter
import net.novagamestudios.common_utils.compose.state.ReentrantActionState
import net.novagamestudios.common_utils.compose.thenIf
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.common_utils.core.minus
import net.novagamestudios.kaffeekasse.R
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.MaxWorkWithoutLargeBreak
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.MaxWorkWithoutSmallBreak
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.MinLargeBreakTime
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.MinSmallBreakTime
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.invalidLargeBreak
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.invalidSmallBreak
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry.Companion.isValid
import net.novagamestudios.kaffeekasse.repositories.i11.HiwiTrackerRepository
import net.novagamestudios.kaffeekasse.ui.theme.disabled
import net.novagamestudios.kaffeekasse.ui.util.ClockFace
import net.novagamestudios.kaffeekasse.ui.util.TimePickerState
import net.novagamestudios.kaffeekasse.ui.util.monochrome
import kotlin.math.roundToInt
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds


class EnterWorkingHoursState(
    private val coroutineScope: CoroutineScope,
    private val hiwiTracker: HiwiTrackerRepository,
    internal val date: LocalDate,
    private val onSubmitted: suspend () -> Unit
) : Logger {

    internal val formattedDate: String by lazy {
        date.format(LocalDate.Format {
            dayOfWeek(DayOfWeekNames.GERMAN_FULL)
            chars(", ")
            dayOfMonth(Padding.NONE)
            chars(". ")
            monthName(MonthNames.GERMAN_FULL)
        })
    }

    private val initialTime by lazy {
        LocalTime.now().nearestMinutesInterval(15)
    }

    internal var fromTime by mutableStateOf(initialTime)
    internal var toTime by mutableStateOf(initialTime)
    internal var breakDuration by mutableStateOf(Duration.ZERO)
    internal var currentTimePickerDialog by mutableStateOf<TimePickerDialogState?>(null)
    internal var comment by mutableStateOf("")


    internal val workEntry by derivedStateOf {
        WorkEntry(
            id = null,
            date = date,
            begin = fromTime,
            end = toTime,
            breakTime = breakDuration.takeIf { it.isPositive() },
            note = comment
        )
    }


    private val loadingMutex = ReentrantActionState()
    val isLoading by loadingMutex

    val toasts = ToastsState()
    var indicateSuccess by mutableStateOf(false)

    fun submitWorkEntry(entry: WorkEntry) {
        if (!entry.isValid) return
        coroutineScope.launch {
            val success = loadingMutex.trueWhile {
                try {
                    hiwiTracker.submitWorkEntry(entry)
                    true
                } catch (e: Exception) {
                    warn(e) { "Failed to submit entry" }
                    toasts.long("Fehler: ${e.message ?: "Unbekannter Fehler"}")
                    false
                }
            }
            if (success) {
                indicateSuccess = true
                delay(0.5.seconds)
                onSubmitted()
                indicateSuccess = false
            }
        }
    }
}

internal class TimePickerDialogState(
    initialFromTime: LocalTime,
    initialToTime: LocalTime,
    initialMode: Mode
) {
    enum class Mode { From, To }

    val fromTimePicker = TimePickerState(initialFromTime, true)
    val toTimePicker = TimePickerState(initialToTime, true)

    // TODO somehow apply rounding to the time pickers
    val roundedFromTime by derivedStateOf { fromTimePicker.time.nearestMinutesInterval(5) }
    val roundedToTime by derivedStateOf { toTimePicker.time.nearestMinutesInterval(5) }

    var mode by mutableStateOf(initialMode)

    val duration by derivedStateOf { (roundedToTime - roundedFromTime).coerceAtLeast(Duration.ZERO) }
}

@Composable
fun EnterWorkingHoursForm(
    state: EnterWorkingHoursState,
    modifier: Modifier = Modifier
) = Column(
    modifier
        .verticalScroll(rememberScrollState())
        .fillMaxWidth()
        .windowInsetsPadding(WindowInsets.navigationBars)
        .heightIn(min = 300.dp)
        .padding(top = 4.dp, bottom = 20.dp),
    horizontalAlignment = Alignment.CenterHorizontally,
) {
    // Title
    Text(
        state.formattedDate,
        Modifier.padding(vertical = 12.dp),
        style = MaterialTheme.typography.headlineMedium
    )
    HorizontalDivider(Modifier.padding(vertical = 12.dp))
    FormFromTo(state, Modifier.padding(horizontal = 24.dp, vertical = 16.dp))
    FormBreakDuration(state, Modifier.padding(vertical = 16.dp))
    FormComment(
        state,
        Modifier
            .padding(top = 4.dp, bottom = 12.dp)
            .padding(horizontal = 24.dp)
            .widthIn(max = 400.dp)
            .fillMaxWidth()
    )
    FormSubmitAndStatus(
        state,
        onSubmit = { state.submitWorkEntry(state.workEntry) },
        Modifier.padding(horizontal = 24.dp, vertical = 12.dp)
    )
    // Dialog
    state.currentTimePickerDialog?.let {
        TimerPickerDialog(
            it,
            onDismiss = { state.currentTimePickerDialog = null },
            onConfirm = {
                state.fromTime = it.roundedFromTime
                state.toTime = it.roundedToTime.coerceAtLeast(state.fromTime)
                state.currentTimePickerDialog = null
            }
        )
    }
    Toasts(state.toasts)
}

@Composable
private fun FromTo(
    fromTime: LocalTime,
    toTime: LocalTime,
    onClickFrom: (() -> Unit)?,
    onClickTo: (() -> Unit)?,
    duration: Duration?,
    modifier: Modifier = Modifier,
    highlightFrom: Boolean = false,
    highlightTo: Boolean = false
) = RowCenter(
    modifier,
    horizontalArrangement = Arrangement.spacedBy(8.dp)
) {
    TimePickerButton(
        time = fromTime,
        onClick = onClickFrom,
        highlight = highlightFrom
    )
    Icon(Icons.AutoMirrored.Default.ArrowRight, null)
    if (duration != null) {
        ColumnCenter(Modifier.widthIn(min = 64.dp)) {
            Text(duration.fHT(), style = MaterialTheme.typography.titleMedium)
        }
        Icon(Icons.AutoMirrored.Default.ArrowRight, null)
    }
    TimePickerButton(
        time = toTime,
        onClick = onClickTo,
        highlight = highlightTo
    )
}

@Composable
private fun FormFromTo(
    state: EnterWorkingHoursState,
    modifier: Modifier = Modifier
) = FromTo(
    fromTime = state.fromTime,
    toTime = state.toTime,
    onClickFrom = {
        state.currentTimePickerDialog = TimePickerDialogState(
            initialFromTime = state.fromTime,
            initialToTime = state.toTime,
            initialMode = TimePickerDialogState.Mode.From
        )
    },
    onClickTo = {
        state.currentTimePickerDialog = TimePickerDialogState(
            initialFromTime = state.fromTime,
            initialToTime = state.toTime,
            initialMode = TimePickerDialogState.Mode.To
        )
    },
    duration = state.workEntry.workedDuration,
    modifier = modifier
)

@Composable
private fun FormBreakDuration(
    state: EnterWorkingHoursState,
    modifier: Modifier = Modifier
) = RowCenter(
    modifier,
    horizontalArrangement = Arrangement.spacedBy(16.dp)
) {
    ProvideTextStyle(MaterialTheme.typography.titleLarge) {
        Image(
            painterResource(R.drawable.logo_edited),
            null,
            Modifier.size(48.dp),
            colorFilter = ColorFilter.monochrome(LocalContentColor.current)
        )
        Text("Pausenzeit:")
        DurationInput(
            state.breakDuration,
            { state.breakDuration = it },
            Modifier.widthIn(max = 100.dp)
        )
    }
}

@Composable
private fun FormComment(
    state: EnterWorkingHoursState,
    modifier: Modifier = Modifier
) = OutlinedTextField(
    state.comment,
    onValueChange = { state.comment = it },
    modifier,
    label = { Text("Was?") },
    minLines = 3
)

@Composable
private fun FormSubmitAndStatus(
    state: EnterWorkingHoursState,
    onSubmit: () -> Unit,
    modifier: Modifier = Modifier
) = Row(modifier) {
    ProvideTextStyle(MaterialTheme.typography.bodySmall.copy(
        color = MaterialTheme.colorScheme.error
    )) {
        Column(
            Modifier
                .weight(1f)
                .padding(end = 16.dp)) {
            if (state.workEntry.invalidLargeBreak) Text("Ab ${MaxWorkWithoutLargeBreak.fHT()} Arbeitszeit muss die Pausenzeit mindestens ${MinLargeBreakTime.fHT()} betragen!")
            else if (state.workEntry.invalidSmallBreak) Text("Ab ${MaxWorkWithoutSmallBreak.fHT()} Arbeitszeit muss die Pausenzeit mindestens ${MinSmallBreakTime.fHT()} betragen!")
            if (state.workEntry.workedDuration.isNegative()) Text("Arbeitszeit muss positiv sein!")
        }
    }
    if (state.indicateSuccess) BoxCenter(Modifier.size(56.dp)) {
        Icon(Icons.Default.Check, "Success", tint = Color.Green)
    } else CircularLoadingBox(state.isLoading) {
        val canSubmit = state.workEntry.isValid
        val fabColor = FloatingActionButtonDefaults.containerColor
        FloatingActionButton(
            onClick = { if (canSubmit) onSubmit() },
            containerColor = fabColor.disabled(!canSubmit),
            contentColor = contentColorFor(fabColor).disabled(!canSubmit),
            elevation = FloatingActionButtonDefaults.bottomAppBarFabElevation(),
        ) {
            Icon(Icons.AutoMirrored.Filled.Send, "Submit")
        }
    }
}


@Composable
private fun TimePickerButton(
    time: LocalTime,
    onClick: (() -> Unit)?,
    highlight: Boolean,
    modifier: Modifier = Modifier
) = Box(
    modifier
        .thenIf(highlight) {
            border(
                2.dp,
                MaterialTheme.colorScheme.primary,
                RoundedCornerShape(8.dp)
            )
        }
        .clip(RoundedCornerShape(8.dp))
        .background(MaterialTheme.colorScheme.surfaceContainerHigh)
        .clickable(enabled = onClick != null, onClick = { onClick?.invoke() })
        .padding(10.dp)
) {
    Text(
        time.fHT(),
        Modifier,
        style = MaterialTheme.typography.headlineLarge
    )
}

@Composable
private fun TimerPickerDialog(
    state: TimePickerDialogState,
    onDismiss: () -> Unit,
    onConfirm: () -> Unit
) = AlertDialog(
    onDismissRequest = onDismiss,
    confirmButton = {
        TextButton(onClick = onConfirm, enabled = state.roundedFromTime <= state.roundedToTime) { Text("OK") }
    },
    dismissButton = { TextButton(onClick = onDismiss) { Text("Abbrechen") } },
    title = {
        ColumnCenter(Modifier.fillMaxWidth()) {
            FromTo(
                fromTime = state.roundedFromTime,
                toTime = state.roundedToTime,
                onClickFrom = { state.mode = TimePickerDialogState.Mode.From },
                onClickTo = { state.mode = TimePickerDialogState.Mode.To },
                duration = null,
                highlightFrom = state.mode == TimePickerDialogState.Mode.From,
                highlightTo = state.mode == TimePickerDialogState.Mode.To
            )
            Spacer(Modifier.height(12.dp))
            Row(Modifier.padding(horizontal = 8.dp)) {
                ProvideTextStyle(MaterialTheme.typography.titleLarge) {
                    Text("Arbeitszeit:", Modifier.padding(end = 16.dp))
                    Text(state.duration.fHT())
                }
            }
        }
    },
    text = {
        ColumnCenter(Modifier.fillMaxWidth()) {
            val currentTimePickerState = when (state.mode) {
                TimePickerDialogState.Mode.From -> state.fromTimePicker
                TimePickerDialogState.Mode.To -> state.toTimePicker
            }
            ClockFace(
                currentTimePickerState,
                Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(bottom = 16.dp)
            )
            val selections = listOf(
                TimePickerState.Selection.Hour to "Stunden",
                TimePickerState.Selection.Minute to "Minuten"
            )
            SingleChoiceSegmentedButtonRow {
                selections.forEachIndexed { i, (selection, text) ->
                    SegmentedButton(
                        selected = currentTimePickerState.selection == selection,
                        onClick = { currentTimePickerState.selection = selection },
                        shape = SegmentedButtonDefaults.itemShape(i, selections.size),
                        icon = { }
                    ) {
                        Text(text)
                    }
                }
            }
        }
    }
)




private val HoursRange = 0..0
private val MinutesRange = 1..2
private fun String.durationComponents(): Pair<String, String> {
    return sliceOrEmpty(HoursRange) to sliceOrEmpty(MinutesRange)
}
private fun String.sliceOrEmpty(range: IntRange): String {
    val boundedRange = range.first.coerceAtLeast(indices.first)..range.last.coerceAtMost(indices.last)
    return slice(boundedRange)
}
private fun Duration.toStr(): String {
    return toComponents { hours, minutes, _, _ ->
        "${hours.coerceAtMost(9)}${"$minutes".padStart(2, '0')}"
    }
}

private fun String.offsetMapping() = object : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int {
        if (offset <= HoursRange.last) return offset
        return offset + 1
    }
    override fun transformedToOriginal(offset: Int): Int {
        return this@offsetMapping.length
    }
}

@Composable
private fun DurationInput(
    initialDuration: Duration,
    onDurationChange: (Duration) -> Unit,
    modifier: Modifier = Modifier
) {
    var textFieldValue by remember {
        val str = if (initialDuration.isPositive()) initialDuration.toStr() else ""
        mutableStateOf(TextFieldValue(str))
    }
    val textColor = LocalContentColor.current
    OutlinedTextField(
        textFieldValue,
        onValueChange = { new ->
            val (hours, minutes) = new.text.durationComponents()
            val hoursDuration = (hours.toIntOrNull() ?: 0).hours
            val minutesDuration = (minutes.padEnd(MinutesRange.count(), '0').toIntOrNull() ?: 0).minutes
            val duration = hoursDuration + minutesDuration
            textFieldValue = new.copy(text = duration.toStr().take(new.text.length))
            onDurationChange(duration)
        },
        modifier,
        visualTransformation = { str ->
            val (hours, minutes) = str.toString().durationComponents()
            val annotated = AnnotatedString.Builder().apply {
                append(hours)
                pushStyle(SpanStyle(color = textColor.disabled()))
                repeat(HoursRange.count() - hours.length) {
                    append('0')
                }
                pop()
                append("h")
                append(minutes)
                pushStyle(SpanStyle(color = textColor.disabled()))
                repeat(MinutesRange.count() - minutes.length) {
                    append('0')
                }
                pop()
                append("m")
            }.toAnnotatedString()
            TransformedText(
                annotated,
                str.toString().offsetMapping()
            )
        },
        keyboardOptions = KeyboardOptions(
            autoCorrectEnabled = false,
            keyboardType = KeyboardType.Number,
            imeAction = ImeAction.Done
        ),
        textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.Center),
    )
}

private fun LocalTime.nearestMinutesInterval(n: Int): LocalTime {
    require(60 % n == 0) { "n must be a divisor of 60" }
    val roundedMinute = (minute / n.toFloat()).roundToInt() * n
    if (roundedMinute >= 60) {
        if (hour == 23) return java.time.LocalTime.MAX.toKotlinLocalTime()
        return LocalTime(hour + 1, roundedMinute % 60)
    }
    return LocalTime(hour, roundedMinute)
}


// TODO move
val DayOfWeekNames.Companion.GERMAN_FULL get() = DayOfWeekNames(
    monday = "Montag",
    tuesday = "Dienstag",
    wednesday = "Mittwoch",
    thursday = "Donnerstag",
    friday = "Freitag",
    saturday = "Samstag",
    sunday = "Sonntag"
)
val MonthNames.Companion.GERMAN_FULL get() = MonthNames(
    january = "Januar",
    february = "Februar",
    march = "März",
    april = "April",
    may = "Mai",
    june = "Juni",
    july = "Juli",
    august = "August",
    september = "September",
    october = "Oktober",
    november = "November",
    december = "Dezember"
)
fun LocalDate.Companion.now() = java.time.LocalDate.now().toKotlinLocalDate()
fun LocalTime.Companion.now() = java.time.LocalTime.now().toKotlinLocalTime()

