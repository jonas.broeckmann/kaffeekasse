package net.novagamestudios.kaffeekasse.ui.util

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.MutatePriority
import androidx.compose.foundation.MutatorMutex
import androidx.compose.foundation.background
import androidx.compose.foundation.focusable
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TimePickerLayoutType
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.setValue
import androidx.compose.runtime.structuralEqualityPolicy
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.takeOrElse
import androidx.compose.ui.input.pointer.PointerEvent
import androidx.compose.ui.input.pointer.PointerEventPass
import androidx.compose.ui.input.pointer.SuspendingPointerInputModifierNode
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.boundsInParent
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.node.CompositionLocalConsumerModifierNode
import androidx.compose.ui.node.DelegatingNode
import androidx.compose.ui.node.LayoutAwareModifierNode
import androidx.compose.ui.node.ModifierNodeElement
import androidx.compose.ui.node.PointerInputModifierNode
import androidx.compose.ui.node.requireDensity
import androidx.compose.ui.platform.InspectorInfo
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.semantics.onClick
import androidx.compose.ui.semantics.selectableGroup
import androidx.compose.ui.semantics.selected
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.traversalIndex
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.center
import androidx.compose.ui.unit.dp
import androidx.compose.ui.util.fastFilter
import androidx.compose.ui.util.fastFirstOrNull
import androidx.compose.ui.util.fastForEachIndexed
import androidx.compose.ui.util.fastMap
import kotlinx.coroutines.launch
import kotlinx.datetime.LocalTime
import net.novagamestudios.kaffeekasse.ui.util.TimePickerState.Selection
import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.hypot
import kotlin.math.roundToInt
import kotlin.math.sin



/**
 * A class to handle state changes in a [TimePicker]
 *
 * @param initialHour
 *  starting hour for this state, will be displayed in the time picker when launched
 *  Ranges from 0 to 23
 * @param initialMinute
 *  starting minute for this state, will be displayed in the time picker when launched.
 *  Ranges from 0 to 59
 * @param is24Hour The format for this time picker `false` for 12 hour format with an AM/PM toggle
 *  or `true` for 24 hour format without toggle.
 */
@Stable
@ExperimentalMaterial3Api
class TimePickerState(
    initialTime: LocalTime,
    is24Hour: Boolean,
) {

    var minute: Int
        get() = minuteAngle.toMinute()
        set(value) { setMinute(value) }
    var hour: Int
        get() = hourAngle.toHour() + if (isAfternoon) 12 else 0
        set(value) { setHour(value) }
    val is24hour: Boolean = is24Hour

    val time: LocalTime by derivedStateOf { LocalTime(hour, minute) }

    internal val hourForDisplay: Int get() = hourForDisplay(hour)
    internal val selectorPos by derivedStateOf(structuralEqualityPolicy()) {
        val inInnerCircle = isInnerCircle
        val handleRadiusPx = ClockDialSelectorHandleContainerSize / 2
        val selectorLength = if (is24Hour && inInnerCircle && selection == Selection.Hour) {
            InnerCircleRadius
        } else {
            OuterCircleSizeRadius
        }.minus(handleRadiusPx)

        val length = selectorLength + handleRadiusPx
        val offsetX = length * cos(currentAngle.value) + ClockDialContainerSize / 2
        val offsetY = length * sin(currentAngle.value) + ClockDialContainerSize / 2

        DpOffset(offsetX, offsetY)
    }

    internal var center by mutableStateOf(IntOffset.Zero)
    internal val values get() = if (selection == Selection.Minute) Minutes else Hours

    var selection by mutableStateOf(Selection.Hour)
    internal var isAfternoonToggle by mutableStateOf(initialTime.hour >= 12 && !is24Hour)
    internal var isInnerCircle by mutableStateOf(initialTime.hour >= 12)

    internal var hourAngle by mutableFloatStateOf(
        RadiansPerHour * (initialTime.hour % 12) - FullCircle / 4
    )
    internal var minuteAngle by mutableFloatStateOf(
        RadiansPerMinute * initialTime.minute - FullCircle / 4
    )

    private val mutex = MutatorMutex()
    private val isAfternoon by derivedStateOf { is24hour && isInnerCircle || isAfternoonToggle }

    internal val currentAngle = Animatable(hourAngle)

    internal fun setMinute(minute: Int) {
        minuteAngle = RadiansPerMinute * minute - FullCircle / 4
    }

    internal fun setHour(hour: Int) {
        isInnerCircle = hour >= 12
        hourAngle = RadiansPerHour * (hour % 12) - FullCircle / 4
    }

    internal fun moveSelector(x: Float, y: Float, maxDist: Float) {
        if (selection == Selection.Hour && is24hour) {
            isInnerCircle = dist(x, y, center.x, center.y) < maxDist
        }
    }

    internal fun isSelected(value: Int): Boolean =
        if (selection == Selection.Minute) {
            value == minute
        } else {
            hour == (value + if (isAfternoon) 12 else 0)
        }

    internal suspend fun update(value: Float, fromTap: Boolean = false) {
        mutex.mutate(MutatePriority.UserInput) {
            if (selection == Selection.Hour) {
                hourAngle = value.toHour() % 12 * RadiansPerHour
            } else if (fromTap) {
                minuteAngle = (value.toMinute() - value.toMinute() % 5) * RadiansPerMinute
            } else {
                minuteAngle = value.toMinute() * RadiansPerMinute
            }

            if (fromTap) {
                currentAngle.snapTo(minuteAngle)
            } else {
                currentAngle.snapTo(offsetHour(value))
            }
        }
    }

    internal suspend fun animateToCurrent() {
        val (start, end) = if (selection == Selection.Hour) {
            valuesForAnimation(minuteAngle, hourAngle)
        } else {
            valuesForAnimation(hourAngle, minuteAngle)
        }

        currentAngle.snapTo(start)
        currentAngle.animateTo(end, tween(200))
    }

    private fun hourForDisplay(hour: Int): Int = when {
        is24hour -> hour % 24
        hour % 12 == 0 -> 12
        isAfternoon -> hour - 12
        else -> hour
    }

    private fun offsetHour(angle: Float): Float {
        val ret = angle + QuarterCircle.toFloat()
        return if (ret < 0) ret + FullCircle else ret
    }

    private fun Float.toHour(): Int {
        val hourOffset: Float = RadiansPerHour / 2
        val totalOffset = hourOffset + QuarterCircle
        return ((this + totalOffset) / RadiansPerHour).toInt() % 12
    }
    private fun Float.toMinute(): Int {
        val minuteOffset: Float = RadiansPerMinute / 2
        val totalOffset = minuteOffset + QuarterCircle
        return ((this + totalOffset) / RadiansPerMinute).toInt() % 60
    }


    suspend fun settle() {
        val targetValue = valuesForAnimation(currentAngle.value, minuteAngle)
        currentAngle.snapTo(targetValue.first)
        currentAngle.animateTo(targetValue.second, tween(200))
    }

    internal suspend fun onTap(x: Float, y: Float, maxDist: Float, autoSwitchToMinute: Boolean) {
        update(atan(y - center.y, x - center.x), true)
        moveSelector(x, y, maxDist)

        if (selection == Selection.Hour) {
            if (autoSwitchToMinute) {
                selection = Selection.Minute
            } else {
                val targetValue = valuesForAnimation(currentAngle.value, hourAngle)
                currentAngle.snapTo(targetValue.first)
                currentAngle.animateTo(targetValue.second, tween(200))
            }
        } else {
            settle()
        }
    }

    companion object {
        /**
         * The default [Saver] implementation for [TimePickerState].
         */
        fun Saver(): Saver<TimePickerState, *> = Saver(
            save = {
                listOf(
                    it.hour,
                    it.minute,
                    it.is24hour
                )
            },
            restore = { value ->
                TimePickerState(
                    LocalTime(
                        hour = value[0] as Int,
                        minute = value[1] as Int
                    ),
                    is24Hour = value[2] as Boolean
                )
            }
        )
    }

    @JvmInline
    value class Selection private constructor(val value: Int) {
        companion object {
            val Hour = Selection(0)
            val Minute = Selection(1)
        }
    }
}


@Composable
fun ClockFace(
    state: TimePickerState,
    modifier: Modifier = Modifier,
    colors: TimePickerColors = TimePickerDefaults.colors(),
    autoSwitchToMinute: Boolean = true
) {
    Crossfade(
        modifier = modifier
            .background(shape = CircleShape, color = colors.clockDialColor)
            .size(ClockDialContainerSize)
            .semantics { selectableGroup() },
        targetState = state.values,
        animationSpec = tween(durationMillis = 350),
        label = "Clock Face"
    ) { screen ->
        CircularLayout(
            modifier = Modifier
                .then(ClockDialModifier(state, autoSwitchToMinute))
                .size(ClockDialContainerSize)
                .drawSelector(state, colors),
            radius = OuterCircleSizeRadius,
        ) {
            CompositionLocalProvider(
                LocalContentColor provides colors.clockDialContentColor(false)
            ) {
                repeat(screen.size) { index ->
                    val outerValue = if (!state.is24hour || state.selection == TimePickerState.Selection.Minute) {
                        screen[index]
                    } else {
                        screen[index] % 12
                    }
                    ClockText(
                        modifier = Modifier.semantics {
                            traversalIndex = index.toFloat()
                        },
                        state = state,
                        value = outerValue,
                        autoSwitchToMinute = autoSwitchToMinute
                    )
                }

                if (state.selection == Selection.Hour && state.is24hour) {
                    CircularLayout(
                        modifier = Modifier
                            .layoutId(LayoutId.InnerCircle)
                            .size(ClockDialContainerSize)
                            .background(shape = CircleShape, color = Color.Transparent),
                        radius = InnerCircleRadius
                    ) {
                        repeat(ExtraHours.size) { index ->
                            val innerValue = ExtraHours[index]
                            ClockText(
                                modifier = Modifier.semantics {
                                    traversalIndex = 12 + index.toFloat()
                                },
                                state = state,
                                value = innerValue,
                                autoSwitchToMinute = autoSwitchToMinute
                            )
                        }
                    }
                }
            }
        }
    }
}


/** Distribute elements evenly on a circle of [radius] */
@Composable
private fun CircularLayout(
    modifier: Modifier = Modifier,
    radius: Dp,
    content: @Composable () -> Unit,
) {
    Layout(
        modifier = modifier, content = content
    ) { measurables, constraints ->
        val radiusPx = radius.toPx()
        val itemConstraints = constraints.copy(minWidth = 0, minHeight = 0)
        val placeables = measurables.fastFilter {
            it.layoutId != LayoutId.Selector && it.layoutId != LayoutId.InnerCircle
        }.fastMap { measurable -> measurable.measure(itemConstraints) }
        val selectorMeasurable = measurables.fastFirstOrNull { it.layoutId == LayoutId.Selector }
        val innerMeasurable = measurables.fastFirstOrNull { it.layoutId == LayoutId.InnerCircle }
        val theta = FullCircle / (placeables.count())
        val selectorPlaceable = selectorMeasurable?.measure(itemConstraints)
        val innerCirclePlaceable = innerMeasurable?.measure(itemConstraints)

        layout(
            width = constraints.minWidth,
            height = constraints.minHeight,
        ) {
            selectorPlaceable?.place(0, 0)

            placeables.fastForEachIndexed { i, it ->
                val centerOffsetX = constraints.maxWidth / 2 - it.width / 2
                val centerOffsetY = constraints.maxHeight / 2 - it.height / 2
                val offsetX = radiusPx * cos(theta * i - QuarterCircle) + centerOffsetX
                val offsetY = radiusPx * sin(theta * i - QuarterCircle) + centerOffsetY
                it.place(
                    x = offsetX.roundToInt(), y = offsetY.roundToInt()
                )
            }

            innerCirclePlaceable?.place(
                (constraints.minWidth - innerCirclePlaceable.width) / 2,
                (constraints.minHeight - innerCirclePlaceable.height) / 2
            )
        }
    }
}

@Composable
private fun ClockText(
    modifier: Modifier,
    state: TimePickerState,
    value: Int,
    autoSwitchToMinute: Boolean
) {
    val style = MaterialTheme.typography.bodyLarge
    val maxDist = with(LocalDensity.current) { MaxDistance.toPx() }
    var center by remember { mutableStateOf(Offset.Zero) }
    val scope = rememberCoroutineScope()

    val text = value.toString()
    val selected = if (state.selection == Selection.Minute) {
        state.minute.toString() == text
    } else {
        state.hour.toString() == text
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .minimumInteractiveComponentSize()
            .size(MinimumInteractiveSize)
            .onGloballyPositioned { center = it.boundsInParent().center }
            .focusable()
            .semantics(mergeDescendants = true) {
                onClick {
                    scope.launch { state.onTap(center.x, center.y, maxDist, autoSwitchToMinute) }
                    true
                }
                this.selected = selected
            }
    ) {
        Text(
            text = text,
            style = style,
        )
    }
}


private fun Modifier.drawSelector(
    state: TimePickerState,
    colors: TimePickerColors,
): Modifier = this.drawWithContent {
    val selectorOffsetPx = Offset(state.selectorPos.x.toPx(), state.selectorPos.y.toPx())

    val selectorRadius = ClockDialSelectorHandleContainerSize.toPx() / 2
    val selectorColor = colors.selectorColor

    // clear out the selector section
    drawCircle(
        radius = selectorRadius,
        center = selectorOffsetPx,
        color = Color.Black,
        blendMode = BlendMode.Clear,
    )

    // draw the text composables
    drawContent()

    // draw the selector and clear out the numbers overlapping
    drawCircle(
        radius = selectorRadius,
        center = selectorOffsetPx,
        color = selectorColor,
        blendMode = BlendMode.Xor
    )

    val strokeWidth = ClockDialSelectorTrackContainerWidth.toPx()
    val lineLength = selectorOffsetPx.minus(
        Offset(
            (selectorRadius * cos(state.currentAngle.value)),
            (selectorRadius * sin(state.currentAngle.value))
        )
    )

    // draw the selector line
    drawLine(
        start = size.center,
        strokeWidth = strokeWidth,
        end = lineLength,
        color = selectorColor,
        blendMode = BlendMode.SrcOver
    )

    // draw the selector small dot
    drawCircle(
        radius = ClockDialSelectorCenterContainerSize.toPx() / 2,
        center = size.center,
        color = selectorColor,
    )

    // draw the portion of the number that was overlapping
    drawCircle(
        radius = selectorRadius,
        center = selectorOffsetPx,
        color = colors.clockDialContentColor(selected = true),
        blendMode = BlendMode.DstOver
    )
}

@OptIn(ExperimentalMaterial3Api::class)
internal data class ClockDialModifier(
    private val state: TimePickerState,
    private val autoSwitchToMinute: Boolean,
) : ModifierNodeElement<ClockDialNode>() {

    override fun create(): ClockDialNode = ClockDialNode(
        state = state,
        autoSwitchToMinute = autoSwitchToMinute,
    )

    override fun update(node: ClockDialNode) {
        node.updateNode(
            state = state,
            autoSwitchToMinute = autoSwitchToMinute,
        )
    }

    override fun InspectorInfo.inspectableProperties() {
        // Show nothing in the inspector.
    }
}

@OptIn(ExperimentalMaterial3Api::class)
internal class ClockDialNode(
    private var state: TimePickerState,
    private var autoSwitchToMinute: Boolean
) : DelegatingNode(),
    PointerInputModifierNode,
    CompositionLocalConsumerModifierNode,
    LayoutAwareModifierNode {

    private var offsetX = 0f
    private var offsetY = 0f
    private val maxDist get() = with(requireDensity()) { MaxDistance.toPx() }

    private val pointerInputTapNode = delegate(SuspendingPointerInputModifierNode {
        detectTapGestures(
            onPress = {
                offsetX = it.x
                offsetY = it.y
            },
            onTap = {
                coroutineScope.launch { state.onTap(it.x, it.y, maxDist, autoSwitchToMinute) }
            },
        )
    })

    private val pointerInputDragNode = delegate(SuspendingPointerInputModifierNode {
        detectDragGestures(onDragEnd = {
            coroutineScope.launch {
                if (state.selection == Selection.Hour && autoSwitchToMinute) {
                    state.selection = Selection.Minute
                    state.animateToCurrent()
                } else if (state.selection == Selection.Minute) {
                    state.settle()
                }
            }
        }) { _, dragAmount ->
            coroutineScope.launch {
                offsetX += dragAmount.x
                offsetY += dragAmount.y
                state.update(atan(offsetY - state.center.y, offsetX - state.center.x))
            }
            state.moveSelector(offsetX, offsetY, maxDist)
        }
    })

    override fun onRemeasured(size: IntSize) {
        state.center = size.center
    }

    override fun onPointerEvent(
        pointerEvent: PointerEvent,
        pass: PointerEventPass,
        bounds: IntSize
    ) {
        pointerInputTapNode.onPointerEvent(pointerEvent, pass, bounds)
        pointerInputDragNode.onPointerEvent(pointerEvent, pass, bounds)
    }

    override fun onCancelPointerInput() {
        pointerInputTapNode.onCancelPointerInput()
        pointerInputDragNode.onCancelPointerInput()
    }

    fun updateNode(state: TimePickerState, autoSwitchToMinute: Boolean) {
        this.state = state
        this.autoSwitchToMinute = autoSwitchToMinute
    }
}

private enum class LayoutId {
    Selector, InnerCircle,
}

private val ClockDialContainerSize = 256.0.dp
val ClockDialSelectorCenterContainerSize = 8.0.dp
private val ClockDialSelectorHandleContainerSize = 48.0.dp
private val ClockDialSelectorTrackContainerWidth = 2.0.dp

private fun valuesForAnimation(current: Float, new: Float): Pair<Float, Float> {
    var start = current
    var end = new
    if (abs(start - end) <= PI) {
        return Pair(start, end)
    }

    if (start > PI && end < PI) {
        end += FullCircle
    } else if (start < PI && end > PI) {
        start += FullCircle
    }

    return Pair(start, end)
}

private fun dist(x1: Float, y1: Float, x2: Int, y2: Int): Float {
    val x = x2 - x1
    val y = y2 - y1
    return hypot(x.toDouble(), y.toDouble()).toFloat()
}

private fun atan(y: Float, x: Float): Float {
    val ret = atan2(y, x) - QuarterCircle.toFloat()
    return if (ret < 0) ret + FullCircle else ret
}


/**
 * Contains the default values used by [TimePicker]
 */
@ExperimentalMaterial3Api
@Stable
object TimePickerDefaults {

    /**
     * Default colors used by a [TimePicker] in different states
     */
    @Composable
    fun colors() = MaterialTheme.colorScheme.defaultTimePickerColors

    /**
     * Default colors used by a [TimePicker] in different states
     *
     * @param clockDialColor The color of the clock dial.
     * @param clockDialSelectedContentColor the color of the numbers of the clock dial when they
     * are selected or overlapping with the selector
     * @param clockDialUnselectedContentColor the color of the numbers of the clock dial when they
     * are unselected
     * @param selectorColor The color of the clock dial selector.
     * @param containerColor The container color of the time picker.
     * @param periodSelectorBorderColor the color used for the border of the AM/PM toggle.
     * @param periodSelectorSelectedContainerColor the color used for the selected container of
     * the AM/PM toggle
     * @param periodSelectorUnselectedContainerColor the color used for the unselected container
     * of the AM/PM toggle
     * @param periodSelectorSelectedContentColor color used for the selected content of
     * the AM/PM toggle
     * @param periodSelectorUnselectedContentColor color used for the unselected content
     * of the AM/PM toggle
     * @param timeSelectorSelectedContainerColor color used for the selected container of the
     * display buttons to switch between hour and minutes
     * @param timeSelectorUnselectedContainerColor color used for the unselected container of the
     * display buttons to switch between hour and minutes
     * @param timeSelectorSelectedContentColor color used for the selected content of the display
     * buttons to switch between hour and minutes
     * @param timeSelectorUnselectedContentColor color used for the unselected content of the
     * display buttons to switch between hour and minutes
     */
    @Composable
    fun colors(
        clockDialColor: Color = Color.Unspecified,
        clockDialSelectedContentColor: Color = Color.Unspecified,
        clockDialUnselectedContentColor: Color = Color.Unspecified,
        selectorColor: Color = Color.Unspecified,
        containerColor: Color = Color.Unspecified,
        periodSelectorBorderColor: Color = Color.Unspecified,
        periodSelectorSelectedContainerColor: Color = Color.Unspecified,
        periodSelectorUnselectedContainerColor: Color = Color.Unspecified,
        periodSelectorSelectedContentColor: Color = Color.Unspecified,
        periodSelectorUnselectedContentColor: Color = Color.Unspecified,
        timeSelectorSelectedContainerColor: Color = Color.Unspecified,
        timeSelectorUnselectedContainerColor: Color = Color.Unspecified,
        timeSelectorSelectedContentColor: Color = Color.Unspecified,
        timeSelectorUnselectedContentColor: Color = Color.Unspecified,
    ) = MaterialTheme.colorScheme.defaultTimePickerColors.copy(
        clockDialColor = clockDialColor,
        clockDialSelectedContentColor = clockDialSelectedContentColor,
        clockDialUnselectedContentColor = clockDialUnselectedContentColor,
        selectorColor = selectorColor,
        containerColor = containerColor,
        periodSelectorBorderColor = periodSelectorBorderColor,
        periodSelectorSelectedContainerColor = periodSelectorSelectedContainerColor,
        periodSelectorUnselectedContainerColor = periodSelectorUnselectedContainerColor,
        periodSelectorSelectedContentColor = periodSelectorSelectedContentColor,
        periodSelectorUnselectedContentColor = periodSelectorUnselectedContentColor,
        timeSelectorSelectedContainerColor = timeSelectorSelectedContainerColor,
        timeSelectorUnselectedContainerColor = timeSelectorUnselectedContainerColor,
        timeSelectorSelectedContentColor = timeSelectorSelectedContentColor,
        timeSelectorUnselectedContentColor = timeSelectorUnselectedContentColor
    )

    internal val ColorScheme.defaultTimePickerColors: TimePickerColors get() = TimePickerColors(
        clockDialColor = surfaceVariant,
        clockDialSelectedContentColor = onPrimary,
        clockDialUnselectedContentColor = onSurface,
        selectorColor = primary,
        containerColor = surface,
        periodSelectorBorderColor = outline,
        periodSelectorSelectedContainerColor = tertiaryContainer,
        periodSelectorUnselectedContainerColor = Color.Transparent,
        periodSelectorSelectedContentColor = onTertiaryContainer,
        periodSelectorUnselectedContentColor = onSurfaceVariant,
        timeSelectorSelectedContainerColor = primaryContainer,
        timeSelectorUnselectedContainerColor = surfaceVariant,
        timeSelectorSelectedContentColor = onPrimaryContainer,
        timeSelectorUnselectedContentColor = onSurface,
    )

    /** Default layout type, uses the screen dimensions to choose an appropriate layout. */
    @ReadOnlyComposable
    @Composable
    fun layoutType(): TimePickerLayoutType = with(LocalConfiguration.current) {
        if (screenHeightDp < screenWidthDp) {
            TimePickerLayoutType.Horizontal
        } else {
            TimePickerLayoutType.Vertical
        }
    }
}


/**
 * Represents the colors used by a [TimePicker] in different states
 *
 * @constructor create an instance with arbitrary colors.
 * See [TimePickerDefaults.colors] for the default implementation that follows Material
 * specifications.
 *
 * @param clockDialColor The color of the clock dial.
 * @param clockDialSelectedContentColor the color of the numbers of the clock dial when they
 * are selected or overlapping with the selector
 * @param clockDialUnselectedContentColor the color of the numbers of the clock dial when they
 * are unselected
 * @param selectorColor The color of the clock dial selector.
 * @param containerColor The container color of the time picker.
 * @param periodSelectorBorderColor the color used for the border of the AM/PM toggle.
 * @param periodSelectorSelectedContainerColor the color used for the selected container of
 * the AM/PM toggle
 * @param periodSelectorUnselectedContainerColor the color used for the unselected container
 * of the AM/PM toggle
 * @param periodSelectorSelectedContentColor color used for the selected content of
 * the AM/PM toggle
 * @param periodSelectorUnselectedContentColor color used for the unselected content
 * of the AM/PM toggle
 * @param timeSelectorSelectedContainerColor color used for the selected container of the
 * display buttons to switch between hour and minutes
 * @param timeSelectorUnselectedContainerColor color used for the unselected container of the
 * display buttons to switch between hour and minutes
 * @param timeSelectorSelectedContentColor color used for the selected content of the display
 * buttons to switch between hour and minutes
 * @param timeSelectorUnselectedContentColor color used for the unselected content of the
 * display buttons to switch between hour and minutes
 */
@Immutable
@ExperimentalMaterial3Api
class TimePickerColors constructor(
    val clockDialColor: Color,
    val selectorColor: Color,
    val containerColor: Color,
    val periodSelectorBorderColor: Color,
    val clockDialSelectedContentColor: Color,
    val clockDialUnselectedContentColor: Color,
    val periodSelectorSelectedContainerColor: Color,
    val periodSelectorUnselectedContainerColor: Color,
    val periodSelectorSelectedContentColor: Color,
    val periodSelectorUnselectedContentColor: Color,
    val timeSelectorSelectedContainerColor: Color,
    val timeSelectorUnselectedContainerColor: Color,
    val timeSelectorSelectedContentColor: Color,
    val timeSelectorUnselectedContentColor: Color,
) {
    /**
     * Returns a copy of this TimePickerColors, optionally overriding some of the values.
     * This uses the Color.Unspecified to mean “use the value from the source”
     */
    fun copy(
        clockDialColor: Color = this.containerColor,
        selectorColor: Color = this.selectorColor,
        containerColor: Color = this.containerColor,
        periodSelectorBorderColor: Color = this.periodSelectorBorderColor,
        clockDialSelectedContentColor: Color = this.clockDialSelectedContentColor,
        clockDialUnselectedContentColor: Color = this.clockDialUnselectedContentColor,
        periodSelectorSelectedContainerColor: Color = this.periodSelectorSelectedContainerColor,
        periodSelectorUnselectedContainerColor: Color = this.periodSelectorUnselectedContainerColor,
        periodSelectorSelectedContentColor: Color = this.periodSelectorSelectedContentColor,
        periodSelectorUnselectedContentColor: Color = this.periodSelectorUnselectedContentColor,
        timeSelectorSelectedContainerColor: Color = this.timeSelectorSelectedContainerColor,
        timeSelectorUnselectedContainerColor: Color = this.timeSelectorUnselectedContainerColor,
        timeSelectorSelectedContentColor: Color = this.timeSelectorSelectedContentColor,
        timeSelectorUnselectedContentColor: Color = this.timeSelectorUnselectedContentColor,
    ) = TimePickerColors(
        clockDialColor.takeOrElse { this.clockDialColor },
        selectorColor.takeOrElse { this.selectorColor },
        containerColor.takeOrElse { this.containerColor },
        periodSelectorBorderColor.takeOrElse { this.periodSelectorBorderColor },
        clockDialSelectedContentColor.takeOrElse { this.clockDialSelectedContentColor },
        clockDialUnselectedContentColor.takeOrElse { this.clockDialUnselectedContentColor },
        periodSelectorSelectedContainerColor
            .takeOrElse { this.periodSelectorSelectedContainerColor },
        periodSelectorUnselectedContainerColor
            .takeOrElse { this.periodSelectorUnselectedContainerColor },
        periodSelectorSelectedContentColor.takeOrElse { this.periodSelectorSelectedContentColor },
        periodSelectorUnselectedContentColor
            .takeOrElse { this.periodSelectorUnselectedContentColor },
        timeSelectorSelectedContainerColor.takeOrElse { this.timeSelectorSelectedContainerColor },
        timeSelectorUnselectedContainerColor
            .takeOrElse { this.timeSelectorUnselectedContainerColor },
        timeSelectorSelectedContentColor.takeOrElse { this.timeSelectorSelectedContentColor },
        timeSelectorUnselectedContentColor.takeOrElse { this.timeSelectorUnselectedContentColor },
    )

    @Stable
    internal fun periodSelectorContainerColor(selected: Boolean) =
        if (selected) {
            periodSelectorSelectedContainerColor
        } else {
            periodSelectorUnselectedContainerColor
        }

    @Stable
    internal fun periodSelectorContentColor(selected: Boolean) =
        if (selected) {
            periodSelectorSelectedContentColor
        } else {
            periodSelectorUnselectedContentColor
        }

    @Stable
    internal fun timeSelectorContainerColor(selected: Boolean) =
        if (selected) {
            timeSelectorSelectedContainerColor
        } else {
            timeSelectorUnselectedContainerColor
        }

    @Stable
    internal fun timeSelectorContentColor(selected: Boolean) =
        if (selected) {
            timeSelectorSelectedContentColor
        } else {
            timeSelectorUnselectedContentColor
        }

    @Stable
    internal fun clockDialContentColor(selected: Boolean) =
        if (selected) {
            clockDialSelectedContentColor
        } else {
            clockDialUnselectedContentColor
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other === null) return false
        if (this::class != other::class) return false

        other as TimePickerColors

        if (clockDialColor != other.clockDialColor) return false
        if (selectorColor != other.selectorColor) return false
        if (containerColor != other.containerColor) return false
        if (periodSelectorBorderColor != other.periodSelectorBorderColor) return false
        if (periodSelectorSelectedContainerColor != other.periodSelectorSelectedContainerColor)
            return false
        if (periodSelectorUnselectedContainerColor != other.periodSelectorUnselectedContainerColor)
            return false
        if (periodSelectorSelectedContentColor != other.periodSelectorSelectedContentColor)
            return false
        if (periodSelectorUnselectedContentColor != other.periodSelectorUnselectedContentColor)
            return false
        if (timeSelectorSelectedContainerColor != other.timeSelectorSelectedContainerColor)
            return false
        if (timeSelectorUnselectedContainerColor != other.timeSelectorUnselectedContainerColor)
            return false
        if (timeSelectorSelectedContentColor != other.timeSelectorSelectedContentColor)
            return false
        if (timeSelectorUnselectedContentColor != other.timeSelectorUnselectedContentColor)
            return false

        return true
    }

    override fun hashCode(): Int {
        var result = clockDialColor.hashCode()
        result = 31 * result + selectorColor.hashCode()
        result = 31 * result + containerColor.hashCode()
        result = 31 * result + periodSelectorBorderColor.hashCode()
        result = 31 * result + periodSelectorSelectedContainerColor.hashCode()
        result = 31 * result + periodSelectorUnselectedContainerColor.hashCode()
        result = 31 * result + periodSelectorSelectedContentColor.hashCode()
        result = 31 * result + periodSelectorUnselectedContentColor.hashCode()
        result = 31 * result + timeSelectorSelectedContainerColor.hashCode()
        result = 31 * result + timeSelectorUnselectedContainerColor.hashCode()
        result = 31 * result + timeSelectorSelectedContentColor.hashCode()
        result = 31 * result + timeSelectorUnselectedContentColor.hashCode()
        return result
    }
}


private const val FullCircle: Float = (PI * 2).toFloat()
private const val QuarterCircle = PI / 2
private const val RadiansPerMinute: Float = FullCircle / 60
private const val RadiansPerHour: Float = FullCircle / 12f

private val OuterCircleSizeRadius = 101.dp
private val InnerCircleRadius = 69.dp

private val MaxDistance = 74.dp
private val MinimumInteractiveSize = 48.dp
private val Minutes = listOf(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55)
private val Hours = listOf(12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
private val ExtraHours = Hours.fastMap { (it % 12 + 12) }

