package net.novagamestudios.kaffeekasse.ui.kaffeekasse

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import com.patrykandpatrick.vico.compose.axis.axisLabelComponent
import com.patrykandpatrick.vico.compose.axis.horizontal.rememberBottomAxis
import com.patrykandpatrick.vico.compose.axis.vertical.rememberEndAxis
import com.patrykandpatrick.vico.compose.chart.CartesianChartHost
import com.patrykandpatrick.vico.compose.chart.layer.lineSpec
import com.patrykandpatrick.vico.compose.chart.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.chart.layout.fullWidth
import com.patrykandpatrick.vico.compose.chart.rememberCartesianChart
import com.patrykandpatrick.vico.compose.chart.scroll.rememberChartScrollSpec
import com.patrykandpatrick.vico.compose.component.marker.rememberMarkerComponent
import com.patrykandpatrick.vico.compose.component.rememberLineComponent
import com.patrykandpatrick.vico.compose.component.rememberTextComponent
import com.patrykandpatrick.vico.compose.component.shape.shader.color
import com.patrykandpatrick.vico.compose.style.ChartStyle
import com.patrykandpatrick.vico.compose.style.LocalChartStyle
import com.patrykandpatrick.vico.compose.style.ProvideChartStyle
import com.patrykandpatrick.vico.core.axis.AxisItemPlacer
import com.patrykandpatrick.vico.core.axis.formatter.AxisValueFormatter
import com.patrykandpatrick.vico.core.chart.layout.HorizontalLayout
import com.patrykandpatrick.vico.core.chart.scale.AutoScaleUp
import com.patrykandpatrick.vico.core.chart.values.AxisValueOverrider
import com.patrykandpatrick.vico.core.component.marker.MarkerComponent
import com.patrykandpatrick.vico.core.component.shape.DashedShape
import com.patrykandpatrick.vico.core.component.shape.shader.DynamicShaders
import com.patrykandpatrick.vico.core.component.shape.shader.TopBottomShader
import com.patrykandpatrick.vico.core.marker.MarkerLabelFormatter
import com.patrykandpatrick.vico.core.model.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.model.LineCartesianLayerModel
import com.patrykandpatrick.vico.core.model.lineSeries
import com.patrykandpatrick.vico.core.scroll.InitialScroll
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import net.novagamestudios.common_utils.compose.logging.debug
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemCategory
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Transaction
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.TransactionsChartsState.ChartSettings.TimeFilter
import net.novagamestudios.kaffeekasse.util.charts.BackgroundShader
import net.novagamestudios.kaffeekasse.util.charts.IntegerAxisItemPlacer
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.roundToInt

class TransactionsChartsState(
    private val coroutineScope: CoroutineScope,
    private val transactionsFlow: Flow<List<Transaction>?>
) : Logger {

    var chartSettings by mutableStateOf(ChartSettings(
        timeFilter = TimeFilter.LastMonth,
        categoryFilter = { true }
    ))

    @OptIn(ExperimentalCoroutinesApi::class)
    private val balanceOverTimeData by lazy {
        transactionsFlow
            .mapLatest { transactions -> transactions?.let { calculateBalanceOverTimeData(it) } }
            .flowOn(Dispatchers.IO)
            .stateIn(coroutineScope, SharingStarted.Eagerly, null)
    }
    val overTimeChartModelProducer = CartesianChartModelProducer.build(Dispatchers.IO)

    init {
        coroutineScope.launch {
            snapshotFlow { chartSettings }.collectLatest { settings -> updateCharts(settings) }
        }
        coroutineScope.launch {
            balanceOverTimeData.collectLatest { data ->
                data?.let { updateBalanceOverTime(it, chartSettings) }
            }
        }
    }

    @Suppress("RedundantSuspendModifier")
    private suspend fun calculateBalanceOverTimeData(transactions: List<Transaction>): List<Pair<Long, Double>> {
        val overTime = transactions
            .asReversed()
            .groupBy { it.date.epochDays }
            .mapValues { (_, value) -> value.last().newBalance }
            .map { (date, total) -> date to total }

        val filled = mutableListOf<Pair<Long, Double>>()
        var lastDay: Long? = null
        var lastValue = 0.0
        // Fill in missing days
        for ((day, value) in overTime) {
            if (lastDay != null) {
                for (d in (lastDay + 1..<day)) {
                    filled += d to lastValue
                }
            }
            filled += day to value
            lastDay = day
            lastValue = value
        }
        return filled
    }
    private suspend fun updateBalanceOverTime(data: List<Pair<Long, Double>>, settings: ChartSettings) {
        overTimeChartModelProducer.runTransaction {
            val filledX = mutableListOf<Long>()
            val filledY = mutableListOf<Double>()

            var lastDay: Long? = settings.timeFilter.start?.epochDays
            var lastValue = 0.0
            // Fill in missing days
            for ((day, value) in data.filter { it.first.epochDays in settings.timeFilter }) {
                if (lastDay != null) {
                    for (d in (lastDay + 1..<day)) {
                        filledX.add(d)
                        filledY.add(lastValue)
                    }
                }
                filledX.add(day)
                filledY.add(value)
                lastDay = day
                lastValue = value
            }
            if (lastDay != null) {
                for (d in (lastDay + 1..settings.timeFilter.end.epochDays)) {
                    filledX.add(d)
                    filledY.add(lastValue)
                }
            }
//            debug { "x $filledX" }
//            debug { "y $filledY" }
            lineSeries { series(x = filledX, y = filledY) }
//                val v = (1..50).toList()
//                lineSeries { series(v, v.map { Random.nextFloat() * 16f - 6 }) }
        }.await()
    }

    private suspend fun updateCharts(settings: ChartSettings) = supervisorScope {
        debug { "updateCharts $settings" }
        coroutineScope.launch(Dispatchers.IO) {
            balanceOverTimeData.value?.let { data -> updateBalanceOverTime(data, settings) }
        }
    }

    data class ChartSettings(
        val timeFilter: TimeFilter,
        val categoryFilter: (ItemCategory) -> Boolean
    ) {
        sealed interface TimeFilter {
            val start: LocalDateTime?
            val end: LocalDateTime
            operator fun contains(date: LocalDateTime): Boolean = start
                ?.let { date in it..end }
                ?: (date <= end)

            data object AllTime : TimeFilter {
                override val start = null
                override val end get() = now
            }
            data object LastWeek : TimeFilter {
                override val start get() = now.minusWeeks(1)!!
                override val end get() = now
            }
            data object LastMonth : TimeFilter {
                override val start get() = now.minusMonths(1)!!
                override val end get() = now
            }
            data object Last3Months : TimeFilter {
                override val start get() = now.minusMonths(3)!!
                override val end get() = now
            }
            data object Last6Months : TimeFilter {
                override val start get() = now.minusMonths(6)!!
                override val end get() = now
            }
            data object LastYear : TimeFilter {
                override val start get() = now.minusYears(1)!!
                override val end get() = now
            }

            companion object {
                private val now: LocalDateTime get() = LocalDateTime.now()
            }
        }
    }
    companion object {
        private val LocalDateTime.epochDays get() = toLocalDate().toEpochDay()
        private val Long.epochDays get() = LocalDate.ofEpochDay(this).atStartOfDay()
    }
}

@Composable
fun TransactionsCharts(
    state: TransactionsChartsState,
    modifier: Modifier = Modifier,
    lazyListState: LazyListState = rememberLazyListState()
) = ProvideChartStyle(
    LocalChartStyle.current.copy(
        lineLayer = remember { ChartStyle.LineLayer(
            lines = listOf(
                lineSpec(
                    shader = TopBottomShader(
                        DynamicShaders.color(Color.Green),
                        DynamicShaders.color(Color.Red)
                    ),
                    // crashes if left default
                    backgroundShader = BackgroundShader.gradient(
                        Color.Green,
                        Color.Red
                    )
                )
            )
        ) }
    )
) {
    LazyColumn(
        modifier.fillMaxSize(),
        state = lazyListState,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        item {
            BalanceOverTimeChart(state, Modifier.fillMaxWidth())
        }
    }
}

@Composable
fun TransactionsChartsFilter(
    state: TransactionsChartsState,
    modifier: Modifier = Modifier
) = Box(modifier) {
    var showDropdown by remember { mutableStateOf(false) }
    TextButton(onClick = { showDropdown = !showDropdown }) {
        ChartTimeFilterLabel(state.chartSettings.also { debug { "settings $it" } }.timeFilter)
        val animatedRotation by animateFloatAsState(
            if (showDropdown) 180f else 0f,
            label = "Rotation"
        )
        Icon(
            Icons.Default.ArrowDropDown,
            null,
            Modifier.graphicsLayer { rotationZ = animatedRotation })
    }
    DropdownMenu(expanded = showDropdown, onDismissRequest = { showDropdown = false }) {
        listOf(
            TimeFilter.LastWeek,
            TimeFilter.LastMonth,
            TimeFilter.Last3Months,
            TimeFilter.Last6Months,
            TimeFilter.LastYear,
            TimeFilter.AllTime
        ).forEach { filter ->
            DropdownMenuItem(
                text = { ChartTimeFilterLabel(filter) },
                onClick = {
                    Logger("").debug { "filter $filter" }
                    state.chartSettings = state.chartSettings.copy(timeFilter = filter)
                    showDropdown = false
                }
            )
        }
    }
}

@Composable
private fun BalanceOverTimeChart(
    state: TransactionsChartsState,
    modifier: Modifier = Modifier
) = Column(modifier) {
    Text(
        "Kontostand",
        Modifier
            .padding(horizontal = 16.dp)
            .padding(top = 8.dp),
        style = MaterialTheme.typography.titleMedium
    )
    CartesianChartHost(
        chart = rememberCartesianChart(
            rememberLineCartesianLayer(
                axisValueOverrider = remember { VerticalAxisPadding() }
            ),
            endAxis = rememberEndAxis(
                valueFormatter = remember {
                    AxisValueFormatter { value, _, _ ->
                        String.format("%.0f€", value)
                    }
                },
                itemPlacer = IntegerAxisItemPlacer()
            ),
            bottomAxis = rememberBottomAxis(
                label = axisLabelComponent(typeface = Typeface.DEFAULT),
                valueFormatter = remember {
                    val dateTimeFormatter = DateTimeFormatter.ofPattern("d. MMM")
                    AxisValueFormatter { day, _, _ ->
                        (LocalDate.ofEpochDay(day.toLong())).format(dateTimeFormatter)
                    }
                },
                labelRotationDegrees = 60f,
                itemPlacer = AxisItemPlacer.Horizontal.default(
                    addExtremeLabelPadding = true
                )
            )
        ),
        modelProducer = state.overTimeChartModelProducer,
        Modifier.height(300.dp),
        marker = rememberMarkerComponent(
            label = rememberTextComponent(
                typeface = Typeface.MONOSPACE
            ),
            labelPosition = MarkerComponent.LabelPosition.Top,
            guideline = rememberLineComponent(
                color = MaterialTheme.colorScheme.outline,
                thickness = 1.dp,
                shape = DashedShape()
            )
        ).apply {
            labelFormatter = remember { MarkerLabelFormatter { markedEntries, _ ->
                val entryModel = markedEntries[0]
                val modelEntry = entryModel.entry as LineCartesianLayerModel.Entry
                SpannableStringBuilder().append(
                    String.format("%.2f€", modelEntry.y),
                    ForegroundColorSpan(entryModel.color),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            } }
        },
        chartScrollSpec = rememberChartScrollSpec(
            isScrollEnabled = false,
            initialScroll = InitialScroll.End
        ),
        autoScaleUp = AutoScaleUp.Full,
        horizontalLayout = HorizontalLayout.fullWidth(),
        placeholder = {
            CircularProgressIndicator(Modifier.align(Alignment.Center))
        }
    )
}

@Composable
private fun ChartTimeFilterLabel(
    filter: TimeFilter
) = when (filter) {
    TimeFilter.AllTime -> Text("All time")
    TimeFilter.LastWeek -> Text("Last week")
    TimeFilter.LastMonth -> Text("Last month")
    TimeFilter.Last3Months -> Text("Last 3 months")
    TimeFilter.Last6Months -> Text("Last 6 months")
    TimeFilter.LastYear -> Text("Last year")
}

class VerticalAxisPadding(
    private val diffFactor: Float = 0.1f
) : AxisValueOverrider<LineCartesianLayerModel> {
    override fun getMinY(model: LineCartesianLayerModel): Float {
        val diff = (model.maxY - model.minY) * diffFactor
        return (model.minY - diff).roundToInt().toFloat()
    }
    override fun getMaxY(model: LineCartesianLayerModel): Float {
        val diff = (model.maxY - model.minY) * diffFactor
        return (model.maxY + diff).roundToInt().toFloat()
    }
}


/*

NOTES:

Am meisten
über zeit (zeit filter, all time, heute, gestern, letzte woche, letzter monat, letztes jahr)
über kategorie (kategorie filter, alle kategorien, kategorie1, kategorie2, kategorie3)
 - pie chart
pro wochentag
 - bar chart



 */

