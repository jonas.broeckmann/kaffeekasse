package net.novagamestudios.kaffeekasse.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ProvidableCompositionLocal
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat
import net.novagamestudios.kaffeekasse.App.Companion.settings
import net.novagamestudios.kaffeekasse.repositories.Settings
import net.novagamestudios.kaffeekasse.repositories.Settings.Companion.isDarkMode
import net.novagamestudios.kaffeekasse.ui.util.derived

private val DarkColorScheme = darkColorScheme(
    primary = AppYellow,
    secondary = AppBlue,
    tertiary = AppDarkBlue
)

private val LightColorScheme = lightColorScheme(
    primary = AppYellow,
    secondary = AppBlue,
    tertiary = AppDarkBlue
)

@Composable
fun KaffeekasseTheme(
    settings: State<Settings> = settings(),
    darkTheme: Boolean = settings.isDarkMode,
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }
        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }

    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.background.toArgb()
            window.navigationBarColor = colorScheme.background.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = !darkTheme
            WindowCompat.getInsetsController(window, view).isAppearanceLightNavigationBars = !darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography
    ) {
        CompositionLocalProvider(
            LocalAnimationSwitch provides settings.derived { animationsEnabled }.value
        ) {
            content()
        }
    }
}


fun Color.enabled(enabled: Boolean = true) = disabled(!enabled)
fun Color.disabled(disabled: Boolean = true) = if (disabled) copy(alpha = 0.38f) else this


val LocalAnimationSwitch: ProvidableCompositionLocal<Boolean> = compositionLocalOf { true }

typealias TransitionSpec<T> = AnimatedContentTransitionScope<T>.() -> ContentTransform
@Composable
fun <T> ifAnimationsEnabled(transitionSpec: TransitionSpec<T>): TransitionSpec<T> = if (LocalAnimationSwitch.current) transitionSpec else {
    { ContentTransform(EnterTransition.None, ExitTransition.None) }
}

@Composable
fun EnterTransition.ifAnimationsEnabled(): EnterTransition = if (LocalAnimationSwitch.current) this else EnterTransition.None
@Composable
fun ExitTransition.ifAnimationsEnabled(): ExitTransition = if (LocalAnimationSwitch.current) this else ExitTransition.None
