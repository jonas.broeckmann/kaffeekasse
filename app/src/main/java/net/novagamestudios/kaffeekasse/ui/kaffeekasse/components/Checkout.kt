package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBars
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Send
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Remove
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SheetState
import androidx.compose.material3.Text
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.Toasts
import net.novagamestudios.common_utils.compose.ToastsState
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.compose.components.CircularLoadingBox
import net.novagamestudios.common_utils.compose.components.TransparentListItem
import net.novagamestudios.common_utils.compose.maskedCircleIcon
import net.novagamestudios.common_utils.compose.state.ReentrantActionState
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.kaffeekasse.KaffeekasseModule.Companion.kaffeekasseCartProvider
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.APIPurchaseAccount
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Cart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.MutableCart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.PurchaseAccount
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ScraperPurchaseAccount
import net.novagamestudios.kaffeekasse.model.kaffeekasse.isEmpty
import net.novagamestudios.kaffeekasse.model.kaffeekasse.isNotEmpty
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.model.session.User
import net.novagamestudios.kaffeekasse.repositories.LoginRepository
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.PurchaseController.WrappedAccount
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards.CategoryIcon
import net.novagamestudios.kaffeekasse.util.richdata.RichDataFlow
import net.novagamestudios.kaffeekasse.util.richdata.RichDataState
import net.novagamestudios.kaffeekasse.util.richdata.awaitDataOrNull
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichStateHere
import net.novagamestudios.kaffeekasse.util.richdata.combineRich
import net.novagamestudios.kaffeekasse.util.richdata.dataOrNull
import net.novagamestudios.kaffeekasse.util.richdata.mapRich
import net.novagamestudios.kaffeekasse.util.richdata.stateIn
import kotlin.time.Duration.Companion.seconds


class CheckoutState<PA : PurchaseAccount> private constructor(
    coroutineScope: CoroutineScope,
    private val session: Session.WithRealUser,
    val cart: MutableCart,
    private val purchaseController: PurchaseController<PA>,
    private val loginRepository: LoginRepository,
    private val onSubmitted: suspend () -> Unit
) : Logger, CoroutineScope by coroutineScope {

    var showModal by mutableStateOf(false)

    val hasInstantCheckout: Boolean = session is Session.WithDevice

    private val loadingMutex = ReentrantActionState()
    val isCheckoutLoading by loadingMutex

    val toasts = ToastsState()

    var indicateSuccess by mutableStateOf(false)

    private val wrappedAccountSelectionFlow: RichDataFlow<AccountSelectionState<WrappedAccount<PA>>> by lazy {
        purchaseController
            .purchaseAccounts
            .mapRich { accounts ->
                val sorted = accounts.sortedBy { it.account.name }
                val selected = sorted.withIndex().firstOrNull { it.value.isDefault }?.index
                    ?: sorted.withIndex().firstOrNull { it.value.account.name.firstLast == session.realUser.displayName }?.index
                    ?: -1
                AccountSelectionState(
                    accounts = sorted,
                    initialIndex = selected
                )
            }
    }

    val accountSelectionState: RichDataState<AccountSelectionState<PurchaseAccount>> get() = wrappedAccountSelectionFlow
        .collectAsRichStateHere()

    fun refreshPurchaseAccounts() {
        launch {
            loadingMutex.trueWhile {
                purchaseController.refreshPurchaseAccountsIfNeeded()
            }
        }
    }

    fun submitCart(logoutAfter: Boolean = false) {
        if (isCheckoutLoading) {
            warn { "Checkout already in progress, not submitting" }
            return
        }
        if (cart.isEmpty()) {
            warn { "Cart is empty, not submitting" }
            return
        }
        launch {
            val success = loadingMutex.trueWhile {
                val purchaseAccount = wrappedAccountSelectionFlow.awaitDataOrNull()?.selectedAccount ?: run {
                    warn { "No purchase account loaded or selected, not aborting" }
                    toasts.short("Bitte Konto wählen")
                    return@trueWhile false
                }
                try {
                    purchaseController.performPurchase(purchaseAccount.account, cart)
                    true
                } catch (e: Exception) {
                    warn(e) { "Failed to submit cart" }
                    toasts.long("Fehler: ${e.message ?: e::class.simpleName ?: "Unbekannter Fehler"}")
                    false
                }
            }
            if (success) {
                indicateSuccess = true
                delay(0.5.seconds)
                cart.clear()
                onSubmitted()
                indicateSuccess = false
                if (logoutAfter) {
                    loginRepository.logoutUser()
                }
            }
        }
    }

    companion object {
        context (RepositoryProvider)
        fun create(
            session: Session.WithRealUser,
            coroutineScope: CoroutineScope,
            onSubmitted: suspend () -> Unit
        ): CheckoutState<*> = CheckoutState(
            coroutineScope = coroutineScope,
            session = session,
            cart = kaffeekasseCartProvider[session.realUser],
            purchaseController = when (session) {
                is Session.WithDevice -> APIPurchaseController(
                    user = session.realUser,
                    coroutineScope = coroutineScope,
                    kaffeekasse = kaffeekasseRepository
                )
                else -> ScraperPurchaseController(
                    user = session.realUser,
                    kaffeekasse = kaffeekasseRepository
                )
            },
            loginRepository = loginRepository,
            onSubmitted = onSubmitted
        )
    }
}

private interface PurchaseController<PA : PurchaseAccount> {
    val purchaseAccounts: RichDataFlow<List<WrappedAccount<PA>>>
    suspend fun refreshPurchaseAccountsIfNeeded()
    suspend fun performPurchase(purchaseAccount: PA, cart: Cart)

    data class WrappedAccount<out PA : PurchaseAccount>(
        val account: PA,
        override val isDefault: Boolean
    ) : PurchaseAccount by account, PurchaseAccount.WithDefault
}

private class ScraperPurchaseController(
    private val user: User,
    private val kaffeekasse: KaffeekasseRepository
) : PurchaseController<ScraperPurchaseAccount> {
    private val manualBillAccounts = kaffeekasse.manualBillAccounts[user]
    override val purchaseAccounts: RichDataFlow<List<WrappedAccount<ScraperPurchaseAccount>>> = manualBillAccounts.mapRich { accounts ->
        accounts.map { account ->
            WrappedAccount(
                account = account,
                isDefault = account.isDefault
            )
        }
    }

    override suspend fun refreshPurchaseAccountsIfNeeded() {
        manualBillAccounts.ensureCleanData()
    }

    override suspend fun performPurchase(purchaseAccount: ScraperPurchaseAccount, cart: Cart) {
        kaffeekasse.purchase(
            asUser = user,
            cart = cart,
            account = purchaseAccount
        )
    }
}

private class APIPurchaseController(
    private val user: User,
    coroutineScope: CoroutineScope,
    private val kaffeekasse: KaffeekasseRepository
) : PurchaseController<APIPurchaseAccount> {

    private val selfUserBasic = kaffeekasse.basicUserInfoList.mapRich { userList ->
        val selfDisplayName = user.displayName ?: return@mapRich null
        userList.firstOrNull { it.name.firstLast == selfDisplayName }
    }.stateIn(coroutineScope, SharingStarted.Eagerly)
    private val selfId get() = selfUserBasic.value.dataOrNull?.id

//    private val selfUserExtended = selfUserBasic.flatMapLatestRich {
//        kaffeekasse.getExtendedUserInfo(it.id)
//    }

//    override val purchaseAccounts: RichDataFlow<List<PurchaseAccount>> = combineRich(
//        selfUserExtended,
//        kaffeekasse.basicUserInfoList
//    ) { self, userList ->
//        // FIXME wrong way around
//
//        val fromWhitelist = self.whitelist.orEmpty()
//        val fromBlacklist = self.blacklist?.let { blacklist ->
//            val blacklistIds = blacklist.mapTo(mutableSetOf()) { it.id }
//            userList.filter { it.id !in blacklistIds }
//        }.orEmpty()
//
//        listOf(self) + fromWhitelist + fromBlacklist
//    }

    override val purchaseAccounts = combineRich(
        kaffeekasse.manualBillAccounts[user],
        kaffeekasse.basicUserInfoList
    ) { scraperAccounts, apiAccounts ->
        scraperAccounts.mapNotNull { scraperAccount ->
            apiAccounts.firstOrNull { it.name == scraperAccount.name }?.let { apiAccount ->
                WrappedAccount(
                    account = apiAccount,
                    isDefault = scraperAccount.isDefault
                )
            }
        }
    }

    override suspend fun refreshPurchaseAccountsIfNeeded() {
        kaffeekasse.basicUserInfoList.ensureCleanData()
        selfId?.let {
            kaffeekasse.getExtendedUserInfo(it).ensureCleanData()
        }
    }
    override suspend fun performPurchase(purchaseAccount: APIPurchaseAccount, cart: Cart) {
        kaffeekasse.purchase(
            asUser = user,
            cart = cart,
            targetAccount = purchaseAccount.takeUnless { it.id == selfId }
        )
    }
}


@Composable
fun Checkout(
    state: CheckoutState<*>,
    modifier: Modifier = Modifier
) {
    if (state.cart.isNotEmpty() && !state.showModal) Box(modifier.padding(16.dp)) {
        if (state.indicateSuccess) {
            BoxCenter(Modifier.size(56.dp)) {
                Icon(Icons.Default.Check, "Success", tint = Color.Green)
            }
        } else if (state.isCheckoutLoading) {
            CircularProgressIndicator(Modifier)
        } else {
            CheckoutFABs(
                itemCount = state.cart.itemCount,
                onCheckout = { state.showModal = true },
                onInstantCheckout = if (state.hasInstantCheckout) {
                    {
                        state.info { "User clicked instant checkout" }
                        state.submitCart(logoutAfter = true)
                    }
                } else null
            )
        }
    }
    if (state.showModal) {
        LaunchedEffect(Unit) {
            state.refreshPurchaseAccounts()
        }
        CheckoutModal(
            accountSelectionState = state.accountSelectionState.dataOrNull ?: AccountSelectionState(emptyList(), -1),
            cart = state.cart,
            onSubmit = { state.submitCart() },
            onDismissRequest = { state.showModal = false },
            isLoading = state.isCheckoutLoading,
            indicateSuccess = state.indicateSuccess
        )
    }
    Toasts(state.toasts)
}

@Composable
private fun CheckoutFABs(
    itemCount: Int,
    onCheckout: () -> Unit,
    onInstantCheckout: (() -> Unit)?,
    modifier: Modifier = Modifier
) = Column(
    modifier,
    verticalArrangement = Arrangement.spacedBy(16.dp),
    horizontalAlignment = Alignment.End
) {
    FloatingActionButton(
        onClick = onCheckout
    ) {
        Icon(Icons.Default.KeyboardArrowUp, null)
    }
    if (onInstantCheckout != null) ExtendedFloatingActionButton(
        text = { Text("Schnell buchen") },
        icon = { CountIcon(itemCount) },
        onClick = onInstantCheckout
    )
}

@Composable
fun CountIcon(
    itemCount: Int,
    modifier: Modifier = Modifier
) = BoxCenter(modifier.maskedCircleIcon(LocalContentColor.current)) {
    Text("$itemCount", fontWeight = FontWeight.Bold)
}

@Composable
private fun CheckoutModal(
    accountSelectionState: AccountSelectionState<*>,
    cart: MutableCart,
    onSubmit: () -> Unit,
    onDismissRequest: () -> Unit,
    modifier: Modifier = Modifier,
    sheetState: SheetState = rememberModalBottomSheetState(),
    isLoading: Boolean = false,
    indicateSuccess: Boolean = false
) {
    ModalBottomSheet(
        onDismissRequest = onDismissRequest,
        modifier,
        sheetState = sheetState
    ) {
        CheckoutDetails(
            accountSelectionState = accountSelectionState,
            cart = cart,
            onSubmit = onSubmit,
            Modifier
                .windowInsetsPadding(WindowInsets.navigationBars)
                .verticalScroll(rememberScrollState())
                .padding(horizontal = 16.dp),
            isLoading = isLoading,
            indicateSuccess = indicateSuccess
        )
    }
    // Hide checkout modal when cart is empty
    if (cart.isEmpty()) LaunchedEffect(Unit) {
        launch { sheetState.hide() }.invokeOnCompletion { onDismissRequest() }
    }
}

@Composable
private fun CheckoutDetails(
    accountSelectionState: AccountSelectionState<*>,
    cart: MutableCart,
    onSubmit: () -> Unit,
    modifier: Modifier = Modifier,
    isLoading: Boolean = false,
    indicateSuccess: Boolean = false
) = Column(modifier) {
    Column(verticalArrangement = Arrangement.spacedBy(8.dp)) {
        cart.forEach { entry ->
            CheckoutCartEntry(
                entry = entry,
                isLoading = { isLoading },
                onRemove = { cart -= entry.item }
            )
        }
    }
    HorizontalDivider(Modifier.padding(vertical = 16.dp), color = MaterialTheme.colorScheme.outline)
    Row(
        verticalAlignment = Alignment.Bottom
    ) {
        // Account
        AccountSelection(accountSelectionState)

        Spacer(
            Modifier
                .weight(1f)
                .width(16.dp))

        // Submit
        if (indicateSuccess) BoxCenter(Modifier.size(56.dp)) {
            Icon(Icons.Default.Check, "Success", tint = Color.Green)
        } else CircularLoadingBox(isLoading) {
            FloatingActionButton(
                onClick = onSubmit,
                elevation = FloatingActionButtonDefaults.bottomAppBarFabElevation(),
            ) {
                Icon(Icons.AutoMirrored.Filled.Send, "Submit")
            }
        }
    }
    Spacer(Modifier.height(32.dp))
}

@Composable
private fun CheckoutCartEntry(
    entry: Cart.Entry,
    isLoading: () -> Boolean,
    onRemove: () -> Unit,
    modifier: Modifier = Modifier
) = TransparentListItem(
    headlineContent = {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text("${entry.count}x", Modifier.padding(end = 8.dp))
            CategoryIcon(entry.item.category,
                Modifier
                    .padding(end = 4.dp)
                    .scale(0.8f))
            Text(entry.item.cleanFullName)
        }
    },
    modifier,
    trailingContent = {
        IconButton(onClick = onRemove, enabled = (!isLoading() && entry.count > 0)) {
            if (entry.count > 1) {
                Icon(Icons.Default.Remove, "Remove")
            } else {
                Icon(Icons.Default.Delete, "Remove")
            }
        }
    }
)

