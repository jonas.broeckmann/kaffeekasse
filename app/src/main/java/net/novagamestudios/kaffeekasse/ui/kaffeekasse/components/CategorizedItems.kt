package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components

import android.content.Context
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.state.MutableDataStoreState
import net.novagamestudios.common_utils.core.toastShort
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.kaffeekasse.App.Companion.settings
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Cart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Item
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemCategory
import net.novagamestudios.kaffeekasse.model.kaffeekasse.MutableCart
import net.novagamestudios.kaffeekasse.repositories.UserSettings
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards.BasicCardGrid
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards.CategoryCard
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards.LazyBasicCardGrid
import net.novagamestudios.kaffeekasse.ui.theme.ifAnimationsEnabled

class CategorizedItemsState(
    items: Iterable<Item>
) {
    val itemsByCategory = items
        .groupBy { it.category }
        .mapValues { (_, items) -> items.sortedBy { it.cleanFullName } }

    val categoriesWithItems by derivedStateOf {
        itemsByCategory.entries.sortedBy { it.key }.map { it.toPair() }
    }

    // if there is only one category, select it by default
    private val homeCategory get() = if (itemsByCategory.size == 1) itemsByCategory.keys.first() else null

    var selectedCategory by mutableStateOf(homeCategory)
        private set

    fun select(category: ItemCategory) {
        selectedCategory = category
    }

    val canNavigateBack by derivedStateOf { selectedCategory != homeCategory }
    fun navigateBack() {
        selectedCategory = homeCategory
    }
    fun reset() {
        selectedCategory = homeCategory
    }

    val backNavigation = BackNavigationHandler.derivedOf(
        canNavigateBack = { selectedCategory != homeCategory },
        onNavigateBack = {
            if (selectedCategory == homeCategory) return@derivedOf false
            selectedCategory = homeCategory
            true
        }
    )
}

@Composable
fun CategorizedItems(
    state: CategorizedItemsState,
    cart: MutableCart,
    modifier: Modifier = Modifier
) = Box(modifier) {
    AnimatedContent(
        targetState = state.selectedCategory,
        Modifier.fillMaxHeight(),
        transitionSpec = ifAnimationsEnabled {
            val categoryGridScale = 1.6f
            val itemGridScale = 0.5f
            if (targetState == null) ContentTransform(
                targetContentEnter = scaleIn(initialScale = categoryGridScale) + fadeIn(),
                initialContentExit = scaleOut(targetScale = itemGridScale) + fadeOut()
            ) else ContentTransform(
                targetContentEnter = scaleIn(initialScale = itemGridScale) + fadeIn(),
                initialContentExit = scaleOut(targetScale = categoryGridScale) + fadeOut()
            )
        },
        label = "ItemGroupSelection"
    ) { category ->
        if (category == null) LazyCategoryCardGrid(
            state = remember(state, cart) {
                CategoryCardGridState(
                    itemsByCategory = state.categoriesWithItems,
                    cart = cart,
                    onClick = { state.select(it) },
                )
            },
            spacerForFAB = true
        ) else LazyItemCardGrid(
            items = state.itemsByCategory[category] ?: emptyList(),
            cart = cart,
            spacerForFAB = true
        )
    }
}

private data class CategoryCardGridState(
    val itemsByCategory: List<Pair<ItemCategory, List<Item>>>,
    val cart: Cart,
    private val onClick: (ItemCategory) -> Unit,
) {
    fun click(category: ItemCategory) {
        onClick(category)
    }
}

@Composable
private fun LazyCategoryCardGrid(
    state: CategoryCardGridState,
    modifier: Modifier = Modifier,
    scrollable: Boolean = true,
    spacerForFAB: Boolean = false
) = LazyBasicCardGrid(
    items = state.itemsByCategory,
    modifier,
    scrollable = scrollable,
    spacerForFAB = spacerForFAB
) { (category, items) ->
    CategoryCard(
        category = category,
        highlighted = items.any { it in state.cart },
        onClick = { state.click(category) }
    )
}

@Composable
fun LazyItemCardGrid(
    items: List<Item>,
    cart: MutableCart,
    modifier: Modifier = Modifier,
    scrollable: Boolean = true,
    spacerForFAB: Boolean = false
) = LazyBasicCardGrid(
    items = items,
    modifier,
    scrollable = scrollable,
    spacerForFAB = spacerForFAB
) { item ->
    ItemCard(item = item, cart = cart)
}


@Composable
fun ItemCardGrid(
    items: List<Item>,
    cart: MutableCart,
    modifier: Modifier = Modifier,
    spacerForFAB: Boolean = false
) = BasicCardGrid(
    items = items,
    modifier,
    spacerForFAB = spacerForFAB
) { item ->
    ItemCard(item = item, cart = cart)
}


@Composable
private fun ItemCard(
    item: Item,
    cart: MutableCart
) {
    val coroutineScope = rememberCoroutineScope()
    val userSettings by settings().userSettings
    val context = LocalContext.current
    net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.cards.ItemCard(
        item = item,
        highlighted = item in cart,
        count = cart[item],
        onClick = {
            if (item in cart) cart.removeAll(item)
            else cart += item
        },
        onAdd = { cart += item },
        onRemove = { cart -= item },
        onLongClick = {
            coroutineScope.launch {
                userSettings?.addToFavourites(item, context)
            }
        }
    )
}

private suspend fun MutableDataStoreState<UserSettings>.addToFavourites(
    item: Item,
    context: Context
) = update { old ->
    val list = old.favoriteItemIds.toMutableList()
    val wasFavorite = item.id in list
    if (wasFavorite) list -= item.id
    else list += item.id
    old.copy(favoriteItemIds = list).also {
        context.toastShort(
            if (wasFavorite) "Aus Favoriten entfernt"
            else "Zu Favoriten hinzugefügt"
        )
    }
}

