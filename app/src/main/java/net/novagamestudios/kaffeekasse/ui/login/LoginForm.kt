package net.novagamestudios.kaffeekasse.ui.login

import android.content.Context
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Password
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material.icons.rounded.Coffee
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.autofill.AutofillType
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import cafe.adriel.voyager.navigator.Navigator
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.common_utils.voyager.model.collectAsStateHere
import net.novagamestudios.kaffeekasse.model.credentials.Login
import net.novagamestudios.kaffeekasse.model.credentials.isValid
import net.novagamestudios.kaffeekasse.repositories.LoginRepository
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.SettingsRepository
import net.novagamestudios.kaffeekasse.repositories.i11.PortalRepository
import net.novagamestudios.kaffeekasse.ui.AppInfoTopBarAction
import net.novagamestudios.kaffeekasse.ui.FullscreenIconButton
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.components.FailureRetryScreen
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.LoginNavigation
import net.novagamestudios.kaffeekasse.ui.util.autofill
import net.novagamestudios.kaffeekasse.ui.util.rememberSerializableState


class LoginFormScreenModel private constructor(
    private val loginRepository: LoginRepository,
    private val settings: SettingsRepository,
    private val portal: PortalRepository
) : ScreenModel, Logger {

    val session by portal.session

    val autoLogin by derivedStateOf { settings.value.autoLogin }

    var initialFormInput by mutableStateOf(Login.Empty)

    val loginErrors by loginRepository.errors.collectAsStateHere()


    fun login(login: Login, autoLogin: Boolean, activityContext: Context) {
        screenModelScope.launch {
            loginRepository.login(login, autoLogin, activityContext)
        }
    }

    fun tryAutoLogin(activityContext: Context) {
        screenModelScope.launch {
            loginRepository.tryAutoLogin(activityContext)
        }
    }

    companion object : ScreenModelFactory<RepositoryProvider, LoginNavigation.FormScreen, LoginFormScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: LoginNavigation.FormScreen) = LoginFormScreenModel(
            loginRepository = loginRepository,
            settings = settingsRepository,
            portal = portalRepository
        )
    }
}

class LoginFormContent(provider: ScreenModelProvider<LoginFormScreenModel>) : AppScaffoldContentWithModel<LoginFormScreenModel>(provider) {
    @Composable
    override fun TopAppBarActions(navigator: Navigator) {
        AppInfoTopBarAction()
        LoginDeviceButton()
        FullscreenIconButton()
    }
}

@Composable
fun LoginForm(
    model: LoginFormScreenModel,
    modifier: Modifier = Modifier
) = BoxCenter(modifier) {
    val activityContext = LocalContext.current
    LaunchedEffect(Unit) {
        model.initialFormInput = Login.Empty
        model.tryAutoLogin(activityContext)
    }
    Column(
        Modifier.width(IntrinsicSize.Min),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Icon(
            Icons.Rounded.Coffee,
            "Kaffeekasse",
            Modifier
                .padding(16.dp)
                .size(48.dp),
            tint = LocalContentColor.current.copy(alpha = 0.5f)
        )

        val focusManager = LocalFocusManager.current
        var currentLogin by rememberSerializableState(model.initialFormInput) { mutableStateOf(model.initialFormInput) }
        var autoLogin by rememberSaveable(model.autoLogin) { mutableStateOf(model.autoLogin) }
        OutlinedTextField(
            value = currentLogin.username,
            onValueChange = { currentLogin = currentLogin.copy(username = it) },
            Modifier.autofill(
                listOf(AutofillType.Username),
                onFill = { currentLogin = currentLogin.copy(username = it) }
            ),
            label = { Text("Nutzername") },
            leadingIcon = { Icon(Icons.Default.Person, "Nutzername") },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            ),
            keyboardActions = KeyboardActions(onNext = { focusManager.moveFocus(FocusDirection.Next) }),
            singleLine = true
        )
        var showPassword by remember { mutableStateOf(false) }
        OutlinedTextField(
            value = currentLogin.password,
            onValueChange = { currentLogin = currentLogin.copy(password = it) },
            Modifier.autofill(
                listOf(AutofillType.Password),
                onFill = { currentLogin = currentLogin.copy(password = it) }
            ),
            label = { Text("Passwort") },
            leadingIcon = { Icon(Icons.Default.Password, "Passwort") },
            trailingIcon = {
                IconButton(onClick = { showPassword = !showPassword }) {
                    Icon(
                        if (showPassword) Icons.Default.Visibility else Icons.Default.VisibilityOff,
                        if (showPassword) "Hide password" else "Show password"
                    )
                }
            },
            visualTransformation = if (showPassword) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(onDone = {
                model.login(
                    currentLogin,
                    autoLogin,
                    activityContext
                )
            }),
            singleLine = true
        )
        ListItem(
            headlineContent = {
                Text(
                    "Automatisch einloggen",
                    style = MaterialTheme.typography.labelLarge
                )
            },
            trailingContent = {
                Switch(
                    checked = autoLogin,
                    onCheckedChange = { autoLogin = it }
                )
            }
        )

        Button(
            onClick = { model.login(currentLogin, autoLogin, activityContext) },
            Modifier.align(Alignment.End),
            enabled = currentLogin.isValid
        ) {
            Text("Einloggen")
        }

        val errors = model.loginErrors
        if (errors != null) FailureRetryScreen(
            message = "Failed to login",
            Modifier.padding(top = 16.dp),
            exceptions = errors,
            onRetry = null
        ) else Spacer(Modifier.height(48.dp + 16.dp))
    }
}

