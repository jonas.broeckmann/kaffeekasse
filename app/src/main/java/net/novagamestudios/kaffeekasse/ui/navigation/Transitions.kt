package net.novagamestudios.kaffeekasse.ui.navigation

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabNavigator
import cafe.adriel.voyager.transitions.ScreenTransitionContent
import net.novagamestudios.kaffeekasse.ui.theme.ifAnimationsEnabled

private fun appTransitionSpec() = ContentTransform(
    targetContentEnter = fadeIn(
        tween(durationMillis = 220, delayMillis = 90)
    ) + scaleIn(
        tween(durationMillis = 220, delayMillis = 90),
        initialScale = 0.92f
    ),
    initialContentExit = fadeOut(
        tween(durationMillis = 90)
    )
)

@Composable
fun AppScreenTransition(
    navigator: Navigator,
    modifier: Modifier = Modifier,
    key: String = "app-screen-transition",
    transitionSpec: AnimatedContentTransitionScope<Screen>.() -> ContentTransform = { appTransitionSpec() },
    content: ScreenTransitionContent = { it.Content() }
) = AnimatedContent(
    targetState = navigator.lastItem,
    transitionSpec = ifAnimationsEnabled(transitionSpec),
    modifier = modifier,
    label = key
) { screen ->
    navigator.saveableState("$key-saveable", screen) {
        content(screen)
    }
}

@Composable
fun AppTabTransition(
    tabNavigator: TabNavigator,
    modifier: Modifier = Modifier,
    key: String = "app-tab-transition",
    transitionSpec: AnimatedContentTransitionScope<Tab>.() -> ContentTransform = { appTransitionSpec() },
    content: @Composable AnimatedVisibilityScope.(Tab) -> Unit = { it.Content() }
) = AnimatedContent(
    targetState = tabNavigator.current,
    transitionSpec = ifAnimationsEnabled(transitionSpec),
    modifier = modifier,
    label = key
) { tab ->
    tabNavigator.saveableState("$key-saveable", tab) {
        content(tab)
    }
}