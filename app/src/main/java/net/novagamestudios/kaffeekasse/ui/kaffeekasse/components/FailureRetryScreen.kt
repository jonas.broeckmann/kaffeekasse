package net.novagamestudios.kaffeekasse.ui.kaffeekasse.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedIconButton
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import net.novagamestudios.common_utils.compose.components.ColumnCenter
import net.novagamestudios.kaffeekasse.ui.util.DebugDataText

@Composable
fun FailureRetryScreen(
    message: String,
    modifier: Modifier = Modifier,
    errorsShort: List<String> = emptyList(),
    errorDetail: @Composable (() -> Unit)? = null,
    onRetry: (() -> Unit)? = null
) = ColumnCenter(modifier) {
    if (onRetry != null) OutlinedIconButton(
        onClick = onRetry,
        Modifier.padding(bottom = 16.dp)
    ) {
        Icon(Icons.Default.Refresh, "Retry")
    }
    Text(
        message,
        color = MaterialTheme.colorScheme.error,
        textAlign = TextAlign.Center
    )
    if (errorsShort.isNotEmpty()) {
        Spacer(Modifier.height(8.dp))
        errorsShort.forEach {
            Text(
                it,
                color = MaterialTheme.colorScheme.error,
                fontFamily = FontFamily.Monospace,
                textAlign = TextAlign.Center
            )
        }
    }

    if (errorDetail != null) {
        Spacer(Modifier.height(8.dp))
        var showDetailsDialog by remember { mutableStateOf(false) }
        TextButton(onClick = { showDetailsDialog = true }) { Text("Details") }
        if (showDetailsDialog) AlertDialog(
            onDismissRequest = { showDetailsDialog = false },
            confirmButton = {
                Button(onClick = { showDetailsDialog = false }) { Text("OK") }
            },
            modifier,
            text = { errorDetail() }
        )
    }
}

@Composable
fun FailureRetryScreen(
    message: String,
    modifier: Modifier = Modifier,
    exceptions: List<Exception> = emptyList(),
    errorsShort: List<String> = exceptions.map { it.message ?: it::class.simpleName ?: "Unknown error" },
    additionalDetails: Any? = null,
    onRetry: (() -> Unit)? = null
) = FailureRetryScreen(
    message = message,
    modifier = modifier,
    errorsShort = errorsShort,
    errorDetail = if (exceptions.isNotEmpty()) {
        {
            ProvideTextStyle(MaterialTheme.typography.bodySmall) {
                Column(
                    Modifier.verticalScroll(rememberScrollState()),
                    verticalArrangement = Arrangement.spacedBy(16.dp)
                ) {
                    exceptions.forEach {
                        DebugDataText(it.stackTraceToString(),)
                    }
                    if (additionalDetails != null) {
                        DebugDataText(additionalDetails)
                    }
                }
            }
        }
    } else null
)

