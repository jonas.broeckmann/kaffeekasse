package net.novagamestudios.kaffeekasse.ui

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Update
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.core.toastLong
import net.novagamestudios.common_utils.compose.DashedShape
import net.novagamestudios.common_utils.compose.Toasts
import net.novagamestudios.common_utils.compose.ToastsState
import net.novagamestudios.common_utils.compose.components.CircularLoadingBox
import net.novagamestudios.common_utils.compose.components.ColumnCenter
import net.novagamestudios.common_utils.compose.components.LinearProgressIndicator
import net.novagamestudios.common_utils.compose.components.RowCenter
import net.novagamestudios.common_utils.compose.components.TransparentListItem
import net.novagamestudios.common_utils.compose.state.ReentrantActionState
import net.novagamestudios.common_utils.compose.state.collectAsStateIn
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.voyager.model.GlobalScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.common_utils.voyager.model.getValue
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.App.Companion.settings
import net.novagamestudios.kaffeekasse.BuildConfig
import net.novagamestudios.kaffeekasse.model.app.AppRelease
import net.novagamestudios.kaffeekasse.model.app.AppVersion
import net.novagamestudios.kaffeekasse.model.date_time.format
import net.novagamestudios.kaffeekasse.repositories.InstallStatus
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.UpdateController
import net.novagamestudios.kaffeekasse.repositories.releases.Releases
import net.novagamestudios.kaffeekasse.ui.util.openInBrowser
import java.time.format.DateTimeFormatter


class UpdatesScreenModel private constructor(
    private val releases: Releases,
    private val updateController: UpdateController,
    private val gitLabProjectUrl: String
) : ScreenModel, Logger {
    var showAppInfo by mutableStateOf(false)

    private val checkingMutex = ReentrantActionState()
    val isChecking by checkingMutex

    val toasts = ToastsState()

    val newerReleases by releases.newerReleases
        .map { it.orEmpty() }
        .collectAsStateIn(screenModelScope, emptyList())
    var showReleasesDialog by mutableStateOf(false)

    private val installMutex = ReentrantActionState()

    val installStatus by updateController.installStatus.collectAsStateIn(screenModelScope)
    val isInstalling by derivedStateOf { installStatus != null }
    val isInstallDismissible by derivedStateOf { installStatus !is InstallStatus.InProgress }

    init {
        screenModelScope.launch {
            snapshotFlow { newerReleases }.collect {
                showReleasesDialog = it.isNotEmpty()
            }
        }
    }

    fun checkForUpdates() {
        screenModelScope.launch {
            checkingMutex.trueWhile {
                releases.newerReleases.value = null
                try {
                    releases.fetchNewerReleases()
                } catch (e: Throwable) {
                    error (e) { "Failed to fetch newer releases" }
                    toasts.short("Failed to check for updates: ${e.message}")
                }
            }
        }
    }

    fun update() {
        val release = newerReleases.firstOrNull() ?: return
        screenModelScope.launch {
            showReleasesDialog = false
            installMutex.trueWhile {
                updateController.updateApp(release)
            }
        }
    }
    fun retryUpdate() {
        if (!tryDismissInstall()) return
        update()
    }
    fun tryDismissInstall(): Boolean {
        if (!isInstallDismissible) return false
        updateController.installStatus.value = null
        return true
    }

    fun openInBrowser(context: Context) {
        context.openInBrowser(gitLabProjectUrl)
    }

    companion object : GlobalScreenModelFactory<RepositoryProvider, App, UpdatesScreenModel>,
        ScreenModelProvider<UpdatesScreenModel> {
        context (RepositoryProvider)
        override fun create(app: App) = UpdatesScreenModel(
            releases = releases,
            updateController = updateController,
            gitLabProjectUrl = app.gitLab.projectUrl
        )
        @get:Composable override val model by this
    }
}


@Composable
fun UpdateDialogs(model: UpdatesScreenModel = UpdatesScreenModel.model) {
    val context = LocalContext.current
    if (model.showAppInfo) AppInfoDialog(
        onDismissRequest = { model.showAppInfo = false },
        isCheckingForUpdates = model.isChecking,
        onCheckForUpdates = { model.checkForUpdates() },
        onOpenInBrowser = { model.openInBrowser(context) }
    )
    if (model.isInstalling) InstallDialog(model)
    else if (model.newerReleases.isNotEmpty() && model.showReleasesDialog) NewerReleasesDialog(
        onDismissRequest = { model.showReleasesDialog = false },
        newerReleases = model.newerReleases,
        onStartUpdate = { model.update() }
    )
    Toasts(model.toasts)
}

@Composable
private fun NewerReleasesDialog(
    onDismissRequest: () -> Unit,
    newerReleases: List<AppRelease>,
    onStartUpdate: () -> Unit
) = AlertDialog(
    onDismissRequest = onDismissRequest,
    confirmButton = {
        Button(onClick = onStartUpdate) {
            Text("Install ${newerReleases.first().version}")
        }
    },
    dismissButton = {
        TextButton(onClick = onDismissRequest) {
            Text("OK")
        }
    },
    title = {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Icon(Icons.Default.Update, null)
            Text("Update available")
        }
    },
    text = {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            var first = true
            newerReleases.forEach { release ->
                if (!first) HorizontalDivider(
                    Modifier
                        .padding(vertical = 16.dp)
                        .clip(DashedShape(4.dp)))
                first = false
                Release(release)
            }
        }
    }
)

@Composable
private fun Release(
    release: AppRelease
) = Column {
    Text("Release ${release.version}", style = MaterialTheme.typography.headlineMedium)
    val date = remember(release) { release.createdAt.format(releaseDateFormatter) }
    Text(date, style = MaterialTheme.typography.bodySmall)
    release.description?.let {
        Text(it)
    }
}

private val releaseDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

@Composable
private fun InstallDialog(
    model: UpdatesScreenModel
) = AlertDialog(
    onDismissRequest = { model.tryDismissInstall() },
    confirmButton = {
        if (model.installStatus is InstallStatus.Failure) TextButton(onClick = { model.retryUpdate() }) {
            Text("Retry")
        }
    },
    dismissButton = {
        if (model.installStatus is InstallStatus.Success) TextButton(onClick = { model.tryDismissInstall() }) {
            Text("Cool")
        } else if (model.isInstallDismissible) TextButton(onClick = { model.tryDismissInstall() }) {
            Text("Cancel")
        }
    },
    title = {
        when (model.installStatus) {
            is InstallStatus.InProgress -> Text("Installing update")
            is InstallStatus.Success -> Text("Success")
            is InstallStatus.Failure -> Text("Failure")
            null -> { }
        }
    },
    text = {
        Column(verticalArrangement = Arrangement.spacedBy(8.dp)) {
            when (val status = model.installStatus) {
                is InstallStatus.InProgress.Installing -> {
                    Text("Installing")
                    LinearProgressIndicator(status.progress)
                }
                is InstallStatus.InProgress.Downloading -> {
                    Text("Downloading")
                    LinearProgressIndicator(status.progress)
                }
                is InstallStatus.InProgress.PendingUserAction -> {
                    Text("Please allow the installation")
                }
                is InstallStatus.Success -> {
                    Text("Installation was successful")
                }
                is InstallStatus.Failure -> {
                    val reason = status.reason
                    if (reason != null) Text(reason)
                    if (status is InstallStatus.Failure.Aborted) Text("You may try again")
                }
                null -> { }
            }
        }
    },
    properties = DialogProperties(
        dismissOnBackPress = false,
        dismissOnClickOutside = false
    )
)


@Composable
fun AppInfoTopBarAction(model: UpdatesScreenModel = UpdatesScreenModel.model) {
    IconButton(onClick = { model.showAppInfo = true }) {
        Icon(Icons.Default.Info, "Info")
    }
}

@Composable
private fun AppInfoDialog(
    onDismissRequest: () -> Unit,
    isCheckingForUpdates: Boolean,
    onCheckForUpdates: () -> Unit,
    onOpenInBrowser: () -> Unit
) = AlertDialog(
    onDismissRequest = onDismissRequest,
    confirmButton = { TextButton(onClick = onDismissRequest) { Text("OK") } },
    dismissButton = {
        CircularLoadingBox(loading = isCheckingForUpdates) {
            OutlinedButton(onClick = onCheckForUpdates, enabled = !isCheckingForUpdates) {
                Text("Check for updates")
            }
        }
    },
    modifier = Modifier.widthIn(max = 400.dp),
    icon = { Icon(Icons.Default.Info, null) },
    title = { Text("Kaffeekasse") },
    text = {
        ColumnCenter(
            Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState())
        ) {
            val versionText = listOfNotNull(
                "Version: ${AppVersion.Current}",
                "(debug)".takeIf { BuildConfig.DEBUG }
            ).joinToString(" ")
            Text(versionText, style = MaterialTheme.typography.titleMedium)
            Spacer(Modifier.height(4.dp))
            RowCenter(
                Modifier
                    .clip(RoundedCornerShape(50))
                    .clickable { onOpenInBrowser() }
                    .padding(4.dp),
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Icon(Icons.Default.Code, "Code")
                Text(
                    "Source code on GitLab",
                    textDecoration = TextDecoration.Underline,
                    style = MaterialTheme.typography.bodyMedium
                )
            }
            Spacer(Modifier.height(16.dp))
            Text("Made with love\nby\nJonas Broeckmann", textAlign = TextAlign.Center)
            Spacer(Modifier.height(16.dp))
            val settings = settings()
            TransparentListItem(
                headlineContent = { Text("Fancy animations") },
                trailingContent = { Switch(
                    settings.value.animationsEnabled,
                    onCheckedChange = { new -> settings.tryUpdate { it.copy(animationsEnabled = new) } }
                ) }
            )
            TransparentListItem(
                headlineContent = { Text("Developer mode") },
                trailingContent = { Switch(
                    settings.value.developerMode,
                    onCheckedChange = { new -> settings.tryUpdate { it.copy(developerMode = new) } }
                ) }
            )
            Spacer(Modifier.height(16.dp))
            Row(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
                val context = LocalContext.current
                TextButton(onClick = {
                    try {
                        @Suppress("ControlFlowWithEmptyBody") while (true) { }
                    } catch (e: Throwable) {
                        e.printStackTrace()
                        context.toastLong(e.toString())
                    }
                }) {
                    Text("Busy waiting")
                }
                TextButton(onClick = {
                    try {
                        val f: ((Any) -> Unit) -> Unit = { it(it) }
                        @Suppress("UNCHECKED_CAST") f(f as (Any) -> Unit)
                    } catch (e: Throwable) {
                        e.printStackTrace()
                        context.toastLong(e.toString())
                    }
                }) {
                    Text("Recursion")
                }
            }
        }
    }
)

