package net.novagamestudios.kaffeekasse.ui.login

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Password
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.DividerDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import cafe.adriel.voyager.navigator.Navigator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import net.novagamestudios.common_utils.compose.Toasts
import net.novagamestudios.common_utils.compose.ToastsState
import net.novagamestudios.common_utils.compose.components.LinearProgressIndicator
import net.novagamestudios.common_utils.compose.components.RowCenter
import net.novagamestudios.common_utils.compose.logging.warn
import net.novagamestudios.common_utils.compose.state.rememberDerivedStateOf
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.R
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.BasicUserInfo
import net.novagamestudios.kaffeekasse.data.cleanName
import net.novagamestudios.kaffeekasse.model.kaffeekasse.UserAuthCredentials
import net.novagamestudios.kaffeekasse.model.session.Device
import net.novagamestudios.kaffeekasse.repositories.LoginRepository
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.ui.AppInfoTopBarAction
import net.novagamestudios.kaffeekasse.ui.FullscreenIconButton
import net.novagamestudios.kaffeekasse.ui.handleSession
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.LoginNavigation
import net.novagamestudios.kaffeekasse.ui.util.AlphabetSelectionChar
import net.novagamestudios.kaffeekasse.ui.util.FailureRetryScreen
import net.novagamestudios.kaffeekasse.ui.util.HorizontalSelectionBar
import net.novagamestudios.kaffeekasse.ui.util.PullToRefreshIndicator
import net.novagamestudios.kaffeekasse.ui.util.RichDataContent
import net.novagamestudios.kaffeekasse.ui.util.TopBarSearchAction
import net.novagamestudios.kaffeekasse.ui.util.TopBarSearchField
import net.novagamestudios.kaffeekasse.ui.util.TopBarSearchFieldState
import net.novagamestudios.kaffeekasse.ui.util.VerticalSelectionBar
import net.novagamestudios.kaffeekasse.ui.util.pullToRefresh
import net.novagamestudios.kaffeekasse.util.richdata.asRichDataFlow
import net.novagamestudios.kaffeekasse.util.richdata.combineRich
import net.novagamestudios.kaffeekasse.util.richdata.stateIn
import net.novagamestudios.kaffeekasse.util.richdata.withFunctions


class UserSelectionScreenModel private constructor(
    val device: Device,
    val loginRepository: LoginRepository,
    kaffeekasse: KaffeekasseRepository
) : ScreenModel, Logger {
    val users = kaffeekasse.basicUserInfoList

    val searchState = TopBarSearchFieldState()
    
    val filteredUsers = run {
        val data = combineRich(
            snapshotFlow { searchState.queryOrBlank.lowercase() }.asRichDataFlow(),
            users,
            loadDuringTransform = true
        ) { query, users ->
            val sorted = users.asSequence().sortedBy { it.name }
            if (query.isBlank()) return@combineRich sorted.toList()
            sorted.filter { query in it.name.firstLast.lowercase() }.toList()
        }
            .flowOn(Dispatchers.IO)
            .stateIn(screenModelScope)
        data withFunctions users
    }

    var userAuthDialog by mutableStateOf<UserAuthDialogState?>(null)

    val toasts = ToastsState()

    data class UserAuthDialogState(
        val user: BasicUserInfo,
        val pin: String = ""
    ) {
        val auth get() = UserAuthCredentials(pin = pin.takeIf { it.isNotBlank() })
    }

    fun loginUser(
        user: BasicUserInfo,
        auth: UserAuthCredentials = UserAuthCredentials.Empty,
        navigator: Navigator? = null
    ) {
        if (user.mayHavePin && auth.pin == null) {
            userAuthDialog = UserAuthDialogState(user)
            return
        }
        screenModelScope.launch {
            when (val result = loginRepository.login(user, auth)) {
                is LoginRepository.LoginResult.Success -> {
                    userAuthDialog = null
                    navigator?.handleSession(result.session)
                }
                is LoginRepository.LoginResult.Failure -> {
                    toasts.short(result.error)
                }
            }
        }
    }

    fun loginUser(dialog: UserAuthDialogState) {
        loginUser(dialog.user, dialog.auth)
    }

    companion object : ScreenModelFactory<RepositoryProvider, LoginNavigation.UserSelectionScreen, UserSelectionScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: LoginNavigation.UserSelectionScreen) = UserSelectionScreenModel(
            device = screen.device,
            loginRepository = loginRepository,
            kaffeekasse = kaffeekasseRepository,
        )
    }
}


class UserSelectionContent(provider: ScreenModelProvider<UserSelectionScreenModel>) : AppScaffoldContentWithModel<UserSelectionScreenModel>(provider) {
    @Composable
    override fun TopAppBarTitle(navigator: Navigator) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            if (model.searchState.show) {
                TopBarSearchField(model.searchState)
            } else {
                Image(
                    painterResource(R.drawable.logo_edited),
                    "Kaffeekasse",
                    Modifier.size(32.dp)
                )
                DeviceInfo(model.device, Modifier.padding(16.dp))
            }
        }
    }

    @Composable
    override fun TopAppBarActions(navigator: Navigator) {
        TopBarSearchAction(model.searchState, alwaysShow = true)
        AppInfoTopBarAction()
        LogoutDeviceButton()
        FullscreenIconButton()
    }

    @Composable
    override fun backNavigationHandler(navigator: Navigator): BackNavigationHandler {
        val model = model
        return remember { model.searchState.backNavigation }
    }
}



@Composable
fun UserSelection(
    model: UserSelectionScreenModel,
    modifier: Modifier = Modifier
) {
    val pullToRefreshState = rememberPullToRefreshState()
    Box(
        modifier
            .fillMaxSize()
            .clip(RectangleShape)
            .pullToRefresh(model.users, pullToRefreshState),
        contentAlignment = Alignment.TopCenter
    ) {
//        UserSearchBar(
//            query = model.searchQuery,
//            onQueryChange = { model.searchQuery = it },
//            onSearch = {
//                model.filteredUsers.value
//                    .dataOrNull
//                    ?.singleOrNull()
//                    ?.let { model.loginUser(it) }
//            },
//            Modifier
//                .padding(8.dp)
//                .align(Alignment.TopCenter)
//        )
        RichDataContent(
            source = model.filteredUsers,
            errorContent = { error ->
                warn { "Failed to load users: ${error.messages}" }
                FailureRetryScreen(
                    error = error,
                    message = "Failed to load users",
                    modifier = Modifier.fillMaxSize()
                )
            },
            loadingContent = { progress ->
                LinearProgressIndicator(
                    progress,
                    Modifier
                        .padding(top = 8.dp)
                        .align(Alignment.BottomCenter)
                        .fillMaxWidth()
                )
            },
            dataContent = { users ->
                UserGridWithSelectionBar(
                    users = users,
                    onClick = { model.loginUser(it) },
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        )
        PullToRefreshIndicator(
            data = model.users,
            state = pullToRefreshState,
            Modifier.align(Alignment.TopCenter)
        )
    }
    model.userAuthDialog?.let { state ->
        UserAuthDialog(
            state,
            onChange = { model.userAuthDialog = it },
            onDismiss = { model.userAuthDialog = null },
            onSubmit = { model.loginUser(state) }
        )
    }
    Toasts(model.toasts)
}


@Composable
private fun UserGridWithSelectionBar(
    users: List<BasicUserInfo>,
    onClick: (BasicUserInfo) -> Unit,
    modifier: Modifier = Modifier
) {
//    val searchBarOffset = SearchBarDefaults.InputFieldHeight
    val searchBarOffset = 0.dp

    val gridState = rememberLazyGridState()
    val coroutineScope = rememberCoroutineScope()

    val currentStartChars: Set<Char> by rememberDerivedStateOf {
        val start = gridState.layoutInfo.viewportStartOffset + gridState.layoutInfo.beforeContentPadding
        val end = gridState.layoutInfo.viewportEndOffset - gridState.layoutInfo.afterContentPadding
        gridState.layoutInfo.visibleItemsInfo
            .asSequence()
            .filter {
                val mid = it.offset.y + (it.size.height / 2)
                mid in start..<end
            }
            .mapNotNull { it.contentType as? Int }
            .mapNotNullTo(mutableSetOf()) { users.getOrNull(it)?.name?.char }
    }
    val userIndexAndCharIndexByChar: Map<Char, Pair<Int, Int>> by rememberDerivedStateOf {
        users.asSequence()
            .withIndex()
            .map { (userIndex, user) ->
                user.name.char to userIndex
            }
            .distinctBy { (char, _) -> char }
            .withIndex()
            .associate { (charIndex, pair) ->
                val (char, userIndex) = pair
                char to Pair(userIndex, charIndex)
            }
    }
    val chars = remember { ('A'..'Z').toList() }

    @Composable
    fun Grid(modifier: Modifier = Modifier) = UserGrid(
        gridState = gridState,
        users = users,
        onClick = onClick,
        topOffset = searchBarOffset,
        modifier
    )

    val onSelectIndex: (Int) -> Unit = block@{ index ->
        val (userIndex, charIndex) = (userIndexAndCharIndexByChar[chars[index.coerceIn(chars.indices)]] ?: return@block)
        val sectionIndex = userIndex + charIndex
        coroutineScope.launch {
            gridState.scrollToItem(sectionIndex)
        }
    }

    val itemContent: @Composable (Int) -> Unit = { i ->
        val char = chars[i]
        AlphabetSelectionChar(
            char,
            highlighted = char in currentStartChars,
            enabled = char in userIndexAndCharIndexByChar
        )
    }


    when {
        users.isEmpty() -> Text("Niemand hat diesen Namen 🤔", modifier)
        LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE -> Column(modifier) {
            Grid(Modifier.weight(1f))
            HorizontalDivider()
            HorizontalSelectionBar(
                itemCount = chars.size,
                onSelect = onSelectIndex,
                itemContent = itemContent
            )
        }
        else -> Row(modifier) {
            VerticalSelectionBar(
                itemCount = chars.size,
                onSelect = onSelectIndex,
                Modifier.padding(top = searchBarOffset),
                itemContent = itemContent
            )
            Grid(Modifier.weight(1f))
        }
    }
}


@Composable
private fun UserGrid(
    gridState: LazyGridState,
    users: List<BasicUserInfo>,
    onClick: (BasicUserInfo) -> Unit,
    topOffset: Dp,
    modifier: Modifier = Modifier
) = LazyVerticalGrid(
    columns = GridCells.Adaptive(minSize = 250.dp),
    modifier,
    state = gridState,
    contentPadding = PaddingValues(
        start = 8.dp,
        top = 12.dp + topOffset,
        end = 8.dp,
        bottom = 12.dp
    ),
    horizontalArrangement = Arrangement.Center
) {
    var prevChar: Char? = null
    users.forEachIndexed { i, user ->
        val char = user.name.char
        if (prevChar != char) {
            item(
                key = char,
                span = { GridItemSpan(maxLineSpan) },
                contentType = char
            ) {
                Row(
                    Modifier.padding(6.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        "$char",
                        Modifier.padding(horizontal = 6.dp),
                        color = DividerDefaults.color,
                        fontSize = 24.sp,
                        fontWeight = FontWeight.Bold
                    )
                    HorizontalDivider(thickness = 1.dp)
                }
            }
        }
        prevChar = char
        item(
            key = i,
            contentType = i
        ) {
            UserItem(
                user = user,
                onClick = { onClick(user) },
                Modifier
                    .padding(6.dp)
                    .widthIn(max = 300.dp)
            )
        }
    }
}


@Composable
private fun UserItem(
    user: BasicUserInfo,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) = OutlinedCard(
    onClick = onClick,
    modifier
) {
    RowCenter(
        Modifier
            .height(56.dp)
            .padding(horizontal = 16.dp)
    ) {
        if (App.developerMode) Text(
            "${user.id}",
            Modifier.padding(end = 8.dp),
            style = MaterialTheme.typography.labelSmall
        )
        ProvideTextStyle(MaterialTheme.typography.titleMedium) {
            val (lastName, firstName) = remember(user) {
                val (last, first) = user.name
                last to first
            }
            Text(lastName)
            if (firstName != null) {
                Text(",")
                Spacer(Modifier.width(8.dp))
                Text(firstName)
            }
        }
        Spacer(Modifier.weight(1f))
        if (user.noPinSet == false) {
            Icon(Icons.Default.Lock, "No PIN set")
        }
    }
}


@Composable
private fun UserAuthDialog(
    state: UserSelectionScreenModel.UserAuthDialogState,
    onChange: (UserSelectionScreenModel.UserAuthDialogState) -> Unit,
    onDismiss: () -> Unit,
    onSubmit: () -> Unit,
    modifier: Modifier = Modifier
) = AlertDialog(
    onDismissRequest = { onDismiss() },
    confirmButton = {
        Button(
            onClick = onSubmit,
            enabled = state.pin.isNotBlank()
        ) { Text("Einloggen") }
    },
    modifier,
    dismissButton = {
        TextButton(onClick = onDismiss) { Text("Abbrechen") }
    },
    icon = { Icon(Icons.Default.Password, "PIN") },
    title = { Text(state.user.name.firstLast) },
    text = {
        val focusRequester = remember { FocusRequester() }
        OutlinedTextField(
            state.pin,
            onValueChange = { onChange(state.copy(pin = it)) },
            Modifier.focusRequester(focusRequester),
            label = { Text("PIN") },
            leadingIcon = { Icon(Icons.Default.Lock, "PIN") },
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { onSubmit() }
            ),
            singleLine = true
        )
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
    }
)


@Composable
private fun DeviceInfo(
    device: Device,
    modifier: Modifier = Modifier
) = Row(
    modifier,
    verticalAlignment = Alignment.CenterVertically
) {
    Text(device.name ?: "Unbekanntes Gerät")
    device.knownItemGroup?.let {
        ProvideTextStyle(MaterialTheme.typography.titleMedium) {
            Spacer(Modifier.width(16.dp))
            Icon(Icons.Default.ChevronRight, null)
            Text(
                it.cleanName,
                style = MaterialTheme.typography.titleMedium
            )
        }
    }
}

