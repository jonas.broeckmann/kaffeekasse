package net.novagamestudios.kaffeekasse.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CloseFullscreen
import androidx.compose.material.icons.filled.OpenInFull
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.compose.logging.verbose
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.LoggerForFun
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.voyager.model.GlobalScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.common_utils.voyager.model.getValue
import net.novagamestudios.common_utils.voyager.requireWithKey
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.PortalRepository
import net.novagamestudios.kaffeekasse.ui.navigation.AppModulesScreen
import net.novagamestudios.kaffeekasse.ui.navigation.AppScreenTransition
import net.novagamestudios.kaffeekasse.ui.navigation.LoginNavigation
import net.novagamestudios.kaffeekasse.ui.theme.KaffeekasseTheme


class AppScreenModel private constructor(
    private val portal: PortalRepository
) : ScreenModel, Logger {

    val session by portal.session

    companion object : GlobalScreenModelFactory<RepositoryProvider, App, AppScreenModel>, ScreenModelProvider<AppScreenModel> {
        context (RepositoryProvider)
        override fun create(app: App) = AppScreenModel(
            portal = portalRepository
        )
        @get:Composable override val model by this
    }
}


@Composable
fun App(
    model: AppScreenModel = AppScreenModel.model
) = KaffeekasseTheme {
    verbose { "recompose app" }
    Navigator(
        LoginNavigation,
        key = NavigatorKey
    ) { navigator ->
        if (navigator.lastItem matches model.session) {
            AppScreenTransition(
                navigator = navigator,
                Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .fillMaxSize(),
            )
        } else {
            autoNavigateLogin(navigator, model)
            Surface {
                BoxCenter(Modifier.fillMaxSize()) {
                    CircularProgressIndicator()
                }
            }
        }
    }
    UpdateDialogs()
}

private infix fun Screen.matches(session: Session) = when (this) {
    is LoginNavigation -> session !is Session.WithRealUser
    is AppModulesScreen -> session is Session.WithRealUser
    else -> false
}

private const val NavigatorKey = "app-navigator"

@Composable
private fun autoNavigateLogin(
    navigator: Navigator,
    model: AppScreenModel
) {
    navigator.handleSession(model.session)
}


fun Navigator.handleSession(session: Session) {
    val logger = LoggerForFun()
    val appNavigator = requireWithKey(NavigatorKey)
    when (appNavigator.lastItem) {
        is LoginNavigation -> {
            if (session is Session.WithRealUser) {
                logger.debug { "Auto-navigate to AppModulesScreen because $session" }
                appNavigator.replaceAll(AppModulesScreen(session))
            }
        }
        is AppModulesScreen -> {
            if (session !is Session.WithRealUser) {
                logger.debug { "Auto-navigate to LoginNavigation because $session" }
                appNavigator.replaceAll(LoginNavigation)
            }
        }
    }
}


@Composable
fun FullscreenIconButton(
    modifier: Modifier = Modifier
) {
    val settings = App.settings()
    IconButton(
        onClick = { settings.tryUpdate { it.copy(fullscreen = !it.fullscreen) } },
        modifier
    ) {
        if (!settings.value.fullscreen) {
            Icon(Icons.Default.OpenInFull, "Vollbild")
        } else {
            Icon(Icons.Default.CloseFullscreen, "Vollbild verlassen")
        }
    }
}


