package net.novagamestudios.kaffeekasse.ui.kaffeekasse

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.navigator.Navigator
import net.novagamestudios.common_utils.compose.components.CircularProgressIndicator
import net.novagamestudios.common_utils.compose.components.ColumnCenter
import net.novagamestudios.common_utils.core.format
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.voyager.BackNavigationHandler
import net.novagamestudios.common_utils.voyager.model.ScreenModelFactory
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.kaffeekasse.App
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Account
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.i11.KaffeekasseRepository
import net.novagamestudios.kaffeekasse.ui.AppInfoTopBarAction
import net.novagamestudios.kaffeekasse.ui.login.LogoutTopBarAction
import net.novagamestudios.kaffeekasse.ui.navigation.AppScaffoldContentWithModel
import net.novagamestudios.kaffeekasse.ui.navigation.AppSubpageTitle
import net.novagamestudios.kaffeekasse.ui.navigation.KaffeekasseNavigation
import net.novagamestudios.kaffeekasse.ui.util.DebugDataText
import net.novagamestudios.kaffeekasse.ui.util.FailureRetryScreen
import net.novagamestudios.kaffeekasse.ui.util.PullToRefreshBox
import net.novagamestudios.kaffeekasse.ui.util.RichDataContent
import net.novagamestudios.kaffeekasse.util.richdata.collectAsRichStateHere

class AccountScreenModel private constructor(
    val session: Session.WithRealUser,
    private val kaffeekasse: KaffeekasseRepository
) : ScreenModel, Logger {

    val account = kaffeekasse.account[session.realUser].collectAsRichStateHere()

    companion object : ScreenModelFactory<RepositoryProvider, KaffeekasseNavigation.AccountScreen, AccountScreenModel> {
        context (RepositoryProvider)
        override fun create(screen: KaffeekasseNavigation.AccountScreen) = AccountScreenModel(
            session = screen.session,
            kaffeekasse = kaffeekasseRepository
        )
    }
}

class AccountContent(provider: ScreenModelProvider<AccountScreenModel>) : AppScaffoldContentWithModel<AccountScreenModel>(provider) {
    @Composable
    override fun TopAppBarTitle(navigator: Navigator) {
        AppSubpageTitle("Konto")
    }

    @Composable
    override fun TopAppBarActions(navigator: Navigator) {
        AppInfoTopBarAction()
        LogoutTopBarAction(model.session)
    }

    @Composable
    override fun backNavigationHandler(navigator: Navigator): BackNavigationHandler {
        return BackNavigationHandler.default()
    }
}


@Composable
fun Account(
    model: AccountScreenModel,
    modifier: Modifier = Modifier
) = PullToRefreshBox(
    data = model.account,
    modifier.fillMaxSize()
) {
    ColumnCenter(Modifier.fillMaxSize().verticalScroll(rememberScrollState())) {
        RichDataContent(
            state = model.account,
            errorContent = { error ->
                FailureRetryScreen(
                    error = error,
                    message = "Failed to fetch account",
                    Modifier.fillMaxSize()
                )
            },
            loadingContent = { progress ->
                CircularProgressIndicator(progress, Modifier.align(Alignment.CenterHorizontally))
            }
        ) { account ->
            AccountDetails(
                account = account,
                Modifier.align(Alignment.CenterHorizontally)
            )
        }
        if (App.developerMode) ColumnCenter(
            Modifier.padding(16.dp)
        ) {
            Text("Session", style = MaterialTheme.typography.headlineSmall)
            DebugDataText("${model.session.realUser}")
            DebugDataText("${model.session.data}")
        }
    }
}

@Composable
private fun AccountDetails(
    account: Account,
    modifier: Modifier = Modifier
) = ColumnCenter(
    modifier.width(OutlinedTextFieldDefaults.MinWidth)
) {
    Icon(
        imageVector = Icons.Default.Person,
        contentDescription = "Name",
        Modifier.size(48.dp)
    )
    Spacer(Modifier.height(16.dp))
    Text(
        "${account.firstName} ${account.lastName}",
        textAlign = TextAlign.Center,
        style = MaterialTheme.typography.headlineMedium
    )

    Spacer(Modifier.height(64.dp))

    ColumnCenter {
        Text(
            "Kontostand".uppercase(),
            style = MaterialTheme.typography.labelMedium
        )
        Text(
            "${account.total.format("%.2f")} €",
            Modifier.padding(8.dp),
            color = when {
                account.total > 0 -> Color.Green
                account.total < 0 -> Color.Red
                else -> Color.Unspecified
            },
            fontFamily = FontFamily.Monospace,
            style = MaterialTheme.typography.headlineMedium
        )
    }

    Spacer(Modifier.height(16.dp))

    Row {
        ColumnCenter(Modifier.weight(1f)) {
            Text(
                "Verzehrt".uppercase(),
                style = MaterialTheme.typography.labelMedium
            )
            Text(
                "${account.paid.format("%.2f")} €",
                Modifier.padding(8.dp),
                fontFamily = FontFamily.Monospace,
                style = MaterialTheme.typography.titleLarge
            )
        }

        Spacer(Modifier.width(16.dp))

        ColumnCenter(Modifier.weight(1f)) {
            Text(
                "Eingezahlt".uppercase(),
                style = MaterialTheme.typography.labelMedium
            )
            Text(
                "${account.deposited.format("%.2f")} €",
                Modifier.padding(8.dp),
                fontFamily = FontFamily.Monospace,
                style = MaterialTheme.typography.titleLarge
            )
        }
    }

    Spacer(Modifier.height(64.dp))
}
