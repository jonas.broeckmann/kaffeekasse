package net.novagamestudios.kaffeekasse.ui.util

import android.annotation.SuppressLint
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.spring
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.TargetedFlingBehavior
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.pager.PageSize
import androidx.compose.foundation.pager.PagerDefaults
import androidx.compose.foundation.pager.PagerScope
import androidx.compose.foundation.pager.PagerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt
import kotlin.reflect.KMutableProperty0


abstract class KeyedPagerState<K : Any>(
    currentPage: Int,
    currentPageOffsetFraction: Float = 0f
) : PagerState(
    currentPage = currentPage,
    currentPageOffsetFraction = currentPageOffsetFraction
) {

    protected abstract fun keyToIndex(key: K): Int
    internal abstract fun indexToKey(index: Int): K

    val currentPageKey: K get() = indexToKey(currentPage)
    val targetPageKey: K get() = indexToKey(targetPage)
    val settledPageKey: K get() = indexToKey(settledPage)

    suspend fun scrollToPage(
        page: K,
        pageOffsetFraction: Float = 0f
    ) = scrollToPage(keyToIndex(page), pageOffsetFraction)

    suspend fun animateScrollToPage(
        page: K,
        pageOffsetFraction: Float = 0f,
        animationSpec: AnimationSpec<Float> = spring()
    ) = animateScrollToPage(keyToIndex(page), pageOffsetFraction, animationSpec)

}


@Composable
fun <K : Any> HorizontalKeyedPager(
    state: KeyedPagerState<K>,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    pageSize: PageSize = PageSize.Fill,
    beyondViewportPageCount: Int = PagerDefaults.BeyondViewportPageCount,
    pageSpacing: Dp = 0.dp,
    verticalAlignment: Alignment.Vertical = Alignment.CenterVertically,
    flingBehavior: TargetedFlingBehavior = PagerDefaults.flingBehavior(state = state),
    userScrollEnabled: Boolean = true,
    reverseLayout: Boolean = false,
    pageNestedScrollConnection: NestedScrollConnection = PagerDefaults.pageNestedScrollConnection(
        state,
        Orientation.Horizontal
    ),
    pageContent: @Composable PagerScope.(page: K) -> Unit
) {
    androidx.compose.foundation.pager.HorizontalPager(
        state = state,
        modifier = modifier,
        contentPadding = contentPadding,
        pageSize = pageSize,
        beyondViewportPageCount = beyondViewportPageCount,
        pageSpacing = pageSpacing,
        verticalAlignment = verticalAlignment,
        flingBehavior = flingBehavior,
        userScrollEnabled = userScrollEnabled,
        reverseLayout = reverseLayout,
        key = { state.indexToKey(it) },
        pageNestedScrollConnection = pageNestedScrollConnection
    ) { page ->
        pageContent(state.indexToKey(page))
    }
}

@SuppressLint("ComposableNaming")
@Composable
fun <K : Any> synchronizePagerState(
    pagerState: KeyedPagerState<K>,
    getter: () -> K,
    setter: (K) -> Unit
) {
    LaunchedEffect(getter()) {
        pagerState.animateScrollToPage(getter())
    }
    LaunchedEffect(pagerState.currentPageKey) {
        setter(pagerState.currentPageKey)
    }
}

@SuppressLint("ComposableNaming")
@Composable
fun <K : Any> synchronizePagerState(
    pagerState: KeyedPagerState<K>,
    property: KMutableProperty0<K>
) = synchronizePagerState(
    pagerState = pagerState,
    getter = property::get,
    setter = property::set
)




@Composable
fun HorizontalPagedLayout(
    state: PagerState,
    modifier: Modifier = Modifier,
    content: @Composable (Int) -> Unit
) {
    val beyondCount = 1
    Layout(
        content = {
            for (i in state.currentPage - beyondCount..state.currentPage + beyondCount) {
                content(i)
            }
        },
        modifier
    ) { measurables, constraints ->
        val placeables = measurables.map { it.measure(constraints) }
        val startIndex = state.currentPage - beyondCount
        val currentIndex = state.currentPage - startIndex
        val width = placeables[currentIndex].width
        val height = placeables.maxOf { it.height }
        layout(width, height) {
            val offset = state.currentPageOffsetFraction * width
            var x = -beyondCount * width
            for (i in placeables.indices) {
                placeables[i].placeRelative(
                    x = (x - offset).roundToInt(),
                    y = 0
                )
                x += placeables[i].width
            }
        }
    }
}



