package net.novagamestudios.kaffeekasse.ui.navigation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabNavigator
import cafe.adriel.voyager.navigator.tab.TabOptions
import net.novagamestudios.common_utils.compose.components.BoxCenter
import net.novagamestudios.common_utils.voyager.model.ScreenModelProvider
import net.novagamestudios.common_utils.voyager.model.getValue
import net.novagamestudios.common_utils.voyager.scaffold.ScaffoldContentProvider
import net.novagamestudios.common_utils.voyager.screen.ScreenWithModel
import net.novagamestudios.kaffeekasse.model.session.Device
import net.novagamestudios.kaffeekasse.model.session.Session
import net.novagamestudios.kaffeekasse.model.session.deviceOrNull
import net.novagamestudios.kaffeekasse.ui.AppModulesScreenModel
import net.novagamestudios.kaffeekasse.ui.hiwi_tracker.HiwiTrackerModuleContent
import net.novagamestudios.kaffeekasse.ui.hiwi_tracker.HiwiTrackerModuleScreenModel
import net.novagamestudios.kaffeekasse.ui.hiwi_tracker.Overview
import net.novagamestudios.kaffeekasse.ui.hiwi_tracker.OverviewContent
import net.novagamestudios.kaffeekasse.ui.hiwi_tracker.OverviewScreenModel
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.Account
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.AccountContent
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.AccountScreenModel
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.DynamicManualBill
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.KaffeekasseModuleContent
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.KaffeekasseModuleScreenModel
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.ManualBillContent
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.ManualBillScreenModel
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.Transactions
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.TransactionsContent
import net.novagamestudios.kaffeekasse.ui.kaffeekasse.TransactionsScreenModel
import net.novagamestudios.kaffeekasse.ui.login.LoginAdditional
import net.novagamestudios.kaffeekasse.ui.login.LoginContent
import net.novagamestudios.kaffeekasse.ui.login.LoginForm
import net.novagamestudios.kaffeekasse.ui.login.LoginFormContent
import net.novagamestudios.kaffeekasse.ui.login.LoginFormScreenModel
import net.novagamestudios.kaffeekasse.ui.login.LoginScreenModel
import net.novagamestudios.kaffeekasse.ui.login.UserSelection
import net.novagamestudios.kaffeekasse.ui.login.UserSelectionContent
import net.novagamestudios.kaffeekasse.ui.login.UserSelectionScreenModel


sealed interface LoginNavigation {

    companion object : LoginNavigation, ScreenWithModel<LoginScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by LoginScreenModel
        private val initialScreen: Screen @Composable get() = model
            .session
            .deviceOrNull
            ?.let { UserSelectionScreen(it) }
            ?: FormScreen
        override val scaffoldContent = LoginContent(this)
        @Composable override fun Content() = AppScaffoldNavigator(
            key = "login-navigator",
            initialRoute = listOf(initialScreen),
            scaffoldContent = scaffoldContent,
            topAppBarScrollBehavior = null,
            content = {
                if (model.isLoading) {
                    BoxCenter(Modifier.fillMaxSize()) {
                        CircularProgressIndicator()
                    }
                } else {
                    AppScaffoldNavigatorDefaultContent("login-navigator-content")
                }
                LoginAdditional(model, this)
            }
        )
    }

    data object FormScreen : LoginNavigation, ScreenWithModel<LoginFormScreenModel>, ScaffoldContentProvider {
        private fun readResolve(): Any = FormScreen
        @get:Composable override val model by LoginFormScreenModel
        override val scaffoldContent = LoginFormContent(this)
        @Composable override fun Content() = LoginForm(model)
    }

    data class UserSelectionScreen(
        val device: Device
    ) : LoginNavigation, ScreenWithModel<UserSelectionScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by UserSelectionScreenModel
        override val scaffoldContent = UserSelectionContent(this)
        @Composable override fun Content() = UserSelection(model)
    }
}


sealed interface ModuleTab : Tab {
    val session: Session.WithRealUser
}


data class AppModulesScreen(
    val session: Session.WithRealUser
) : ScreenWithModel<AppModulesScreenModel> {
    @get:Composable override val model by AppModulesScreenModel
    @Composable override fun Content() {
        val model = model
        TabNavigator(
            remember { model.initialModuleTab() },
            disposeNestedNavigators = true,
            key = "app-modules-tabs",
        ) { tabNavigator ->
            AppTabTransition(
                tabNavigator = tabNavigator,
                Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .fillMaxSize(),
                key = "app-modules"
            )
            val currentTab = tabNavigator.current
            LaunchedEffect(currentTab) {
                if (currentTab is ModuleTab) model.onNavigateModuleTab(currentTab)
            }
        }
    }
}


sealed interface KaffeekasseNavigation {

    data class Tab(
        override val session: Session.WithRealUser
    ) : ModuleTab, KaffeekasseNavigation, ScreenModelProvider<KaffeekasseModuleScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by KaffeekasseModuleScreenModel
        override val scaffoldContent = KaffeekasseModuleContent(this)
        @Composable override fun Content() = AppScaffoldNavigator(
            key = "kaffeekasse-navigator",
            initialRoute = listOf(ManualBillScreen(session)),
            scaffoldContent = scaffoldContent
        )
        override val options @Composable get() = remember { TabOptions(1u, "Kaffeekasse") }
    }

    data class ManualBillScreen(
        val session: Session.WithRealUser
    ) : KaffeekasseNavigation, ScreenWithModel<ManualBillScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by ManualBillScreenModel
        override val scaffoldContent = ManualBillContent(this)
        @Composable override fun Content() = DynamicManualBill(model)
    }

    data class AccountScreen(
        val session: Session.WithRealUser
    ) : KaffeekasseNavigation, ScreenWithModel<AccountScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by AccountScreenModel
        override val scaffoldContent = AccountContent(this)
        @Composable override fun Content() = Account(model)
    }

    data class TransactionsScreen(
        val session: Session.WithRealUser
    ) : KaffeekasseNavigation, ScreenWithModel<TransactionsScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by TransactionsScreenModel
        override val scaffoldContent = TransactionsContent(this)
        @Composable override fun Content() = Transactions(model)
    }
}


sealed interface HiwiTrackerNavigation {

    data class Tab(
        override val session: Session.WithRealUser
    ) : ModuleTab, HiwiTrackerNavigation, ScreenModelProvider<HiwiTrackerModuleScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by HiwiTrackerModuleScreenModel
        override val scaffoldContent = HiwiTrackerModuleContent(this)
        @Composable override fun Content() = AppScaffoldNavigator(
            key = "hiwitracker-navigator",
            initialRoute = listOf(OverviewScreen(session)),
            scaffoldContent = scaffoldContent
        )
        override val options @Composable get() = remember { TabOptions(2u, "Hiwi Tracker") }
    }

    data class OverviewScreen(
        val session: Session.WithRealUser
    ) : HiwiTrackerNavigation, ScreenWithModel<OverviewScreenModel>, ScaffoldContentProvider {
        @get:Composable override val model by OverviewScreenModel
        override val scaffoldContent = OverviewContent(this)
        @Composable override fun Content() = Overview(model)
    }
}
