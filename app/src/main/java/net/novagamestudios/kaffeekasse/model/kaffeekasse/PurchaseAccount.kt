package net.novagamestudios.kaffeekasse.model.kaffeekasse

interface PurchaseAccount {
    val id: Int
    val name: Name

    interface WithDefault : PurchaseAccount {
        val isDefault: Boolean
    }
}
