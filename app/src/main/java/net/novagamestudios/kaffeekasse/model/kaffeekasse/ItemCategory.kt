package net.novagamestudios.kaffeekasse.model.kaffeekasse

enum class ItemCategory {
    ColdBeverage,
    HotBeverage,
    Snack,
    IceCream,
    Food,
    Fruit,
    Other
}