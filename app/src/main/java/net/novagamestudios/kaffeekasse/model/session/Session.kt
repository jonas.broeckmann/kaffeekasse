package net.novagamestudios.kaffeekasse.model.session


sealed interface Session : java.io.Serializable {
    val data: String?
    data object Empty : Session {
        override val data: String? get() = null
    }
    interface WithDevice : Session {
        val device: Device
    }
    interface WithRealUser : Session {
        val realUser: User
    }
    interface WithDeviceAndUser : Session, WithDevice, WithRealUser

    companion object {
        operator fun invoke(
            device: Device?,
            user: User?,
            data: String? = null
        ): Session {
            val realUser = user?.takeIf { it.isRealUser }
            return when {
                device != null && realUser != null -> SessionWithDeviceAndRealUser(device, realUser, data)
                device != null -> SessionWithDevice(device, data)
                realUser != null -> SessionWithRealUser(realUser, data)
                else -> Empty
            }
        }
    }
}


val Session.deviceOrNull: Device? get() = (this as? Session.WithDevice)?.device
val Session.realUserOrNull: User? get() = (this as? Session.WithRealUser)?.realUser


private val User.isKaffeekasse get() = user == "kaffeekasse"
private val User.isRealUser get() = !isKaffeekasse


private data class SessionWithDevice(
    override val device: Device,
    override val data: String?
) : Session.WithDevice

private data class SessionWithRealUser(
    override val realUser: User,
    override val data: String?
) : Session.WithRealUser

private data class SessionWithDeviceAndRealUser(
    override val device: Device,
    override val realUser: User,
    override val data: String?
) : Session.WithDeviceAndUser
