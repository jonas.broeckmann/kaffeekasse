package net.novagamestudios.kaffeekasse.model.kaffeekasse

import java.time.LocalDateTime


data class Transaction(
    val date: LocalDateTime,
    val payer: String?,
    val payee: String,
    val purpose: Purpose,
    val total: Double,
    val newBalance: Double,
    val refundId: String
) {
    sealed interface Purpose {
        data class Purchase(
            val itemName: String,
            val itemCount: String,
            val unitPrice: Double
        ) : Purpose {
            val knownId by lazy { KnownItem.idByOriginalName[itemName] }
            val knownItems by lazy { KnownItem.byId[knownId] ?: emptyList() }
        }
        data object Deposit : Purpose
        data class Other(
            val description: String
        ) : Purpose
    }
}





