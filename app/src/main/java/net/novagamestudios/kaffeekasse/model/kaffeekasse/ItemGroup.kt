package net.novagamestudios.kaffeekasse.model.kaffeekasse

interface ItemGroup {
    val id: Int?
    val originalName: String?

    val name: String
    val items: List<Item>
}