package net.novagamestudios.kaffeekasse.model.date_time

import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.date_time.serializers.ISODateSerializer

typealias ISODate = @Serializable(with = ISODateSerializer::class) LocalDate