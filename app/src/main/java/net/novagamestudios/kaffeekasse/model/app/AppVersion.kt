package net.novagamestudios.kaffeekasse.model.app

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.novagamestudios.common_utils.core.format
import net.novagamestudios.kaffeekasse.BuildConfig


/**
 * A version of the app in the format `{major}.{minor}.{patch}-{type}{iteration}`.
 */
@Serializable(with = AppVersionSerializer::class)
data class AppVersion(
    val major: Int,
    val minor: Int,
    val patch: Int,
    val type: Type,
    val iteration: Int,
) : Comparable<AppVersion> {

    override fun compareTo(other: AppVersion): Int = when {
        major > other.major -> 1
        major < other.major -> -1
        minor > other.minor -> 1
        minor < other.minor -> -1
        patch > other.patch -> 1
        patch < other.patch -> -1
        type != other.type -> type.compareTo(other.type)
        iteration > other.iteration -> 1
        iteration < other.iteration -> -1
        else -> 0
    }

    override fun toString(): String = listOfNotNull(
        "$major.$minor.$patch",
        if (type == Type.Stable) null else "$type${iteration format "%02d"}"
    ).joinToString("-")

    companion object {
        private val Regex by lazy { """(\d+)\.(\d+)\.(\d+)(-([A-Za-z]+)(\d+)?)?""".toRegex() }

        operator fun invoke(string: String): AppVersion? {
            val match = Regex.matchEntire(string) ?: return null
            val (major, minor, patch, _, type, iteration) = match.destructured
            return AppVersion(
                major = major.toInt(),
                minor = minor.toInt(),
                patch = patch.toInt(),
                type = if (type.isBlank()) Type.Stable else (Type[type] ?: return null),
                iteration = iteration.toIntOrNull() ?: 0
            )
        }

        fun findIn(string: String) = Regex.find(string)?.value?.let { AppVersion(it) }

        val Current = AppVersion(BuildConfig.VERSION_NAME) ?: throw AssertionError("Invalid version: ${BuildConfig.VERSION_NAME}")
    }

    enum class Type(val label: String) {
        Alpha("alpha"),
        Beta("beta"),
        Stable("stable");

        override fun toString() = label

        companion object {
            operator fun get(label: String) = entries.firstOrNull { it.label == label }
        }
    }
}

object AppVersionSerializer : KSerializer<AppVersion> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor(AppVersion::class.simpleName!!, PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: AppVersion) = encoder.encodeString(value.toString())
    override fun deserialize(decoder: Decoder): AppVersion {
        val string = decoder.decodeString()
        return AppVersion(string) ?: throw SerializationException("Invalid version: $string")
    }
}
