package net.novagamestudios.kaffeekasse.model.date_time.serializers

import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.novagamestudios.kaffeekasse.model.date_time.format
import net.novagamestudios.kaffeekasse.model.date_time.parse
import java.time.format.DateTimeFormatter

class ISODateTimeSerializer : KSerializer<LocalDateTime> {
    private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME!!
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("ISODateTime", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: LocalDateTime) {
        encoder.encodeString(value.format(formatter))
    }
    override fun deserialize(decoder: Decoder): LocalDateTime {
        return LocalDateTime.parse(decoder.decodeString(), formatter)
    }
}