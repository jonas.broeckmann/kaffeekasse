package net.novagamestudios.kaffeekasse.model.session

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val user: String? = null,
    val displayName: String? = null
) : java.io.Serializable

