package net.novagamestudios.kaffeekasse.model.kaffeekasse

interface Cart : Iterable<Cart.Entry> {
    val itemCount: Int
    override fun iterator(): Iterator<Entry>
    operator fun get(item: Item): Int
    operator fun contains(item: Item): Boolean
    data class Entry(val item: Item, val count: Int)
}

interface MutableCart : Cart {
    operator fun plusAssign(item: Item)
    operator fun minusAssign(item: Item)
    fun removeAll(item: Item)
    fun clear()
}

fun Cart.isEmpty() = itemCount <= 0
fun Cart.isNotEmpty() = !isEmpty()

