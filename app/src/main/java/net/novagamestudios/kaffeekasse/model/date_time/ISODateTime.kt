package net.novagamestudios.kaffeekasse.model.date_time

import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.date_time.serializers.ISODateTimeSerializer

typealias ISODateTime = @Serializable(with = ISODateTimeSerializer::class) LocalDateTime