package net.novagamestudios.kaffeekasse.model.credentials

import kotlinx.serialization.Serializable

@Serializable
data class Login(
    val username: String,
    val password: String
) {
    companion object {
        val Empty = Login("", "")
    }
}

