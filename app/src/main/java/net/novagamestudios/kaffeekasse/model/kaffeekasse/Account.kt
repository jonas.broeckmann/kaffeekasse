package net.novagamestudios.kaffeekasse.model.kaffeekasse


data class Account(
    val firstName: String,
    val lastName: String,
    val total: Double,
    val paid: Double,
    val deposited: Double
)


