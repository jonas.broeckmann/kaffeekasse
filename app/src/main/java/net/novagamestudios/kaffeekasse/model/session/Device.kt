package net.novagamestudios.kaffeekasse.model.session

import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItemGroup

@Serializable
data class Device(
    val name: String?,
    val itemTypeId: Int?
) : java.io.Serializable {
    val knownItemGroup by lazy { itemTypeId?.let { KnownItemGroup.byId[it] } }
}