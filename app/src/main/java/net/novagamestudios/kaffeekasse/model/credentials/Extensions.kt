package net.novagamestudios.kaffeekasse.model.credentials

val Login.isValid get() = username.isNotBlank() && password.isNotBlank()

val DeviceCredentials.isValid get() = deviceId.isNotBlank() && apiKey.isNotBlank()
