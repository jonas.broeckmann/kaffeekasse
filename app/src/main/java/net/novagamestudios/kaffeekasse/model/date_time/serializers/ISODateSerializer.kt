package net.novagamestudios.kaffeekasse.model.date_time.serializers

import kotlinx.datetime.LocalDate
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.novagamestudios.kaffeekasse.model.date_time.format
import net.novagamestudios.kaffeekasse.model.date_time.parse
import java.time.format.DateTimeFormatter

class ISODateSerializer : KSerializer<LocalDate> {
    private val formatter = DateTimeFormatter.ISO_DATE!!
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("ISODate", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: LocalDate) {
        encoder.encodeString(value.format(formatter))
    }
    override fun deserialize(decoder: Decoder): LocalDate {
        return LocalDate.parse(decoder.decodeString(), formatter)
    }
}