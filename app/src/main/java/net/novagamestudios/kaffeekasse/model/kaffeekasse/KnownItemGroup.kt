package net.novagamestudios.kaffeekasse.model.kaffeekasse

enum class KnownItemGroup(
    val id: Int,
    val originalName: String
) {
    Kueche       (5, "Küche"),
    Getraenkeraum(6, "Getränkeraum"),
    Farbdrucker  (7, "Farbdrucker"),
    ;
    companion object {
        val byId by lazy { entries.associateBy { it.id } }
        val byOriginalName by lazy { entries.associateBy { it.originalName } }
    }
}