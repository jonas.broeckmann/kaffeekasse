package net.novagamestudios.kaffeekasse.model.hiwi_tracker

import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalTime
import net.novagamestudios.common_utils.core.minus
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes


interface WorkEntry {
    val id: Int?
    val date: LocalDate
    val begin: LocalTime
    val end: LocalTime
    val breakDurationOrNull: Duration?
    val note: String
    val entryDuration: Duration get() = end - begin
    val breakDuration: Duration get() = breakDurationOrNull ?: Duration.ZERO
    val workedDuration: Duration get() = entryDuration - (breakDurationOrNull ?: Duration.ZERO)

    companion object {
        operator fun invoke(
            id: Int?,
            date: LocalDate,
            begin: LocalTime,
            end: LocalTime,
            breakTime: Duration?,
            note: String
        ): WorkEntry = WorkEntryImpl(
            id = id,
            date = date,
            begin = begin,
            end = end,
            breakDurationOrNull = breakTime,
            note = note
        )

        private data class WorkEntryImpl(
            override val id: Int?,
            override val date: LocalDate,
            override val begin: LocalTime,
            override val end: LocalTime,
            override val breakDurationOrNull: Duration?,
            override val note: String
        ) : WorkEntry

        val MaxWorkWithoutSmallBreak = 6.hours
        val MaxWorkWithoutLargeBreak = 9.hours
        val MinSmallBreakTime = 30.minutes
        val MinLargeBreakTime = 45.minutes

        val WorkEntry.invalidLargeBreak get() = entryDuration > MaxWorkWithoutLargeBreak && breakDuration < MinLargeBreakTime
        val WorkEntry.invalidSmallBreak get() = entryDuration > MaxWorkWithoutSmallBreak && breakDuration < MinSmallBreakTime
        val WorkEntry.isValid: Boolean get() {
            if (breakDuration.isNegative()) return false
            if (!workedDuration.isPositive()) return false
            if (invalidLargeBreak) return false
            if (invalidSmallBreak) return false
            return true
        }
    }
}




