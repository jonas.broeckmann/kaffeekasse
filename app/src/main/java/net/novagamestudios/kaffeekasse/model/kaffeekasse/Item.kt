package net.novagamestudios.kaffeekasse.model.kaffeekasse


interface Item {
    val id: Int
    val originalName: String

    val category: ItemCategory

    val cleanProductName: String
    val cleanVariantName: String?
    val cleanFullName get() = cleanProductName + cleanVariantName?.let { " ($it)" }.orEmpty()

    val price: Double?
    val estimatedPrice: Double?


    val imageDrawable: Int?
    val imageUrl: String?
}
