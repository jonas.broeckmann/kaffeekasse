package net.novagamestudios.kaffeekasse.model.kaffeekasse

import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.data.category
import net.novagamestudios.kaffeekasse.data.cleanFullName
import net.novagamestudios.kaffeekasse.data.cleanName
import net.novagamestudios.kaffeekasse.data.cleanProductName
import net.novagamestudios.kaffeekasse.data.cleanVariantName
import net.novagamestudios.kaffeekasse.data.drawableResource
import net.novagamestudios.kaffeekasse.data.estimatedPrice

@Serializable
data class ManualBillDetails(
    val accounts: List<PurchaseAccount>,
    val itemGroups: List<ItemGroup>
) {
    @Serializable
    data class PurchaseAccount(
        override val name: Name,
        override val id: Int,
        override val isDefault: Boolean = false
    ) : ScraperPurchaseAccount
    @Serializable
    data class ItemGroup(
        override val originalName: String,
        override val items: List<Item>,
        private val knownItemGroup: KnownItemGroup? = KnownItemGroup.byOriginalName[originalName]
    ) : net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemGroup {
        override val id: Int? get() = knownItemGroup?.id
        override val name: String get() = knownItemGroup?.cleanName ?: originalName
    }
    @Serializable
    data class Item(
        override val originalName: String,
        override val id: Int,
        private val knownItem: KnownItem? = null
    ) : net.novagamestudios.kaffeekasse.model.kaffeekasse.Item {
        override val category get() = knownItem?.category ?: ItemCategory.Other

        override val cleanFullName get() = knownItem?.cleanFullName ?: originalName
        override val cleanProductName get() = knownItem?.cleanProductName ?: originalName
        override val cleanVariantName get() = knownItem?.cleanVariantName

        override val price: Double? get() = null
        override val estimatedPrice: Double? get() = knownItem?.estimatedPrice

        override val imageDrawable: Int? get() = knownItem?.drawableResource
        override val imageUrl: String? get() = null
    }
}

sealed interface ScraperPurchaseAccount : PurchaseAccount, PurchaseAccount.WithDefault {
    override val isDefault: Boolean
}
