package net.novagamestudios.kaffeekasse.model.app

import kotlinx.datetime.LocalDateTime

data class AppRelease(
    val version: AppVersion,
    val downloadUrl: String,
    val createdAt: LocalDateTime,
    val description: String? = null
)