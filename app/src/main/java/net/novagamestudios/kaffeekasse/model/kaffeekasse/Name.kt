package net.novagamestudios.kaffeekasse.model.kaffeekasse

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Name.Companion.toNameOrNull


@Serializable(with = NameWithCommaSerializer::class)
data class Name(
    val last: String,
    val first: String?
) : Comparable<Name> {
    val firstLast by lazy { listOfNotNull(first, last).joinToString(" ") }
    val char by lazy { last.first().uppercaseChar() }

    private val string by lazy { listOfNotNull(last, first).joinToString(Separator) }
    private val compareString by lazy { string.uppercase() }

    override fun toString() = string

    override fun compareTo(other: Name) = compareString.compareTo(other.compareString)

    companion object {
        const val Separator = ", "

        fun String.toNameOrNull(): Name? = this
            .split(Separator)
            .takeIf { it.size <= 2 }
            ?.let { Name(it[0], it.getOrNull(1)) }
    }
}


object NameWithCommaSerializer : KSerializer<Name> {
    override val descriptor = String.serializer().descriptor
    override fun serialize(encoder: Encoder, value: Name) {
        encoder.encodeString(value.toString())
    }
    override fun deserialize(decoder: Decoder): Name {
        val raw = decoder.decodeString()
        return raw.toNameOrNull() ?: throw SerializationException("Invalid name: $raw")
    }
}
