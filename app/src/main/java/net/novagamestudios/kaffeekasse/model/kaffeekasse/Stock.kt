package net.novagamestudios.kaffeekasse.model.kaffeekasse

interface Stock {
    val itemGroups: List<ItemGroup>
}