package net.novagamestudios.kaffeekasse.model.date_time

import kotlinx.datetime.LocalTime
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.date_time.serializers.ISOTimeSerializer

typealias ISOTime = @Serializable(with = ISOTimeSerializer::class) LocalTime