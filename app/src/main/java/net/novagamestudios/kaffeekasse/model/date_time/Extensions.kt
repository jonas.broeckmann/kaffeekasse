package net.novagamestudios.kaffeekasse.model.date_time

import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.LocalTime
import kotlinx.datetime.toJavaLocalDate
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toJavaLocalTime
import kotlinx.datetime.toKotlinLocalDate
import kotlinx.datetime.toKotlinLocalDateTime
import kotlinx.datetime.toKotlinLocalTime
import java.time.format.DateTimeFormatter
import java.time.LocalDate as JavaLocalDate
import java.time.LocalDateTime as JavaLocalDateTime
import java.time.LocalTime as JavaLocalTime

fun LocalDateTime.format(formatter: DateTimeFormatter) = formatter.format(toJavaLocalDateTime())!!
fun LocalDateTime.Companion.parse(input: CharSequence, formatter: DateTimeFormatter) = JavaLocalDateTime.parse(
    input,
    formatter
).toKotlinLocalDateTime()
fun LocalDate.format(formatter: DateTimeFormatter) = formatter.format(toJavaLocalDate())!!
fun LocalDate.Companion.parse(input: CharSequence, formatter: DateTimeFormatter) = JavaLocalDate.parse(
    input,
    formatter
).toKotlinLocalDate()

fun LocalTime.format(formatter: DateTimeFormatter) = formatter.format(toJavaLocalTime())!!
fun LocalTime.Companion.parse(input: CharSequence, formatter: DateTimeFormatter) = JavaLocalTime.parse(
    input,
    formatter
).toKotlinLocalTime()