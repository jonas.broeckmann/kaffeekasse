package net.novagamestudios.kaffeekasse.model.credentials

import kotlinx.serialization.Serializable

@Serializable
data class DeviceCredentials(
    val deviceId: String,
    val apiKey: String
) {
    companion object {
        val Empty = DeviceCredentials("", "")
    }
}

