package net.novagamestudios.kaffeekasse.model.hiwi_tracker

import android.os.Parcel
import android.os.Parcelable
import kotlinx.datetime.LocalDate
import kotlinx.datetime.Month
import kotlinx.datetime.number
import kotlinx.serialization.Serializable
import net.novagamestudios.common_utils.core.logging.Logger


@Serializable
@JvmInline
value class MonthKey(val monthIndex: Int) : Comparable<MonthKey>, Parcelable, Logger {
    private val year get() = monthIndex / MonthsPerYear
    private val month get() = Month((monthIndex % MonthsPerYear) + 1)

    constructor(year: Int, month: Month) : this(year * MonthsPerYear + month.number - 1) {
        require(year >= 0) { "Year must be non-negative" }
    }
    constructor(date: LocalDate) : this(date.year, date.month)

    init {
        require(monthIndex >= 0) { "Month index must be non-negative" }
    }

    fun toLocalDate(): LocalDate = LocalDate(year, month, 1)

    override fun toString() = "$year-${month.number.toString().padStart(2, '0')}"
    override fun compareTo(other: MonthKey) = monthIndex.compareTo(other.monthIndex)

    operator fun plus(months: Int) = MonthKey(monthIndex + months)
    operator fun minus(months: Int) = MonthKey(monthIndex - months)

    operator fun minus(other: MonthKey) = (monthIndex - other.monthIndex)

    companion object {
        private const val MonthsPerYear = 12
        fun LocalDate.toMonthKey() = MonthKey(this)

        @JvmField
        val CREATOR = object : Parcelable.Creator<MonthKey> {
            override fun createFromParcel(parcel: Parcel): MonthKey {
                return MonthKey(parcel.readInt())
            }
            override fun newArray(size: Int): Array<MonthKey?> {
                return arrayOfNulls(size)
            }
        }
    }
    override fun describeContents(): Int {
        return 0
    }
    override fun writeToParcel(out: Parcel, flags: Int) {
        out.writeInt(monthIndex)
    }
}



