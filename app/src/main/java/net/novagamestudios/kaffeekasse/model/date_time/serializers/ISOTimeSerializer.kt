package net.novagamestudios.kaffeekasse.model.date_time.serializers

import kotlinx.datetime.LocalTime
import kotlinx.datetime.toKotlinLocalTime
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.novagamestudios.kaffeekasse.model.date_time.format
import net.novagamestudios.kaffeekasse.model.date_time.parse
import java.time.format.DateTimeFormatter
import java.time.LocalTime as JavaLocalTime

class ISOTimeSerializer : KSerializer<LocalTime> {
    private val formatter = DateTimeFormatter.ISO_TIME!!
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("ISOTime", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: LocalTime) {
        encoder.encodeString(value.format(formatter))
    }
    override fun deserialize(decoder: Decoder): LocalTime {
        val string = decoder.decodeString()
        if (string.startsWith("24:")) return JavaLocalTime.MAX.toKotlinLocalTime()
        return LocalTime.parse(string, formatter)
    }
}