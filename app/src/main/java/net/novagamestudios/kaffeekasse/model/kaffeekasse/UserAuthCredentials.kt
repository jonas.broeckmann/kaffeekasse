package net.novagamestudios.kaffeekasse.model.kaffeekasse

data class UserAuthCredentials(
    val pin: String? = null,
    val rwthId: String? = null,
    val key: String? = null
) {
    companion object {
        val Empty = UserAuthCredentials()
    }
}
