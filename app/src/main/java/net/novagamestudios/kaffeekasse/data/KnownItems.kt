package net.novagamestudios.kaffeekasse.data

import net.novagamestudios.kaffeekasse.R
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ItemCategory
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Almdudler
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Apple
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.BaguetteBistro2
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Balisto
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Bier330
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Bier500
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Bionade
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CafeAuLait
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Cappuccino
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ChioTortillaChips
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ChocolateCreamCocoa
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ChouffeBier
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Clementine
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ClubMate
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CocaCola
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CocaCola05Glass
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CocaCola330
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CoffeeLarge
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CoffeeSmall
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CokeZero
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CokeZeroCaffeineFree
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.CujaMaraSplit
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.DuplexBlackWhite
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.DuplexColor
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Duplo
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.EiflerLandbier
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.EngelbertApple
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.EngelbertNatural
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.EngelbertSprudel
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ErdingerAlcoholFree330
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ErdingerAlcoholFree500
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Erdnuesse
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Espresso
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Euglueh
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Fanta
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Fassbrause
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.FritzCola
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Gerolsteiner
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Hanuta
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.HariboBag
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceBounty
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceCornetto
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceFlutschfinger
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceMars
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceNogger
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceSnickers
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.IceTwix
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Katjes
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.KinderChocolate
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Knoppers
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.KnoppersBar
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.LatteMacchiato
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Leffe
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.LindtHase
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Magnum
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.MilkMachine
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Milka
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.MioMioMate
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Moccachoc
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Monster
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.NUII
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.NicNacs
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.NutsRoyalNuts
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.OreoCookies
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.PickUp
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Pizza
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.RheinfelsSprudel
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Rockstar
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.SimplexBlackWhite
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.SimplexColor
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.TUC
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Tangerine
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.ThreeDPrintingPerGram
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Tissues
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Trolli
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Twix
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem.Yogurette
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItemGroup


val KnownItemGroup.cleanName get() = when (this) {
    KnownItemGroup.Kueche -> "Küche"
    KnownItemGroup.Getraenkeraum -> "Getränkeraum"
    KnownItemGroup.Farbdrucker -> "Farbdrucker"
}

val ItemCategory.cleanName get() = when (this) {
    ItemCategory.ColdBeverage -> "Kaltgetränke"
    ItemCategory.HotBeverage -> "Heißgetränke"
    ItemCategory.Snack -> "Snacks"
    ItemCategory.IceCream -> "Eis"
    ItemCategory.Food -> "Essen"
    ItemCategory.Fruit -> "Obst"
    ItemCategory.Other -> "Sonstiges"
}

val KnownItem.category get() = when (this) {
    Almdudler -> ItemCategory.ColdBeverage
    Bier330 -> ItemCategory.ColdBeverage
    Bier500 -> ItemCategory.ColdBeverage
    ClubMate -> ItemCategory.ColdBeverage
    CocaCola -> ItemCategory.ColdBeverage
    CocaCola330 -> ItemCategory.ColdBeverage
    CokeZero -> ItemCategory.ColdBeverage
    CokeZeroCaffeineFree -> ItemCategory.ColdBeverage
    FritzCola -> ItemCategory.ColdBeverage
    ChouffeBier -> ItemCategory.ColdBeverage
    EiflerLandbier -> ItemCategory.ColdBeverage
    EngelbertApple -> ItemCategory.ColdBeverage
    EngelbertNatural -> ItemCategory.ColdBeverage
    EngelbertSprudel -> ItemCategory.ColdBeverage
    RheinfelsSprudel -> ItemCategory.ColdBeverage
    ErdingerAlcoholFree500 -> ItemCategory.ColdBeverage
    Fanta -> ItemCategory.ColdBeverage
    Fassbrause -> ItemCategory.ColdBeverage
    Bionade -> ItemCategory.ColdBeverage
    Gerolsteiner -> ItemCategory.ColdBeverage
    ErdingerAlcoholFree330 -> ItemCategory.ColdBeverage
    Leffe -> ItemCategory.ColdBeverage
    Rockstar -> ItemCategory.ColdBeverage
    Monster -> ItemCategory.ColdBeverage
    CocaCola05Glass -> ItemCategory.ColdBeverage
    MioMioMate -> ItemCategory.ColdBeverage
    MilkMachine -> ItemCategory.HotBeverage
    CafeAuLait -> ItemCategory.HotBeverage
    Cappuccino -> ItemCategory.HotBeverage
    Espresso -> ItemCategory.HotBeverage
    CoffeeSmall -> ItemCategory.HotBeverage
    CoffeeLarge -> ItemCategory.HotBeverage
    LatteMacchiato -> ItemCategory.HotBeverage
    Moccachoc -> ItemCategory.HotBeverage
    ChocolateCreamCocoa -> ItemCategory.HotBeverage
    Erdnuesse -> ItemCategory.Snack
    Hanuta -> ItemCategory.Snack
    HariboBag -> ItemCategory.Snack
    Knoppers -> ItemCategory.Snack
    KnoppersBar -> ItemCategory.Snack
    Milka -> ItemCategory.Snack
    Balisto -> ItemCategory.Snack
    Duplo -> ItemCategory.Snack
    PickUp -> ItemCategory.Snack
    LindtHase -> ItemCategory.Snack
    OreoCookies -> ItemCategory.Snack
    Trolli -> ItemCategory.Snack
    Twix -> ItemCategory.Snack
    Yogurette -> ItemCategory.Snack
    KinderChocolate -> ItemCategory.Snack
    NutsRoyalNuts -> ItemCategory.Snack
    Katjes -> ItemCategory.Snack
    TUC -> ItemCategory.Snack
    NicNacs -> ItemCategory.Snack
    ChioTortillaChips -> ItemCategory.Snack
    CujaMaraSplit -> ItemCategory.IceCream
    Magnum -> ItemCategory.IceCream
    NUII -> ItemCategory.IceCream
    IceCornetto -> ItemCategory.IceCream
    IceNogger -> ItemCategory.IceCream
    IceMars -> ItemCategory.IceCream
    IceSnickers -> ItemCategory.IceCream
    IceTwix -> ItemCategory.IceCream
    IceBounty -> ItemCategory.IceCream
    IceFlutschfinger -> ItemCategory.IceCream
    Pizza -> ItemCategory.Food
    BaguetteBistro2 -> ItemCategory.Food
    Apple -> ItemCategory.Fruit
    Tangerine -> ItemCategory.Fruit
    Clementine -> ItemCategory.Fruit
    DuplexColor -> ItemCategory.Other
    DuplexBlackWhite -> ItemCategory.Other
    SimplexColor -> ItemCategory.Other
    SimplexBlackWhite -> ItemCategory.Other
    Tissues -> ItemCategory.Other
    ThreeDPrintingPerGram -> ItemCategory.Other
    Euglueh -> ItemCategory.HotBeverage
}
val KnownItem.cleanFullName get() = cleanProductName + cleanVariantName?.let { " ($it)" }.orEmpty()
val KnownItem.cleanProductName get() = when (this) {
    Almdudler -> "Almdudler"
    Bier330 -> "Sonstiges Bier"
    Bier500 -> "Sonstiges Bier"
    ErdingerAlcoholFree330 -> "Erdinger 0‰"
    ErdingerAlcoholFree500 -> "Erdinger 0‰"
    EiflerLandbier -> "Eifler Landbier"
    ChouffeBier -> "Chouffe Bier"
    Leffe -> "Leffe"
    ClubMate -> "Club-Mate"
    CocaCola -> "Coca-Cola"
    CocaCola330 -> "Coca-Cola"
    CocaCola05Glass -> "Coca-Cola"
    CokeZero -> "Coca-Cola Zero"
    CokeZeroCaffeineFree -> "Coca-Cola Zero"
    FritzCola -> "Fritz Cola"
    EngelbertApple -> "Engelbert Apfel"
    EngelbertNatural -> "Engelbert Naturell"
    EngelbertSprudel -> "Engelbert Sprudel"
    RheinfelsSprudel -> "Rheinfels Sprudel"
    Fanta -> "Fanta"
    Fassbrause -> "Fassbrause"
    Bionade -> "Bionade"
    Gerolsteiner -> "Gerolsteiner"
    Rockstar -> "Rockstar"
    Monster -> "Monster"
    MilkMachine -> "Milch"
    MioMioMate -> "Mio Mio Mate"
    CafeAuLait -> "Café au Lait"
    Cappuccino -> "Cappuccino"
    Espresso -> "Espresso"
    CoffeeSmall -> "Kaffee"
    CoffeeLarge -> "Kaffee"
    LatteMacchiato -> "Latte Macchiato"
    Moccachoc -> "Moccachino"
    ChocolateCreamCocoa -> "Kakao"
    Erdnuesse -> "Erdnüsse"
    Hanuta -> "Hanuta"
    HariboBag -> "Haribo"
    Knoppers -> "Knoppers"
    KnoppersBar -> "Knoppers Riegel"
    Milka -> "Milka"
    Balisto -> "Balisto"
    Duplo -> "Duplo"
    PickUp -> "PickUp"
    LindtHase -> "Lindt"
    OreoCookies -> "Oreo"
    Trolli -> "Trolli"
    Twix -> "Twix"
    Yogurette -> "Yogurette"
    KinderChocolate -> "Kinder Schokolade"
    NutsRoyalNuts -> "Nuts Royal Nüsse"
    CujaMaraSplit -> "Cuja Mara Split"
    Magnum -> "Magnum"
    NUII -> "NUII"
    IceCornetto -> "Cornetto"
    IceNogger -> "Nogger"
    IceMars -> "Mars Ice Cream"
    IceSnickers -> "Snickers Ice Cream"
    IceTwix -> "Twix Ice Cream"
    IceBounty -> "Bounty Ice Cream"
    IceFlutschfinger -> "Flutschfinger"
    Pizza -> "Pizza"
    BaguetteBistro2 -> "Bistro Baguette"
    Apple -> "Apfel"
    Tangerine -> "Mandarine"
    Clementine -> "Clementine"
    DuplexColor -> "Duplex"
    DuplexBlackWhite -> "Duplex"
    SimplexColor -> "Simplex"
    SimplexBlackWhite -> "Simplex"
    Tissues -> "Taschentücher"
    ThreeDPrintingPerGram -> "3D-Druck"
    Euglueh -> "Euglüh"
    Katjes -> "Katjes"
    TUC -> "TUC"
    NicNacs -> "NicNac's"
    ChioTortillaChips -> "Chio Tortillas"
}
val KnownItem.cleanVariantName get() = when (this) {
    Bier330 -> "0,33L"
    Bier500 -> "0,5L"
    ErdingerAlcoholFree330 -> "0,33L"
    ErdingerAlcoholFree500 -> "0,5L"
    Fanta -> "Flasche"
    CokeZero -> "Flasche"
    CokeZeroCaffeineFree -> "koffeinfrei"
    CocaCola -> "1L"
    CocaCola330 -> "0,33L"
    CocaCola05Glass -> "0,5L Glas"
    Rockstar -> "inkl. Pfand"
    Monster -> "inkl. Pfand"
    MilkMachine -> "120ml Maschine"
    CoffeeSmall -> "klein"
    CoffeeLarge -> "groß"
    HariboBag -> "Beutel"
    Katjes -> "Beutel"
    LindtHase -> "Hase"
    BaguetteBistro2 -> "2 Stück"
    DuplexColor -> "Farbe"
    DuplexBlackWhite -> "S/W"
    SimplexColor -> "Farbe"
    SimplexBlackWhite -> "S/W"
    ThreeDPrintingPerGram -> "pro g"
    else -> null
}
val KnownItem.estimatedPrice get() = when (this) {
    Almdudler -> null
    Bier330 -> 1.06
    Bier500 -> 1.25
    Bionade -> 0.82
    ChouffeBier -> 1.80
    ClubMate -> null
    CocaCola -> 1.73
    CocaCola330 -> 1.22
    CocaCola05Glass -> 1.35
    CokeZero -> 1.66
    CokeZeroCaffeineFree -> null
    FritzCola -> 1.57
    CafeAuLait -> 0.33
    Cappuccino -> 0.42
    CoffeeSmall -> 0.19
    CoffeeLarge -> 0.32
    ChocolateCreamCocoa -> 0.41
    ErdingerAlcoholFree330 -> 1.15
    ErdingerAlcoholFree500 -> 1.45
    EiflerLandbier -> 1.15
    EngelbertApple -> 0.98
    EngelbertNatural -> 0.57
    EngelbertSprudel -> 0.57
    Espresso -> 0.19
    Erdnuesse -> 1.30
    Fanta -> 1.55
    Fassbrause -> 0.82
    Gerolsteiner -> 0.93
    Leffe -> 2.00
    LatteMacchiato -> 0.55
    Monster -> 1.69
    MilkMachine -> 0.25
    MioMioMate -> 1.03
    Moccachoc -> 0.55
    Rockstar -> 1.69
    RheinfelsSprudel -> 0.57
    Hanuta -> 0.28
    HariboBag -> 1.15
    Knoppers -> 0.32
    KnoppersBar -> 0.45
    Milka -> 0.99
    Balisto -> 0.25
    Duplo -> 0.26
    PickUp -> 0.34
    LindtHase -> 1.00
    OreoCookies -> 0.40
    Trolli -> 0.99
    Twix -> null
    Yogurette -> 0.19
    KinderChocolate -> null
    NutsRoyalNuts -> 2.25
    CujaMaraSplit -> 0.65
    Magnum -> 0.99
    NUII -> 0.99
    IceCornetto -> 0.50
    IceNogger -> 0.65
    IceMars -> 0.68
    IceSnickers -> 0.68
    IceTwix -> 0.68
    IceBounty -> 0.68
    IceFlutschfinger -> 0.49
    Pizza -> 2.99
    BaguetteBistro2 -> 2.00
    Apple -> 0.30
    Tangerine -> null
    Clementine -> null
    DuplexColor -> 0.05
    DuplexBlackWhite -> 0.03
    SimplexColor -> 0.03
    SimplexBlackWhite -> 0.02
    Tissues -> 0.12
    ThreeDPrintingPerGram -> 0.02
    Euglueh -> null
    Katjes -> 0.99
    TUC -> 1.25
    NicNacs -> 1.40
    ChioTortillaChips -> 1.35
}

val KnownItem.drawableResource get() = when (this) {

    // ColdBeverage
    Almdudler -> R.drawable.almdudler_nostalgia_enamel_sign_1_pc_4778_en
    Bionade -> R.drawable.bionade_logo
    ChouffeBier -> R.drawable.la_chouffe_logo
    ClubMate -> R.drawable.club_mate
    CocaCola -> R.drawable.coca_cola_square
    CocaCola330 -> R.drawable.coca_cola_square
    CocaCola05Glass -> R.drawable.coca_cola_square
    CokeZero -> R.drawable.coca_cola_zero_logo_300dpi
    CokeZeroCaffeineFree -> R.drawable.coca_cola_zero_logo_300dpi
    EiflerLandbier -> R.drawable.eifler_landbier
    EngelbertApple -> R.drawable.engelbert_sprudel
    EngelbertNatural -> R.drawable.engelbert_sprudel
    EngelbertSprudel -> R.drawable.engelbert_sprudel
    ErdingerAlcoholFree330 -> R.drawable.logo_erdinger_alkoholfrei_negativ
    ErdingerAlcoholFree500 -> R.drawable.logo_erdinger_alkoholfrei_negativ
    Fanta -> R.drawable.fanta
    Fassbrause -> R.drawable.fassbrause2
    Gerolsteiner -> R.drawable.gerolsteiner_mineralwasser4730
    Leffe -> R.drawable.leffe_brauerei_logo
    Monster -> R.drawable.monster_energy
    RheinfelsSprudel -> R.drawable.rheinfels
    Rockstar -> R.drawable.rockstar_energy_drink_logo_svg
    Bier330 -> R.drawable.bier_klein
    Bier500 -> R.drawable.bier_gross
    MioMioMate -> R.drawable.miomio

    // HotBeverage
    CafeAuLait -> R.drawable.cafe_au_late
    Cappuccino -> R.drawable.cappuccino
    Espresso -> R.drawable.espresso
    CoffeeLarge -> R.drawable.coffee_large
    CoffeeSmall -> R.drawable.coffee_small
    ChocolateCreamCocoa -> R.drawable.hot_chocolate
    LatteMacchiato -> R.drawable.latte_macchiato
    Moccachoc -> R.drawable.moccachoc

    // Snack
    Balisto -> R.drawable.logo_balisto
    Duplo -> R.drawable.logo_duplo_svg
    Erdnuesse -> R.drawable.erdnuesse_removebg_preview
    Hanuta -> R.drawable.hanuta_29
    HariboBag -> R.drawable.haribo_logo_svg
    KinderChocolate -> R.drawable.kinder_schokolade_svg
    Knoppers -> null
    KnoppersBar -> R.drawable.knopper_riegel_broken // TODO
    Milka -> R.drawable._048px_milka_logo_svg
    OreoCookies -> R.drawable.oreo_logo
    Trolli -> R.drawable.trolli_logo
    Twix -> R.drawable._560px_twix_logo_svg
    Yogurette -> R.drawable.yogurette_logo
    Katjes -> null

    // IceCream
    IceCornetto -> null
    CujaMaraSplit -> null
    Magnum -> null
    NUII -> R.drawable.nuii
    IceNogger -> null
    IceMars -> null
    IceSnickers -> null
    IceTwix -> null
    IceBounty -> null

    // Food
    Pizza -> R.drawable.pizza3

    // Fruit

    // Other

    else -> null
}

