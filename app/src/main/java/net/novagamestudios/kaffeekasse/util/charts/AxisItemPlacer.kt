package net.novagamestudios.kaffeekasse.util.charts

import com.patrykandpatrick.vico.core.axis.AxisItemPlacer
import com.patrykandpatrick.vico.core.axis.AxisPosition
import com.patrykandpatrick.vico.core.axis.vertical.VerticalAxis
import com.patrykandpatrick.vico.core.chart.draw.ChartDrawContext
import com.patrykandpatrick.vico.core.context.MeasureContext
import kotlin.math.ceil
import kotlin.math.floor


class IntegerAxisItemPlacer : AxisItemPlacer.Vertical {
    override fun getBottomVerticalAxisInset(
        context: MeasureContext,
        verticalLabelPosition: VerticalAxis.VerticalLabelPosition,
        maxLabelHeight: Float,
        maxLineThickness: Float
    ): Float {
        return 0f
    }
    override fun getTopVerticalAxisInset(
        context: MeasureContext,
        verticalLabelPosition: VerticalAxis.VerticalLabelPosition,
        maxLabelHeight: Float,
        maxLineThickness: Float
    ): Float {
        return maxLabelHeight / 2
    }

    override fun getLabelValues(
        context: ChartDrawContext,
        axisHeight: Float,
        maxLabelHeight: Float,
        position: AxisPosition.Vertical
    ): List<Float> {
        return getWidthMeasurementLabelValues(context, axisHeight, maxLabelHeight, position)
    }

    override fun getWidthMeasurementLabelValues(
        context: MeasureContext,
        axisHeight: Float,
        maxLabelHeight: Float,
        position: AxisPosition.Vertical
    ): List<Float> {
        val yRange = context.chartValues.getYRange(position)
        val min = floor(yRange.minY).toInt()
        val max = ceil(yRange.maxY).toInt()
        val labelValueHeight = yRange.length / axisHeight * maxLabelHeight
        return (min..max step ceil(labelValueHeight).toInt()).map { it.toFloat() }
    }
    override fun getHeightMeasurementLabelValues(
        context: MeasureContext,
        position: AxisPosition.Vertical
    ): List<Float> {
        val yRange = context.chartValues.getYRange(position)
        return listOf(yRange.minY, (yRange.minY + yRange.maxY) / 2, yRange.maxY)
    }
}

