package net.novagamestudios.kaffeekasse.util

import kotlinx.serialization.json.JsonElement

open class WrappedJsonSerializationException(
    message: String?,
    cause: Throwable?,
    val json: JsonElement
) : IllegalArgumentException(message, cause) {
    constructor(e: Exception, json: JsonElement) : this(e.message, e, json)
}