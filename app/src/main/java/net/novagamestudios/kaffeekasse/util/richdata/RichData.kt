package net.novagamestudios.kaffeekasse.util.richdata

import androidx.compose.runtime.Immutable
import net.novagamestudios.common_utils.compose.components.Progress

@Immutable
sealed interface RichData<out T : Any> {
    @Immutable class None<out T : Any> : RichData<T>
    @Immutable data class Loading<out T : Any>(
        val progress: Progress
    ) : RichData<T>
    @Immutable data class Data<out T : Any>(
        val data: T,
        val isDirty: Boolean = false
    ) : RichData<T>
    @Immutable interface Error<out T : Any> : RichData<T> {
        val messages: List<String>
    }
    companion object {
        fun <T : Any> of(value: T?, isDirty: Boolean = false): RichData<T> = if (value == null) None() else Data(value, isDirty)
    }
}

data class RichDataExceptionError<T : Any>(
    val exception: Exception
) : RichData.Error<T> {
    override val messages: List<String> by lazy {
        val list = mutableListOf<String>()
        var current: Throwable? = exception
        while (current != null) {
            list.add(current.message ?: current::class.simpleName ?: "Unknown error")
            current = current.cause
        }
        list
    }
}

data class RichDataCombinedError<T : Any>(
    val errors: List<RichData.Error<T>>
) : RichData.Error<T> {
    override val messages: List<String> by lazy { errors.flatMap { it.messages } }
}

val RichData.Error<*>.exceptions: List<Exception> get() = when (this) {
    is RichDataExceptionError -> listOf(exception)
    is RichDataCombinedError -> errors.flatMap { it.exceptions }
    else -> emptyList()
}



val <T : Any> RichData<T>.isLoading: Boolean get() = this is RichData.Loading || this is RichData.None // Hopefully this will not lead to issues in the future ¯\_(ツ)_/¯
val <T : Any> RichData<T>.isNone: Boolean get() = this is RichData.None
val <T : Any> RichData<T>.dataOrNull: T? get() = (this as? RichData.Data)?.data
val <T : Any> RichData<T>.errorOrNull: RichData.Error<T>? get() = this as? RichData.Error

