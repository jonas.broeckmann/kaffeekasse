package net.novagamestudios.kaffeekasse.util.richdata

import kotlinx.coroutines.CoroutineScope

class KeyedMultiDataSource<K, T : Any>(
    private val coroutineScope: CoroutineScope,
    private val producer: suspend RichDataCollector<T>.(K) -> RichData<T>,
) {
    private val dataByKey = mutableMapOf<K, RichDataSource<T>>()

    operator fun get(key: K): RichDataSource<T> = dataByKey.getOrPut(key) {
        with(coroutineScope) {
            RichDataSource.RichDataSource {
                producer(key)
            }
        }
    }

    fun getOrNull(key: K): RichDataSource<T>? = dataByKey[key]

    fun clear() {
        dataByKey.clear()
    }

    fun markDirty() {
        dataByKey.values.forEach { it.markDirty() }
    }

    companion object {
        context (CoroutineScope)
        operator fun <K, T : Any> invoke(
            producer: suspend RichDataCollector<T>.(K) -> RichData<T>
        ) = KeyedMultiDataSource(this@CoroutineScope, producer)
    }
}