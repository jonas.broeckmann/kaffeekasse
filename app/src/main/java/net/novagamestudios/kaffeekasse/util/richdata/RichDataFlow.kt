package net.novagamestudios.kaffeekasse.util.richdata

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combineTransform
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.flow.transformLatest
import net.novagamestudios.common_utils.core.collection.mapState
import net.novagamestudios.common_utils.compose.components.Progress


typealias RichDataFlow<T> = Flow<RichData<T>>
typealias RichDataStateFlow<T> = StateFlow<RichData<T>>
typealias MutableRichDataStateFlow<T> = MutableStateFlow<RichData<T>>


fun <T : Any> StateFlow<T?>.asRichDataStateFlow(): RichDataStateFlow<T> = mapState { RichData.of(it) }
fun <T : Any> Flow<T?>.asRichDataFlow(): RichDataFlow<T> = map { RichData.of(it) }

fun <T : Any> RichDataFlow<T>.stateIn(
    coroutineScope: CoroutineScope,
    started: SharingStarted = SharingStarted.Lazily,
): RichDataStateFlow<T> {
    return stateIn(coroutineScope, started, RichData.None())
}

fun <T : Any> Flow<T>.richStateIn(
    coroutineScope: CoroutineScope,
    started: SharingStarted = SharingStarted.Lazily,
): RichDataStateFlow<T> {
    return asRichDataFlow().stateIn(coroutineScope, started, RichData.None())
}

@Suppress("UNCHECKED_CAST")
fun <T : Any, R : Any> RichDataFlow<T>.mapRich(
    loadDuringTransform: Boolean = false,
    transform: suspend (T) -> R?
): RichDataFlow<R> = transform {
    if (loadDuringTransform) emit(RichData.Loading(Progress.Indeterminate))
    emit(when (it) {
        is RichData.None -> RichData.None()
        is RichData.Loading -> RichData.Loading(it.progress)
        is RichData.Data -> RichData.of(transform(it.data), it.isDirty)
        is RichData.Error -> it as RichData.Error<R>
    })
}

@OptIn(ExperimentalCoroutinesApi::class)
@Suppress("UNCHECKED_CAST")
fun <T : Any, R : Any> RichDataFlow<T>.mapLatestRich(
    loadDuringTransform: Boolean = false,
    transform: suspend (T) -> R?
): RichDataFlow<R> = transformLatest {
    if (loadDuringTransform) emit(RichData.Loading(Progress.Indeterminate))
    emit(when (it) {
        is RichData.None -> RichData.None()
        is RichData.Loading -> RichData.Loading(it.progress)
        is RichData.Data -> RichData.of(transform(it.data), it.isDirty)
        is RichData.Error -> it as RichData.Error<R>
    })
}

@Suppress("UNCHECKED_CAST")
fun <T : Any, R : Any> RichDataFlow<T>.flatMapLatestRich(transform: suspend (T) -> RichDataFlow<R>?): RichDataFlow<R> = channelFlow {
    collectLatest { latest ->
        when (latest) {
            is RichData.None -> send(RichData.None())
            is RichData.Loading -> send(RichData.Loading(latest.progress))
            is RichData.Data -> {
                val flow = transform(latest.data)
                if (flow != null) flow.collect { send(it) } else send(RichData.None())
            }
            is RichData.Error -> send(latest as RichData.Error<R>)
        }
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <T : Any, R : Any> combineRich(
    vararg flows: RichDataFlow<T>,
    loadDuringTransform: Boolean = false,
    crossinline transform: suspend (List<T>) -> R
): RichDataFlow<R> {
    return combineTransform<RichData<T>, RichData<R>>(*flows) { array ->
        if (loadDuringTransform) emit(RichData.Loading(Progress.Indeterminate))
        emit(if (array.all { it is RichData.Data }) {
            val data = array.map { (it as RichData.Data).data }
            RichData.Data(transform(data), array.any { (it as RichData.Data).isDirty })
        } else if (array.any { it is RichData.Error }) {
            RichDataCombinedError(array.filterIsInstance<RichData.Error<T>>().map { it as RichData.Error<R> })
        } else if (array.any { it is RichData.Loading }) {
            RichData.Loading(Progress.Indeterminate)
        } else {
            RichData.None()
        })
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <T1 : Any, T2 : Any, R : Any> combineRich(
    flow1: RichDataFlow<T1>,
    flow2: RichDataFlow<T2>,
    loadDuringTransform: Boolean = false,
    crossinline transform: suspend (T1, T2) -> R?
): RichDataFlow<R> {
    return combineTransform(flow1, flow2) { data1, data2 ->
        if (loadDuringTransform) emit(RichData.Loading(Progress.Indeterminate))
        emit(if (data1 is RichData.Data && data2 is RichData.Data) {
            RichData.of(transform(data1.data, data2.data), data1.isDirty || data2.isDirty)
        } else if (data1 is RichData.Error || data2 is RichData.Error) {
            RichDataCombinedError(listOfNotNull(data1 as? RichData.Error<R>, data2 as? RichData.Error<R>))
        } else if (data1 is RichData.Loading || data2 is RichData.Loading) {
            RichData.Loading(Progress.Indeterminate)
        } else {
            RichData.None()
        })
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <T1 : Any, T2 : Any, T3 : Any, R : Any> combineRich(
    flow1: RichDataFlow<T1>,
    flow2: RichDataFlow<T2>,
    flow3: RichDataFlow<T3>,
    loadDuringTransform: Boolean = false,
    crossinline transform: suspend (T1, T2, T3) -> R?
): RichDataFlow<R> {
    return combineTransform(flow1, flow2, flow3) { data1, data2, data3 ->
        if (loadDuringTransform) emit(RichData.Loading(Progress.Indeterminate))
        emit(if (data1 is RichData.Data && data2 is RichData.Data && data3 is RichData.Data) {
            RichData.of(transform(data1.data, data2.data, data3.data), data1.isDirty || data2.isDirty || data3.isDirty)
        } else if (data1 is RichData.Error || data2 is RichData.Error || data3 is RichData.Error) {
            RichDataCombinedError(listOfNotNull(data1 as? RichData.Error<R>, data2 as? RichData.Error<R>, data3 as? RichData.Error<R>))
        } else if (data1 is RichData.Loading || data2 is RichData.Loading || data3 is RichData.Loading) {
            RichData.Loading(Progress.Indeterminate)
        } else {
            RichData.None()
        })
    }
}



