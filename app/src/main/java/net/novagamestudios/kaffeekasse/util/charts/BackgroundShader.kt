package net.novagamestudios.kaffeekasse.util.charts

import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Shader
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import com.patrykandpatrick.vico.compose.component.shape.shader.BrushShader
import com.patrykandpatrick.vico.compose.component.shape.shader.fromBrush
import com.patrykandpatrick.vico.core.DefaultAlpha
import com.patrykandpatrick.vico.core.chart.draw.ChartDrawContext
import com.patrykandpatrick.vico.core.component.shape.shader.CacheableDynamicShader
import com.patrykandpatrick.vico.core.component.shape.shader.DynamicShader
import com.patrykandpatrick.vico.core.component.shape.shader.DynamicShaders
import com.patrykandpatrick.vico.core.context.DrawContext

class BackgroundShader(
    private val topShader: DynamicShader,
    private val bottomShader: BrushShader
) : CacheableDynamicShader() {
    private val paint = Paint()
    override fun createShader(
        context: DrawContext,
        left: Float,
        top: Float,
        right: Float,
        bottom: Float
    ): Shader {
        val width = (right - left).toInt().coerceAtLeast(1)
        val height = (bottom - top).toInt().coerceAtLeast(1)
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val chartContext = context as? ChartDrawContext
        val isTop = top == chartContext?.chartBounds?.top
        paint.shader = (if (isTop) topShader else bottomShader).provideShader(
            context = context,
            left = left,
            top = 0f,
            right = right,
            bottom = height.toFloat(),
        )
        Canvas(bitmap).drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        return BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP).apply {
            setLocalMatrix(Matrix().apply { postTranslate(left, top) })
        }
    }

    companion object {
        fun gradient(
            top: Color,
            bottom: Color
        ): BackgroundShader = BackgroundShader(
            topShader = DynamicShaders.fromBrush(Brush.verticalGradient(listOf(
                top.copy(alpha = DefaultAlpha.LINE_BACKGROUND_SHADER_START),
                top.copy(alpha = DefaultAlpha.LINE_BACKGROUND_SHADER_END)
            ))),
            bottomShader = DynamicShaders.fromBrush(Brush.verticalGradient(listOf(
                bottom.copy(alpha = DefaultAlpha.LINE_BACKGROUND_SHADER_END),
                bottom.copy(alpha = DefaultAlpha.LINE_BACKGROUND_SHADER_START)
            ))),
        )
    }
}