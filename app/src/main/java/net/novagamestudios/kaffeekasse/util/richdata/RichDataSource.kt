package net.novagamestudios.kaffeekasse.util.richdata

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.launchIn
import net.novagamestudios.common_utils.compose.components.Progress
import net.novagamestudios.common_utils.core.logging.Logger
import kotlin.coroutines.suspendCoroutine

typealias RichDataCollector<T> = FlowCollector<RichData<T>>
typealias RichDataFactory<T> = suspend RichDataCollector<T>.() -> RichData<T>


interface RichDataSource<out T : Any> : RichDataFunctions<T>, RichDataStateFlow<T>, RichDataFlow<T> {
    companion object {
        context (CoroutineScope)
        fun <T : Any> RichDataSource(
            producer: RichDataFactory<T>
        ): RichDataSource<T> = RichDataProducer(this@CoroutineScope, producer)
    }
}


private typealias Updater<T> = suspend RichDataCollector<T>.(RichData<T>) -> Unit

private class RichDataProducer<T : Any>(
    private val coroutineScope: CoroutineScope,
    producer: RichDataFactory<T>
) : RichDataSource<T>, Logger {

    private val wrappedProducer: Updater<T> = updater@{
        emit(RichData.Loading(Progress.Indeterminate))
        emit(producer())
    }

    private val updaterChannel = Channel<Updater<T>>(
        Channel.UNLIMITED,
        onUndeliveredElement = { error { "Failed to deliver updater" } }
    )

    private val outFlow: StateFlow<RichData<T>> = channelFlow {
        var current: RichData<T> = RichData.None()
        val collector = RichDataCollector<T> {
            current = it
            this.send(it)
        }
        with(collector) {
            for (updater in updaterChannel) {
                try {
                    updater(current)
                } catch (e: Exception) {
                    emit(RichDataExceptionError(e))
                }
            }
        }
    }.stateIn(coroutineScope, SharingStarted.Lazily)

    override val replayCache get() = outFlow.replayCache
    override suspend fun collect(collector: RichDataCollector<T>) = outFlow.collect(collector)
    override val value: RichData<T> get() = outFlow.value

    init {
        enqueueUpdater(wrappedProducer)
    }

    private fun enqueueUpdater(updater: Updater<T>) {
        if (!updaterChannel.trySend(updater).isSuccess) {
            error { "Failed to enqueue updater" }
        }
    }
    private suspend fun awaitUpdater(updater: Updater<T>): Unit = suspendCoroutine { continuation ->
        enqueueUpdater { current ->
            updater(current)
            continuation.resumeWith(Result.success(Unit))
        }
    }

    override fun markDirty() = enqueueUpdater { current ->
        if (current is RichData.Data && !current.isDirty) {
            emit(current.copy(isDirty = true))
        }
//        if (current is RichData.Data) {
//            emit(RichData.None())
//        }
    }

    override suspend fun ensureCleanData() {
        outFlow.launchIn(coroutineScope) // Guarantee that the flow is started
        awaitUpdater { current ->
            val needsUpdate = when (current) {
                is RichData.Data -> current.isDirty
                is RichData.Error -> true
                is RichData.Loading -> false
                is RichData.None -> true
            }
            if (needsUpdate) wrappedProducer(current)
        }
    }

    override suspend fun refresh() = awaitUpdater(wrappedProducer)
}


context (RichDataCollector<T>)
suspend fun <T : Any> progress(progress: Progress) {
    emit(RichData.Loading(progress))
}

context (RichDataCollector<T>)
suspend fun <T : Any> progress(progress: Float) {
    emit(RichData.Loading(Progress.Determinate(progress)))
}

