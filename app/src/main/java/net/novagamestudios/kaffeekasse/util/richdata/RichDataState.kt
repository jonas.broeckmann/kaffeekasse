package net.novagamestudios.kaffeekasse.util.richdata

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.Stable
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.screenModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch

@Stable
interface RichDataState<out T : Any> : State<RichData<T>> {
    val isLoading: Boolean
    val isNone: Boolean
    val dataOrNull: T?
    val errorOrNull: RichData.Error<T>?
}

@Stable
interface RichDataStateWithFunctions<T : Any> : RichDataState<T>, RichDataFunctions<T>

@Stable
class MutableRichDataState<T : Any>(
    private val initial: RichData<T> = RichData.None()
) : MutableState<RichData<T>> by mutableStateOf(initial), RichDataState<T> {
    override val isLoading by derivedStateOf { value.isLoading }
    override val isNone by derivedStateOf { value.isNone }
    override val dataOrNull by derivedStateOf { value.dataOrNull }
    override val errorOrNull by derivedStateOf { value.errorOrNull }
}

fun <T : Any> MutableRichDataState<T>.asRichDataState(): RichDataState<T> = this

fun <T : Any> RichDataFlow<T>.collectAsRichStateIn(coroutineScope: CoroutineScope): RichDataState<T> {
    val mutableState = MutableRichDataState<T>()
    coroutineScope.launch {
        collect { mutableState.value = it }
    }
    return mutableState.asRichDataState()
}
fun <T : Any> RichDataSource<T>.collectAsRichStateIn(coroutineScope: CoroutineScope): RichDataStateWithFunctions<T> {
    return (this as RichDataFlow<T>).collectAsRichStateIn(coroutineScope) withFunctions this
}

context (CoroutineScope)
fun <T : Any> RichDataFlow<T>.collectAsRichStateHere() = collectAsRichStateIn(this@CoroutineScope)
context (CoroutineScope)
fun <T : Any> RichDataSource<T>.collectAsRichStateHere() = collectAsRichStateIn(this@CoroutineScope)

context (ScreenModel)
fun <T : Any> RichDataFlow<T>.collectAsRichStateHere() = collectAsRichStateIn(screenModelScope)
context (ScreenModel)
fun <T : Any> RichDataSource<T>.collectAsRichStateHere() = collectAsRichStateIn(screenModelScope)


@Composable
fun <T : Any> RichDataFlow<T>.collectAsRichState(): RichDataState<T> {
    val result = remember { MutableRichDataState<T>() }
    LaunchedEffect(this) {
        collect { result.value = it }
    }
    return result
}

@Composable
fun <T : Any> RichDataSource<T>.collectAsRichState(): RichDataStateWithFunctions<T> {
    val result = (this as RichDataFlow<T>).collectAsRichState()
    return remember(result) { result withFunctions this }
}

suspend fun <T : Any> RichDataFlow<T>.awaitDataOrNull(): T? {
    return filter { it is RichData.Data || it is RichData.Error }.firstOrNull()?.dataOrNull
}




