package net.novagamestudios.kaffeekasse.util.richdata

import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope

interface RichDataFunctions<out T : Any> {
    fun markDirty()
    suspend fun ensureCleanData()
    suspend fun refresh()

    companion object {
        @Suppress("unused")
        val NOOP: RichDataFunctions<Any> = object : RichDataFunctions<Any> {
            override fun markDirty() = Unit
            override suspend fun ensureCleanData() = Unit
            override suspend fun refresh() = Unit
        }
    }
}

operator fun <T : Any> RichDataFunctions<T>.plus(
    other: RichDataFunctions<T>
): RichDataFunctions<T> = object : RichDataFunctions<T> {
    override fun markDirty() {
        this@plus.markDirty()
        other.markDirty()
    }
    override suspend fun ensureCleanData(): Unit = supervisorScope {
        launch { this@plus.ensureCleanData() }
        launch { other.ensureCleanData() }
    }
    override suspend fun refresh(): Unit = supervisorScope {
        launch { this@plus.refresh() }
        launch { other.refresh() }
    }
}



infix fun <T : Any> RichDataStateFlow<out T>.withFunctions(
    functions: RichDataFunctions<T>
): RichDataSource<T> = object : RichDataSource<T>,
    RichDataStateFlow<T> by this,
    RichDataFunctions<T> by functions { }

infix fun <T : Any> RichDataState<T>.withFunctions(
    functions: RichDataFunctions<T>
): RichDataStateWithFunctions<T> = object : RichDataStateWithFunctions<T>,
    RichDataState<T> by this,
    RichDataFunctions<T> by functions { }
