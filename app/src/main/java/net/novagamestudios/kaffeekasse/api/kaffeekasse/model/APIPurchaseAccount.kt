package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import net.novagamestudios.kaffeekasse.model.kaffeekasse.PurchaseAccount

sealed interface APIPurchaseAccount : PurchaseAccount