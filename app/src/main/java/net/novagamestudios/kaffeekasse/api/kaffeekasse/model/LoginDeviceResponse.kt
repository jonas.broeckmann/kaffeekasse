package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LoginDeviceResponse(
    val challenge: String? = null,
    val name: String? = null,
    @SerialName("item_type_id")
    val itemTypeId: Int? = null
)






