package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import net.novagamestudios.common_utils.core.serialization.BooleanAsIntSerializer

@Serializable
data class ItemListResponse(
    @SerialName("item_list")
    val itemList: List<Item>
) {
    @Serializable
    data class Item(
        @SerialName("item_id")
        val id: Int,
        @SerialName("name")
        val originalName: String,
        val gtin: String? = null,
        val price: Double,
        @SerialName("image_url")
        val imageUrl: String? = null,
        val sort: Int,
        @SerialName("sort_p")
        val sortP: Double,
        @SerialName("itemtype_id")
        val itemTypeId: Int,
        val enabled: @Serializable(with = BooleanAsIntSerializer::class) Boolean,
        @SerialName("has_condition_reports")
        val hasConditionReports: JsonElement
    )
}