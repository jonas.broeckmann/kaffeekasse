package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.Serializable

@Serializable
class LogoutUserResponse