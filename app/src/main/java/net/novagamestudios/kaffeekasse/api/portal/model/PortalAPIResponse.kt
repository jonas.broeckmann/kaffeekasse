package net.novagamestudios.kaffeekasse.api.portal.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonNull
import net.novagamestudios.kaffeekasse.model.session.User

@Serializable
open class PortalAPIResponse {
    @SerialName("session")
    val session: Session = Session()
    val infos: List<String> = emptyList()
    val warnings: List<String> = emptyList()
    val errors: List<String> = emptyList()
    @Suppress("unused")
    val navigation: JsonElement = JsonNull
    @Suppress("unused")
    @SerialName("ajaxui")
    val ajaxUI: AjaxUI? = null
    @Serializable
    data class Session(
        @SerialName("login")
        val isLoggedIn: Boolean = false,
        @SerialName("user")
        val username: String? = null,
        @SerialName("displayname")
        val displayName: String? = null
    )
    @Serializable
    data class AjaxUI(
        val reload: Boolean?
    )
}
