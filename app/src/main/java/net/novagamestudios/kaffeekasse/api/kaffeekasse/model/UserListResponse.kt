package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserListResponse(
    @SerialName("user_list")
    val userList: List<BasicUserInfo>
)