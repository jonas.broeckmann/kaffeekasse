package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonArray
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Name

@Serializable
data class UserInfoResponse(
    @SerialName("user_id")
    override val id: Int,
    @SerialName("name")
    override val name: Name,
    @SerialName("balance")
    override val balance: Double,
    @SerialName("favorites")
    override val favorites: JsonArray = JsonArray(emptyList()),
    @SerialName("blacklist")
    override val blacklist: List<BlackWhiteListUserInfo>? = null,
    @SerialName("whitelist")
    override val whitelist: List<BlackWhiteListUserInfo>? = null
) : ExtendedUserInfo

sealed interface ExtendedUserInfo : APIPurchaseAccount {
    override val id: Int
    override val name: Name
    val balance: Double
    val favorites: JsonArray
    val blacklist: List<BlackWhiteListUserInfo>?
    val whitelist: List<BlackWhiteListUserInfo>?
}

