package net.novagamestudios.kaffeekasse.api.hiwi_tracker.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import net.novagamestudios.kaffeekasse.api.portal.model.PortalAPIResponse
import net.novagamestudios.kaffeekasse.model.date_time.ISODate
import net.novagamestudios.kaffeekasse.model.date_time.ISOTime
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry
import kotlin.math.absoluteValue
import kotlin.time.Duration
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.nanoseconds
import kotlin.time.Duration.Companion.seconds
import kotlin.time.times

@Serializable
data class MonthDataResponse(
    @SerialName("session")
    val session: PortalAPIResponse.Session,
    val calendar: Map<ISODate, CalendarEntry>,
    @SerialName("monthname")
    val monthName: String,
    val today: String,
    @SerialName("nextmonthdate")
    val nextMonthDate: String,
    @SerialName("prevmonthdate")
    val prevMonthDate: String,
    @SerialName("selecteddate")
    val selectedDate: String?,
    val entries: List<Entry>,
    @SerialName("total_month")
    val totalMonth: TotalMonth,
    val total: Total,
    @SerialName("last_reset")
    val lastReset: LastReset,
    @SerialName("overtime_expire")
    val overtimeExpire: String,
    val holiday: String? = null,
    val reset: Boolean,
    @SerialName("hidedatecaption")
    val hideDateCaption: Boolean
) {
    @Serializable
    data class CalendarEntry(
        val disabled: Boolean,
        val day: Int,
        @SerialName("hasentry")
        val hasEntry: Boolean,
        val vacation: String? = null,
        val holiday: String? = null
    )
    @Serializable
    data class Entry(
        @SerialName("timetableentry_id")
        override val id: Int,
        @SerialName("timetable_id")
        val timeTableId: Int,
        @SerialName("date")
        override val date: ISODate,
        @SerialName("begin")
        override val begin: ISOTime,
        @SerialName("end")
        override val end: ISOTime,
        @SerialName("breaktime")
        override val breakDurationOrNull: SerializableDuration?,
        @SerialName("note")
        val noteOrNull: String?,
        @SerialName("hoursperday")
        val hoursPerDay: SerializableDuration,
        @SerialName("name")
        val name: String,
        @SerialName("hrsweek")
        val hoursWeek: SerializableDuration,
        @SerialName("hours_worked")
        override val workedDuration: SerializableDuration
    ) : WorkEntry {
        override val note: String get() = noteOrNull ?: ""
    }
    @Serializable
    data class TotalMonth(
        @SerialName("timetable_id")
        val timeTableId: Int,
        val name: String,
        val date: ISODate,
        @SerialName("hoursperweek")
        val hoursPerWeek: SerializableDuration,
        @SerialName("hoursperday")
        val hoursPerDay: SerializableDuration,
        val workdays: Int,
        @SerialName("seconds_worked")
        val secondsWorked: Int,
        val holidays: Int,
        @SerialName("vacation_days")
        val vacationDays: Int?,
        @SerialName("hours_worked")
        val hoursWorked: SerializableDuration,
        @SerialName("hoursthismonth")
        val hoursThisMonth: SerializableDuration,
        @SerialName("hours_balance")
        val hoursBalance: SerializableDuration,
        @SerialName("hours_balance_percent")
        val hoursBalancePercent: Double,
        @SerialName("hours_balance_total_percent")
        val hoursBalanceTotalPercent: Double
    )
    @Serializable
    data class Total(
        @SerialName("timetable_id")
        val timeTableId: Int,
        val date: ISODate,
        @SerialName("hoursperweek")
        val hoursPerWeek: SerializableDuration,
        @SerialName("hoursperday")
        val hoursPerDay: SerializableDuration,
        val workdays: Int,
        @SerialName("seconds_worked")
        val secondsWorked: Int,
        val holidays: Int,
        @SerialName("last_reset")
        val lastReset: String?,
        @SerialName("vacation_days")
        val vacationDays: Int?,
        @SerialName("hours_worked")
        val hoursWorked: SerializableDuration,
        @SerialName("hours_balance")
        val hoursBalance: SerializableDuration,
        @SerialName("hours_balance_percent")
        val hoursBalancePercent: Double
    )
    @Serializable
    data class LastReset(
        val date: ISODate
    )
}
typealias SerializableDuration = @Serializable(with = SerializableDurationSerializer::class) Duration

/*
Examples:
    "hours_worked": "06:30:00",
    "hoursthismonth": "38:00:00.000000",
    "hours_balance": "-31:30:00.000000",
*/
class SerializableDurationSerializer : KSerializer<Duration> {
    override val descriptor: SerialDescriptor =
        PrimitiveSerialDescriptor("SerializableDuration", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: Duration) {
        encoder.encodeString(value.toComponents { hours, minutes, seconds, nanoseconds ->
            (if (hours < 0) "-" else "") + "%02d:%02d:%02d.%06d".format(hours.absoluteValue, minutes, seconds, nanoseconds)
        })
    }
    override fun deserialize(decoder: Decoder): Duration {
        val string = decoder.decodeString()
        val regex = """([+-]?)(\d+):(\d{2}):(\d{2})(\.(\d{1,6}))?$""".toRegex()
        val match = regex.matchEntire(string) ?: throw IllegalArgumentException("Invalid duration: $string")
        val (sign, hours, minutes, seconds, _, nanoseconds) = match.destructured
        return (if (sign == "-") -1 else 1) * (
                hours.toInt().hours +
                minutes.toInt().minutes +
                seconds.toInt().seconds +
                (nanoseconds.toIntOrNull() ?: 0).nanoseconds
        )
    }
}