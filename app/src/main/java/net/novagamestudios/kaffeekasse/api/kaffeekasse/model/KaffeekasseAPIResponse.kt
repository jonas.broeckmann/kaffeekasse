package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.api.portal.model.PortalAPIResponse


@Serializable
data class KaffeekasseAPIResponse(
    val error: Error? = null
) : PortalAPIResponse() {
    @Serializable
    data class Error(
        val code: Code,
        val string: String
    ) {
        @Serializable
        @JvmInline
        value class Code(val value: Long) {
            companion object {
                val NoError = Code(0)
                val Unknown = Code(1)
                val AccessDenied = Code(3)
                val UnsupportedAuthMethod = Code(4)
                val AuthenticationFailure = Code(5)
                val MethodDisabled = Code(6)
                val AuthenticationRequired = Code(7)
                val InvalidFieldValue = Code(207)
                val MissingFieldValue = Code(208)
                val NotFound = Code(209)
                val InvalidAction = Code(210)
                val NotImplemented = Code(0xDEADBEEF)
                val NoCodeYet = Code(0xEA7DEADBEEF)
            }
        }
        val isError get() = code != Code.NoError
    }
    val hasError get() = error?.takeIf { it.isError } != null || errors.isNotEmpty()
}




