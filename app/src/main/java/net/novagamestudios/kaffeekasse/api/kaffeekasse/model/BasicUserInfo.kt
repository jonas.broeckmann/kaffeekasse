package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.novagamestudios.common_utils.core.serialization.BooleanAsIntSerializer
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Name

@Serializable
data class BasicUserInfo(
    @SerialName("id")
    override val id: Int,
    @SerialName("name")
    override val name: Name,
    @SerialName("empty_pin")
    val noPinSet: @Serializable(with = BooleanAsIntSerializer::class) Boolean? = null
) : APIPurchaseAccount {
    val mayHavePin get() = noPinSet == null || !noPinSet
}