package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.api.portal.model.PortalAPIResponse


@Serializable
data class LoggedInUserResponse(
    @SerialName("session")
    val session: PortalAPIResponse.Session,
    @SerialName("Kontostand")
    val balance: Balance
) {
    @Serializable
    data class Balance(
        @SerialName("mybalance")
        val myBalance: MyBalance
    ) {
        @Serializable
        data class MyBalance(
            @SerialName("user_id")
            val userId: Int,
            val user: String,
            val outgoing: Double,
            val incoming: Double,
            val paid: Double,
            val refunded: Double,
            val deposited: Double,
            val withdrawn: Double,
            val total: Double
        ) {
            val firstName by lazy { user.split(", ")[1] }
            val lastName by lazy { user.split(", ")[0] }
        }
    }
}