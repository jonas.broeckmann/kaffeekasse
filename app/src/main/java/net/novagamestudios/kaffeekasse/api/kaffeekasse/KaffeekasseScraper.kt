package net.novagamestudios.kaffeekasse.api.kaffeekasse

import io.ktor.http.URLBuilder
import io.ktor.http.parameters
import it.skrape.selects.Doc
import it.skrape.selects.DocElement
import it.skrape.selects.attribute
import it.skrape.selects.html5.a
import it.skrape.selects.html5.form
import it.skrape.selects.html5.option
import it.skrape.selects.html5.select
import it.skrape.selects.html5.table
import it.skrape.selects.html5.td
import it.skrape.selects.html5.tr
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.kaffeekasse.api.portal.PortalClient
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Cart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.KnownItem
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ManualBillDetails
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Name.Companion.toNameOrNull
import net.novagamestudios.kaffeekasse.model.kaffeekasse.ScraperPurchaseAccount
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Transaction
import net.novagamestudios.kaffeekasse.model.kaffeekasse.isEmpty
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class KaffeekasseScraper(
    client: PortalClient
) : PortalClient.Module(client, "kaffeekasse") {

    private val manualBillUrl = URLBuilder(moduleUrl).apply {
        parameters.append("manualbill", "")
    }.build()
    val transactionsUrl = URLBuilder(moduleUrl).apply {
        parameters.append("balance", "")
    }.build()

    suspend fun manualBillDetails(): ManualBillDetails = with(client) {
        scrapePage(manualBillUrl) {
            scrapeManualBillDetails()
        }
    }

    suspend fun transactions(): List<Transaction> = with(client) {
        scrapePage(transactionsUrl) {
            scrapeTransactions()
        }
    }

    suspend fun submitCart(account: ScraperPurchaseAccount, cart: Cart): Unit = with(client) {
        if (cart.isEmpty()) return
        post(
            manualBillUrl,
            formParameters = parameters {
                cart.forEach { (item, count) ->
                    info { "adding @ $account : $item x $count" }
                    append("tdata[account_id][]", "${account.id}")
                    append("tdata[item_id][]", "${item.id}")
                    append("tdata[item_count][]", "$count")
                    append("manualbill", "")
                    append("lockedfrom", "1")
                }
            }
        )
    }
}

private fun Doc.scrapeManualBillDetails(): ManualBillDetails = form {
    table {
        val accounts = select {
            withId = "manual_account_id"
            option {
                findAll {
                    mapNotNull { option ->
                        if (option.hasAttribute("disabled")) null
                        else if (option.attribute("value").isBlank()) null
                        else ManualBillDetails.PurchaseAccount(
                            name = option.text.let {
                                requireNotNull(it.toNameOrNull()) { "Invalid account name: $it" }
                            },
                            id = option.attribute("value").let {
                                requireNotNull(it.toIntOrNull()) { "Invalid account id: $it" }
                            },
                            isDefault = option.attribute("selected") == "selected"
                        )
                    }
                }
            }
        }
        val groups = select {
            withId = "manual_item_id"
            option {
                findAll {
                    val groups = mutableListOf<ManualBillDetails.ItemGroup>()
                    var currentGroup: String? = null
                    var currentItems = mutableListOf<ManualBillDetails.Item>()
                    for (option in this) {
                        if (option.hasAttribute("disabled")) {
                            if (currentGroup != null) {
                                groups += ManualBillDetails.ItemGroup(
                                    currentGroup,
                                    currentItems
                                )
                            }
                            currentGroup = option.text
                            currentItems = mutableListOf()
                        } else if (option.attribute("value").isBlank()) continue
                        else {
                            val id = option.attribute("value").let {
                                requireNotNull(it.toIntOrNull()) { "Invalid item id: $it" }
                            }
                            val name = option.text
                            val knownItems = KnownItem.byId[id]
                            if (knownItems != null) knownItems.forEach {
                                currentItems += ManualBillDetails.Item(
                                    originalName = name,
                                    id = id,
                                    knownItem = it
                                )
                            } else {
                                currentItems += ManualBillDetails.Item(
                                    originalName = option.text,
                                    id = id
                                )
                            }
                        }
                    }
                    if (currentGroup != null) {
                        groups += ManualBillDetails.ItemGroup(
                            currentGroup,
                            currentItems
                        )
                    }
                    groups
                }
            }
        }
        ManualBillDetails(accounts, groups)
    }
}

private fun Doc.scrapeTransactions(): List<Transaction> = table {
    withClass = "transactions"
    tr {
        findAll {
            mapNotNull { row ->
                if (row.children.any { it.tagName == "th" }) return@mapNotNull null
                row.scrapeTransactionRow()
            }
        }
    }
}
private fun String.euros() = removeSuffix("€").toDouble()
private fun DocElement.scrapeTransactionRow(): Transaction? = td {
    findAll {
        if (size != 7) return@findAll null
        // Example: 2024-02-26 13:05:21
        val dateTime = LocalDateTime.parse(
            this[0].text.trim(),
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        )
        val payer = this[1].text.trim().takeUnless { it == "-" }
        val payee = this[2].text.trim()
        val purpose = this[3].text.parseTransactionPurpose()
        val total = this[4].text.trim().euros()
        val newBalance = this[5].text.trim().euros()
        val refundId = this[6].a { attribute("href") }.substringAfterLast("=")
        Transaction(
            date = dateTime,
            payer = payer,
            payee = payee,
            purpose = purpose,
            total = total,
            newBalance = newBalance,
            refundId = refundId
        )
    }
}
private fun String.parseTransactionPurpose(): Transaction.Purpose {
    val trimmed = trim()
    return when (trimmed.lowercase()) {
        "einzahlung" -> Transaction.Purpose.Deposit
        else -> {
            val match = "([0-9]+)x (.+) à (-?[0-9.]+€?)".toRegex().matchEntire(trimmed)
                ?: return Transaction.Purpose.Other(trimmed)
            val (count, itemName, unitPrice) = match.destructured
            Transaction.Purpose.Purchase(itemName, count, unitPrice.euros())
        }
    }
}
