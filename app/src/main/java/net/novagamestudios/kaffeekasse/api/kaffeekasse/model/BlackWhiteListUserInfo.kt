package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Name

@Serializable
data class BlackWhiteListUserInfo(
    @SerialName("target_id")
    override val id: Int,
    @SerialName("name")
    override val name: Name
) : APIPurchaseAccount