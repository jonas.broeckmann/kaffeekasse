package net.novagamestudios.kaffeekasse.api.hiwi_tracker

import io.ktor.http.URLBuilder
import io.ktor.http.parameters
import kotlinx.datetime.format
import net.novagamestudios.kaffeekasse.api.portal.PortalClient
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.WorkEntry

class HiwiTrackerScraper(
    client: PortalClient
) : PortalClient.Module(client, "hiwi") {
    suspend fun submitWorkEntry(workEntry: WorkEntry): Unit = with(client) {
        post(
            moduleUrl,
            formParameters = parameters {
                append("hw_date", workEntry.date.format(HiwiTrackerFormats.DateFormat))
                append("hw_begin", workEntry.begin.format(HiwiTrackerFormats.TimeFormat))
                append("hw_end", workEntry.end.format(HiwiTrackerFormats.TimeFormat))
                append("hw_breaktime", with(HiwiTrackerFormats) { workEntry.breakDurationOrNull?.apiFormat() ?: "" })
                append("hw_note", workEntry.note)
                append("savetimes", "")
            }
        )
    }

    suspend fun deleteWorkEntry(workEntryId: Int): Unit = with(client) {
        post(
            URLBuilder(moduleUrl).apply {
                parameters.append("deleteentry", "$workEntryId")
            }.build(),
        )
    }
}