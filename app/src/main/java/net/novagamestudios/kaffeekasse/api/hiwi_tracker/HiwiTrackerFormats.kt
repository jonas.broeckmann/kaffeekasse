package net.novagamestudios.kaffeekasse.api.hiwi_tracker

import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalTime
import kotlinx.datetime.format.char
import kotlin.time.Duration


internal object HiwiTrackerFormats {
    val DateFormat = LocalDate.Format {
        year()
        char('-')
        monthNumber()
        char('-')
        dayOfMonth()
    }
    val TimeFormat = LocalTime.Format {
        hour()
        char(':')
        minute()
    }
    fun Duration.apiFormat() = toComponents { hours, minutes, _, _ ->
        "%02d:%02d".format(hours, minutes)
    }
}


