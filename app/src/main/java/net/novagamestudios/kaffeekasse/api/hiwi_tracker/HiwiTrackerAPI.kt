package net.novagamestudios.kaffeekasse.api.hiwi_tracker

import io.ktor.http.parameters
import kotlinx.datetime.format
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.kaffeekasse.api.hiwi_tracker.model.MonthDataResponse
import net.novagamestudios.kaffeekasse.api.portal.PortalClient
import net.novagamestudios.kaffeekasse.model.hiwi_tracker.MonthKey


class HiwiTrackerAPI(
    client: PortalClient
) : PortalClient.Module(client, "hiwi"), Logger {
    suspend fun fetchDataForMonth(month: MonthKey): MonthDataResponse = with(client) {
        requestAPI(
            urlParameters = parameters {
                append("selectdate", month.toLocalDate().format(HiwiTrackerFormats.DateFormat))
            }
        ).decodeAs<MonthDataResponse>()
    }
}