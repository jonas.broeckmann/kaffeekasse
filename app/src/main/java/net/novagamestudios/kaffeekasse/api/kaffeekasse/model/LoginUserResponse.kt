package net.novagamestudios.kaffeekasse.api.kaffeekasse.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class LoginUserResponse(
    @SerialName("user_id")
    val userId: Int
)

