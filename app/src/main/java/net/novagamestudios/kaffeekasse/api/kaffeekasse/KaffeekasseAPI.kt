package net.novagamestudios.kaffeekasse.api.kaffeekasse

import io.ktor.http.Parameters
import io.ktor.http.parameters
import io.ktor.util.sha1
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.ItemListResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.KaffeekasseAPIResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.LoggedInUserResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.LoginDeviceResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.LoginUserResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.LogoutUserResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.PurchaseResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.UserInfoResponse
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.UserListResponse
import net.novagamestudios.kaffeekasse.api.portal.PortalClient
import net.novagamestudios.kaffeekasse.api.kaffeekasse.model.KaffeekasseAPIResponse.Error.Code as ErrorCode

class KaffeekasseAPI(
    client: PortalClient
) : PortalClient.Module(client, "kaffeekasse"), Logger {

    private suspend inline fun <reified R> invokeFunction(
        name: String?,
        arguments: List<Pair<String, String>>,
        updateSession: Boolean = false,
        validateLoggedIn: Boolean = true
    ): Result<R> = with(client) {
        debug { """Invoking ${name?.let { "\"$it\"" }} with arguments $arguments""" }
        val raw = requestAPI(
            urlParameters = parameters { if (name != null) append(name, "") },
            formParameters = Parameters.build { arguments.forEach { (key, value) -> append(key, value) } },
            updateSession = updateSession
        )
        val response = raw.decodeAs<KaffeekasseAPIResponse>()
        with(response) {
            error?.takeIf { it.isError }?.let { error { "Error code ${it.code}: ${it.string}" } }
        }
        return when {
            response.hasError -> Result.Error(response)
            validateLoggedIn && !response.session.isLoggedIn -> {
                debug { "Not logged in" }
                Result.NotLoggedIn(response)
            }
            else -> {
                val result = try {
                    raw.decodeAs<R>()
                } catch (e: Exception) {
                    error(e) { "Failed to decode response as ${R::class.qualifiedName}" }
                    throw e
                }
                Result.Success(response, result)
            }
        }
    }

    sealed interface Result<T> {
        val response: KaffeekasseAPIResponse
        val errorCode: ErrorCode? get() = response.error?.code

        data class Error<T>(override val response: KaffeekasseAPIResponse) : Result<T>
        data class NotLoggedIn<T>(override val response: KaffeekasseAPIResponse) : Result<T>
        data class Success<T>(override val response: KaffeekasseAPIResponse, val result: T) : Result<T>
    }

    suspend fun loggedInUser() = invokeFunction<LoggedInUserResponse>(
        name = null,
        emptyList()
    )


    suspend fun userList() = invokeFunction<UserListResponse>(
        name = "user_list",
        emptyList()
    )

    suspend fun userInfo(userId: Int) = invokeFunction<UserInfoResponse>(
        name = "user_info",
        listOf("user_id" to "$userId")
    )

    suspend fun itemList(itemTypeId: Int? = null) = invokeFunction<ItemListResponse>(
        name = "item_list",
        listOfNotNull(itemTypeId?.let { "item_type_id" to "$it" })
    )


    private suspend fun loginDevice(deviceId: String, challengeResponse: String? = null) = invokeFunction<LoginDeviceResponse>(
        name = "login_device",
        listOfNotNull(
            "id" to deviceId,
            challengeResponse?.let { "response" to it }
        ),
        updateSession = true,
        validateLoggedIn = false
    )

    @OptIn(ExperimentalStdlibApi::class)
    suspend fun performDeviceLogin(deviceId: String, apiKey: String): DeviceLoginResult {
        // Phase 1
        val challenge = when (val response = loginDevice(deviceId)) {
            is Result.Error -> when (response.errorCode) {
                ErrorCode.InvalidFieldValue -> return DeviceLoginResult.Failure.InvalidDeviceId
                ErrorCode.AccessDenied -> return DeviceLoginResult.Failure.AccessDenied
                else -> return DeviceLoginResult.Failure.UnknownError(response.response)
            }
            is Result.NotLoggedIn -> throw AssertionError("Not logged in")
            is Result.Success -> response.result.challenge
                ?: return DeviceLoginResult.Failure.UnknownError(response.response)
        }

        // Phase 2 (useless, but yolo)
        val hash = sha1((challenge + apiKey).encodeToByteArray())
            .toHexString(HexFormat { upperCase = false })
        return when (val response = loginDevice(deviceId, hash)) {
            is Result.Error -> when (response.errorCode) {
                ErrorCode.AuthenticationFailure -> DeviceLoginResult.Failure.AuthenticationFailure
                else -> DeviceLoginResult.Failure.UnknownError(response.response)
            }
            is Result.NotLoggedIn -> throw AssertionError("Not logged in")
            is Result.Success -> DeviceLoginResult.LoggedIn(response.result)
        }
    }

    sealed interface DeviceLoginResult {
        sealed interface Failure : DeviceLoginResult {
            data object InvalidDeviceId : Failure
            data object AccessDenied : Failure
            data object AuthenticationFailure : Failure
            data class UnknownError(val response: KaffeekasseAPIResponse) : Failure
        }
        data class LoggedIn(val response: LoginDeviceResponse) : DeviceLoginResult
    }


    private suspend fun loginUser(
        userId: Int,
        pin: String? = null,
        rwthId: String? = null,
        key: String? = null
    ): Result<LoginUserResponse> {
        val authList = listOfNotNull(
            pin?.let { "pin" to it },
            rwthId?.let { "rwth_id" to it },
            key?.let { "key" to it }
        )
        require(authList.size <= 1) { "More than one auth type specified" }
        val auth = authList.firstOrNull()
        return invokeFunction<LoginUserResponse>(
            name = "login_user",
            listOfNotNull(
                "user_id" to "$userId",
                auth?.let { "auth_type" to it.first },
                auth?.let { it.first to it.second }
            ),
            updateSession = true,
            validateLoggedIn = false
        )
    }

    suspend fun performUserLogin(
        userId: Int,
        pin: String? = null,
        rwthId: String? = null,
        key: String? = null
    ): UserLoginResult {
        return when (val response = loginUser(userId, pin, rwthId, key)) {
            is Result.Error -> when (response.errorCode) {
                ErrorCode.InvalidAction -> UserLoginResult.Failure.PrivateDevice
                ErrorCode.AuthenticationFailure -> UserLoginResult.Failure.UserAuthenticationFailure
                else -> UserLoginResult.Failure.UnknownError(response.response)
            }
            is Result.NotLoggedIn -> throw AssertionError("Not logged in")
            is Result.Success -> UserLoginResult.LoggedIn(response.result)
        }
    }

    sealed interface UserLoginResult {
        sealed interface Failure : UserLoginResult {
            data object PrivateDevice : Failure
            data object UserAuthenticationFailure : Failure
            data class UnknownError(val response: KaffeekasseAPIResponse) : Failure
        }
        data class LoggedIn(val response: LoginUserResponse) : UserLoginResult
    }


    suspend fun logoutUser() = invokeFunction<LogoutUserResponse>(
        name = "logout_user",
        emptyList(),
        updateSession = true,
        validateLoggedIn = false
    )


    suspend fun purchase(
        itemId: Int,
        count: Int,
        targetUserId: Int? = null
    ): Result<PurchaseResponse> = invokeFunction<PurchaseResponse>(
        name = "purchase",
        listOfNotNull(
            "item_id" to "$itemId",
            "item_count" to "$count",
            targetUserId?.let { "other_user_id" to "$it" }
        )
    )
}
