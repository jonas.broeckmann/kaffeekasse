package net.novagamestudios.kaffeekasse.api.portal

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.cookies.HttpCookies
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.forms.FormDataContent
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.Parameters
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.http.parameters
import io.ktor.http.parseServerSetCookieHeader
import io.ktor.http.renderSetCookieHeader
import io.ktor.serialization.kotlinx.json.json
import it.skrape.core.htmlDocument
import it.skrape.selects.Doc
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newCoroutineContext
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.debug
import net.novagamestudios.common_utils.core.logging.error
import net.novagamestudios.common_utils.core.logging.info
import net.novagamestudios.common_utils.core.logging.verbose
import net.novagamestudios.common_utils.core.logging.warn
import net.novagamestudios.kaffeekasse.api.portal.model.PortalAPIResponse
import net.novagamestudios.kaffeekasse.model.credentials.Login
import net.novagamestudios.kaffeekasse.model.credentials.isValid
import net.novagamestudios.kaffeekasse.util.MutableCookiesStorage
import net.novagamestudios.kaffeekasse.util.WrappedJsonSerializationException

class PortalClient(
    coroutineScope: CoroutineScope
) : Logger {
    private val baseUrl = Url("https://embedded.rwth-aachen.de")

    @OptIn(ExperimentalCoroutinesApi::class)
    private val computationScope = coroutineScope.newCoroutineContext(Dispatchers.IO)

    private val jsonFormat by lazy {
        Json {
            prettyPrint = true
            isLenient = true
            ignoreUnknownKeys = true
        }
    }
    private val cookiesStorage by lazy { MutableCookiesStorage(mutableStateListOf()) }
    private val client = HttpClient {
        install(HttpCookies) {
            storage = cookiesStorage
        }
        install(ContentNegotiation) {
            json(
                jsonFormat,
                contentType = ContentType.Text.Any // Uses "text/json"
            )
        }
    }

    internal inline fun <reified T> JsonElement.decodeAs() = try {
        jsonFormat.decodeFromJsonElement<T>(this)
    } catch (e: Exception) {
        throw WrappedJsonSerializationException(e, this)
    }


    private val portal = object : Module(this, "portal") {
        val currentSession = mutableStateOf(Session())
        suspend fun updateSession(restore: String? = null): Unit = withContext(computationScope) {
            if (restore != null) {
                debug { "Restoring cookie: $restore" }
                try {
                    val cookie = parseServerSetCookieHeader(restore)
                    cookiesStorage.addCookie(baseUrl, cookie)
                    debug { "Restored cookie: $cookie" }
                } catch (e: Throwable) {
                    error(e) { "Failed to restore cookie" }
                }
            }

            debug { "Updating session" }
            val response = requestAPI(updateSession = false).decodeAs<PortalAPIResponse>()
            val serializedCookie = cookiesStorage.get(baseUrl)
                .firstOrNull { it.name == SessionCookieName }
                ?.let { renderSetCookieHeader(it) }
            debug { "Session: ${response.session}; Cookie: $serializedCookie" }

            currentSession.value = Session(
                response.session,
                serializedCookie
            )
        }
        private val SessionCookieName = "PORTALSESSID"
    }

    val session: State<Session> get() = portal.currentSession

    data class Session(
        val response: PortalAPIResponse.Session = PortalAPIResponse.Session(),
        val data: String? = null
    )

    suspend fun forceUpdateSession(restore: String? = null) = portal.updateSession(restore)

    suspend fun login(login: Login) {
        require(login.isValid) { "Invalid login" }
        if (session.value.response.username == login.username) return
        debug { "Logging in as ${login.username}" }
        post(
            portal.moduleUrl,
            parameters {
                append("username", login.username)
                append("password", login.password)
                append("login", "")
            },
            updateSession = true
        )
        debug { "Logged in: ${session.value.response.isLoggedIn}" }
    }

    suspend fun logoutAll() {
        cookiesStorage.clear(baseUrl)
        portal.updateSession()
    }


    private suspend fun request(
        url: Url,
        updateSession: Boolean = false,
        block: HttpRequestBuilder.() -> Unit = {}
    ): HttpResponse = withContext(computationScope) {
        debug { """Making request to "$url"""" }
        val httpResponse = client.request(url, block)
        debug { """Response: ${httpResponse.status}""" }
        if (updateSession) portal.updateSession()
        httpResponse
    }

    context (Module)
    internal suspend fun requestAPI(
        urlParameters: Parameters = Parameters.Empty,
        formParameters: Parameters? = null,
        method: HttpMethod = HttpMethod.Post,
        updateSession: Boolean = false,
        block: HttpRequestBuilder.() -> Unit = {}
    ): JsonObject {
        val url = URLBuilder(moduleUrl).apply {
            parameters.append("json", "")
            parameters.appendAll(urlParameters)
        }.build()
        val httpResponse = request(url, updateSession) {
            this.method = method
            if (formParameters != null) {
                if (method == HttpMethod.Get) throw IllegalArgumentException("Cannot send form parameters with GET request")
                setBody(FormDataContent(formParameters))
            }
            block()
        }

        val rawJson = httpResponse.body<JsonObject>()
        val apiResponse = rawJson.decodeAs<PortalAPIResponse>()
        with(apiResponse) {
            errors.forEach { error { it } }
            warnings.forEach { warn { it } }
            infos.forEach { info { it } }
        }
        verbose { "Raw json: $rawJson" }
        return rawJson
    }

    internal suspend fun post(
        url: Url,
        formParameters: Parameters = Parameters.Empty,
        updateSession: Boolean = false,
        block: HttpRequestBuilder.() -> Unit = {}
    ): HttpResponse = request(url, updateSession) {
        method = HttpMethod.Post
        setBody(FormDataContent(formParameters))
        block()
    }

    internal suspend fun <R> scrapePage(
        url: Url,
        block: HttpRequestBuilder.() -> Unit = {},
        scrape: Doc.() -> R
    ): R = withContext(computationScope) {
        val response = request(url) {
            method = HttpMethod.Get
            block()
        }
        htmlDocument(response.bodyAsText()) { scrape() }
    }

    abstract class Module internal constructor(
        val client: PortalClient,
        protected val name: String
    ) {
        internal val moduleUrl by lazy { URLBuilder(client.baseUrl).apply { host = "$name.$host" }.build() }
        protected val computationScope get() = client.computationScope
    }
}


