package net.novagamestudios.kaffeekasse.gitlab

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.app.AppVersion
import net.novagamestudios.kaffeekasse.model.date_time.ISODateTime

@Serializable
data class GitLabPackage(
    val id: Int,
    val name: String,
    val version: AppVersion,
    @SerialName("_links")
    val links: Links,
    @SerialName("created_at")
    val createdAt: ISODateTime
) {
    @Serializable
    data class Links(
        @SerialName("web_path")
        val webPath: String
    ) {
        val projectPath get() = webPath.removePrefix("/").substringBefore("/-/packages")
    }
}