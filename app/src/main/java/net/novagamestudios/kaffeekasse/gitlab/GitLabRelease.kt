package net.novagamestudios.kaffeekasse.gitlab

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.app.AppVersion
import net.novagamestudios.kaffeekasse.model.date_time.ISODateTime

@Serializable
data class GitLabRelease(
    val name: String,
    @SerialName("tag_name")
    val tagName: String,
    val description: String?,
    @SerialName("created_at")
    val createdAt: ISODateTime,
    @SerialName("released_at")
    val releasedAt: ISODateTime,
    @SerialName("upcoming_release")
    val upcomingRelease: Boolean,
    val author: Author,
    val commit: Commit,
    @SerialName("commit_path")
    val commitPath: String,
    @SerialName("tag_path")
    val tagPath: String,
    val assets: Assets,
    val evidences: List<Evidence>,
    @SerialName("_links")
    val links: Links
) {
    val version by lazy { AppVersion.findIn(tagName) }

    @Serializable
    data class Author(
        val id: Int,
        val username: String,
        val name: String,
        val state: String,
        val locked: Boolean,
        @SerialName("avatar_url")
        val avatarUrl: String,
        @SerialName("web_url")
        val webUrl: String
    )

    @Serializable
    data class Commit(
        val id: String,
        @SerialName("short_id")
        val shortId: String,
        @SerialName("created_at")
        val createdAt: ISODateTime,
        @SerialName("parent_ids")
        val parentIds: List<String>,
        val title: String,
        val message: String,
        @SerialName("author_name")
        val authorName: String,
        @SerialName("author_email")
        val authorEmail: String,
        @SerialName("authored_date")
        val authoredDate: ISODateTime,
        @SerialName("committer_name")
        val committerName: String,
        @SerialName("committer_email")
        val committerEmail: String,
        @SerialName("committed_date")
        val committedDate: ISODateTime,
//        val trailers: Map<String, String>,
//        @SerialName("extended_trailers")
//        val extendedTrailers: Map<String, String>,
        @SerialName("web_url")
        val webUrl: String
    )

    @Serializable
    data class Assets(
        val count: Int,
        val sources: List<Source>,
        val links: List<Link>
    ) {
        @Serializable
        data class Source(
            val format: String,
            val url: String
        )

        @Serializable
        data class Link(
            val id: Int,
            val name: String,
            val url: String,
            @SerialName("direct_asset_url")
            val directAssetUrl: String,
            @SerialName("link_type")
            val linkType: String
        )
    }

    @Serializable
    data class Evidence(
        val sha: String,
        val filepath: String,
        @SerialName("collected_at")
        val collectedAt: ISODateTime
    )

    @Serializable
    data class Links(
        @SerialName("closed_issues_url")
        val closedIssuesUrl: String,
        @SerialName("closed_merge_requests_url")
        val closedMergeRequestsUrl: String,
        @SerialName("merged_merge_requests_url")
        val mergedMergeRequestsUrl: String,
        @SerialName("opened_issues_url")
        val openedIssuesUrl: String,
        @SerialName("opened_merge_requests_url")
        val openedMergeRequestsUrl: String,
        val self: String
    )
}