package net.novagamestudios.kaffeekasse.gitlab

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json

class GitLab(
    val instanceUrl: String,
    private val projectPath: String,
    private val projectId: Int
) {
    val projectUrl get() = "$instanceUrl/$projectPath"

    private val apiUrl get() = "$instanceUrl/api/v4"
    private val projectApiUrl get() = "$apiUrl/projects/$projectId"

    @OptIn(ExperimentalCoroutinesApi::class)
    private val networkingScope = Dispatchers.IO.limitedParallelism(1)
    private val client = HttpClient {
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                isLenient = true
                ignoreUnknownKeys = true
                allowSpecialFloatingPointValues = true
                allowStructuredMapKeys = true
                useArrayPolymorphism = false
            })
        }
    }

    private suspend inline fun <reified T> fetch(url: String): T = withContext(networkingScope) {
        client.get(url)
            .also { if (it.status != HttpStatusCode.OK) throw IllegalStateException("Request failed: ${it.bodyAsText()}") }
            .body()
    }

    val packages = Packages()
    val releases = Releases()

    inner class Packages {
        suspend fun list() = fetch<List<GitLabPackage>>("$projectApiUrl/packages")
        @Suppress("unused")
        suspend fun `package`(packageId: Int) = fetch<GitLabPackage>("$projectApiUrl/packages/$packageId")
        suspend fun packageFiles(packageId: Int) = fetch<List<GitLabPackageFile>>("$projectApiUrl/packages/$packageId/package_files")
    }
    inner class Releases {
        suspend fun list() = fetch<List<GitLabRelease>>("$projectApiUrl/releases")
    }
}

