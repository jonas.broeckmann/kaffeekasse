package net.novagamestudios.kaffeekasse.gitlab

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.novagamestudios.kaffeekasse.model.date_time.ISODateTime

@Serializable
data class GitLabPackageFile(
    val id: Int,
    @SerialName("package_id")
    val packageId: Int,
    @SerialName("created_at")
    val createdAt: ISODateTime,
    @SerialName("file_name")
    val filename: String,
    val size: Long,
    @SerialName("file_md5")
    val fileMD5: String?,
    @SerialName("file_sha1")
    val fileSHA1: String?,
    @SerialName("file_sha256")
    val fileSHA256: String?
) {
    fun downloadUrl(projectUrl: String): String = "$projectUrl/-/package_files/$id/download"
}