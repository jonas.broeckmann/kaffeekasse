package net.novagamestudios.kaffeekasse

import android.app.Application
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.snapshotFlow
import cafe.adriel.voyager.navigator.Navigator
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider
import net.novagamestudios.kaffeekasse.repositories.Settings
import org.acra.ACRA
import org.acra.config.limiter
import org.acra.config.mailSender
import org.acra.data.StringFormat
import org.acra.ktx.initAcra


object CrashHandling {
    private const val ReportEmail = "broeckmann@embedded.rwth-aachen.de"

    context (Application)
    fun init() {
        if (BuildConfig.DEBUG) return
        initAcra {
            buildConfigClass = BuildConfig::class.java
            reportFormat = StringFormat.JSON
            mailSender {
                val appName = getString(R.string.app_name)
                mailTo = ReportEmail

                reportAsFile = true
                reportFileName = "${appName.uppercase()}-CRASH-REPORT.json"

                subject = "$appName Crash Report"
                body = """
                    Whoops! It seems like $appName crashed.
                    Please help us to improve the app by sending this crash report.
                """.trimIndent()
            }
            limiter {
                enabled = true
            }
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    private val jsonFormat = Json {
        allowStructuredMapKeys = true
        ignoreUnknownKeys = true
        explicitNulls = false
        serializersModule = Settings.serializersModule
    }

    context (RepositoryProvider)
    suspend fun syncInformation() = supervisorScope {
        launch {
            settingsRepository.values.collectLatest {
                ACRA.errorReporter.putCustomData("CurrentSettings", jsonFormat.encodeToString(it))
            }
        }
        launch {
            snapshotFlow { portalRepository.session.value }.collectLatest {
                ACRA.errorReporter.putCustomData("CurrentPortalSession", "$it")
            }
        }
    }

    @Composable
    fun updateNavigation(navigator: Navigator) {
        LaunchedEffect(navigator.lastItem) {
            ACRA.errorReporter.putCustomData("CurrentNavigationScreen", "${navigator.lastItem::class.qualifiedName}")
        }
    }
}
