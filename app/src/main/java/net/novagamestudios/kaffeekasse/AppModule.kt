package net.novagamestudios.kaffeekasse

import androidx.compose.runtime.mutableStateMapOf
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Cart
import net.novagamestudios.kaffeekasse.model.kaffeekasse.Item
import net.novagamestudios.kaffeekasse.model.kaffeekasse.MutableCart
import net.novagamestudios.kaffeekasse.model.session.User
import net.novagamestudios.kaffeekasse.repositories.RepositoryProvider


sealed interface AppModule {
    val id: String
}


class KaffeekasseModule : AppModule {
    override val id = KaffeekasseModule::class.simpleName!!

    val cartProvider = CartProvider()

    companion object {
        val RepositoryProvider.kaffeekasseCartProvider get() = modules.require<KaffeekasseModule>().cartProvider
    }
}

class HiwiTrackerModule : AppModule {
    override val id = HiwiTrackerModule::class.simpleName!!
}


class AppModules(
    modules: List<AppModule>
) : List<AppModule> by modules {
    constructor(vararg modules: AppModule) : this(listOf(*modules))

    inline fun <reified T : AppModule> get() = filterIsInstance<T>().singleOrNull()
    inline fun <reified T : AppModule> require() = get<T>() ?: error("No single module of type ${T::class.simpleName}")
}

class CartProvider {

    private val cartByUser = mutableMapOf<User, MutableCart>()

    operator fun get(user: User): MutableCart = cartByUser.getOrPut(user) { MutableCartImpl() }

    private class MutableCartImpl : MutableCart {
        private val content = mutableStateMapOf<Item, Int>()
        override fun iterator(): Iterator<Cart.Entry> = object : Iterator<Cart.Entry> {
            private val iterator = content.iterator()
            override fun hasNext() = iterator.hasNext()
            override fun next() = iterator.next().let { (item, count) ->
                Cart.Entry(item, count)
            }
        }
        override operator fun get(item: Item) = content[item] ?: 0
        override operator fun plusAssign(item: Item) {
            content[item] = (content[item] ?: 0) + 1
        }
        override operator fun minusAssign(item: Item) {
            val count = (content[item] ?: 0) - 1
            if (count < 1) content -= item
            else content[item] = count
        }
        override fun removeAll(item: Item) {
            content -= item
        }
        override fun clear() {
            content.clear()
        }
        override operator fun contains(item: Item) = item in content
        override val itemCount get() = content.values.sum()
    }
}


