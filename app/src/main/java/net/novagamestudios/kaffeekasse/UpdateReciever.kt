package net.novagamestudios.kaffeekasse

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import net.novagamestudios.common_utils.core.logging.Logger
import net.novagamestudios.common_utils.core.logging.info


class UpdateReceiver : BroadcastReceiver(), Logger {
    override fun onReceive(context: Context, intent: Intent) {
        info { "Received update intent: $intent" }
        val app = context.applicationContext as App
        app.updateController.handleInstallStatus(intent)
    }
}
