package net.novagamestudios.kaffeekasse.model.app

import org.junit.Assert
import org.junit.Test

class AppVersionTest {
    @Test
    fun stringParsingValid() {
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Stable, 0), AppVersion("1.2.3"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Beta, 0), AppVersion("1.2.3-beta"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Beta, 1), AppVersion("1.2.3-beta1"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Beta, 1), AppVersion("1.2.3-beta01"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Alpha, 0), AppVersion("1.2.3-alpha"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Alpha, 1), AppVersion("1.2.3-alpha1"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Alpha, 1), AppVersion("1.2.3-alpha01"))
    }
    @Test
    fun stringParsingInvalid() {
        Assert.assertNull(AppVersion(""))
        Assert.assertNull(AppVersion("1.2"))
        Assert.assertNull(AppVersion("1.2.3-"))
        Assert.assertNull(AppVersion("1.2.3-1"))
        Assert.assertNull(AppVersion("1.2.3-01"))
        Assert.assertNull(AppVersion("1.2.3-unknown"))
        Assert.assertNull(AppVersion("1.2.3-unknown1"))
        Assert.assertNull(AppVersion("1.2.3-unknown01"))
    }
    @Test
    fun stringFindingValid() {
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Stable, 0), AppVersion.findIn("version 1.2.3 "))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Stable, 0), AppVersion.findIn("version 1.2.3-1"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Stable, 0), AppVersion.findIn("version 1.2.3 -unknown"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Stable, 0), AppVersion.findIn("version 1.2.3 -unknown1"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Stable, 0), AppVersion.findIn("version 1.2.3 -unknown01"))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Beta, 0), AppVersion.findIn("version 1.2.3-beta "))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Beta, 1), AppVersion.findIn("version 1.2.3-beta1 "))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Beta, 1), AppVersion.findIn("version 1.2.3-beta01 "))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Alpha, 0), AppVersion.findIn("version 1.2.3-alpha "))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Alpha, 1), AppVersion.findIn("version 1.2.3-alpha1 "))
        Assert.assertEquals(AppVersion(1, 2, 3, AppVersion.Type.Alpha, 1), AppVersion.findIn("version 1.2.3-alpha01 "))
    }
    @Test
    fun stringFindingInvalid() {
        Assert.assertNull(AppVersion.findIn(""))
        Assert.assertNull(AppVersion.findIn("version 1.2 "))
        Assert.assertNull(AppVersion.findIn("version 1.2.3-unknown "))
    }
    @Test
    fun stringFormatting() {
        Assert.assertEquals("1.2.3", AppVersion(1, 2, 3, AppVersion.Type.Stable, 0).toString())
        Assert.assertEquals("1.2.3-beta00", AppVersion(1, 2, 3, AppVersion.Type.Beta, 0).toString())
        Assert.assertEquals("1.2.3-beta01", AppVersion(1, 2, 3, AppVersion.Type.Beta, 1).toString())
        Assert.assertEquals("1.2.3-alpha00", AppVersion(1, 2, 3, AppVersion.Type.Alpha, 0).toString())
        Assert.assertEquals("1.2.3-alpha01", AppVersion(1, 2, 3, AppVersion.Type.Alpha, 1).toString())
    }
    @Test
    fun comparison() {
        val list = listOf(
            AppVersion(1, 2, 3, AppVersion.Type.Alpha, 0),
            AppVersion(1, 2, 3, AppVersion.Type.Alpha, 1),
            AppVersion(1, 2, 3, AppVersion.Type.Beta, 0),
            AppVersion(1, 2, 3, AppVersion.Type.Beta, 1),
            AppVersion(1, 2, 3, AppVersion.Type.Stable, 0),
            AppVersion(1, 2, 4, AppVersion.Type.Stable, 0),
            AppVersion(1, 3, 3, AppVersion.Type.Stable, 0),
            AppVersion(2, 2, 3, AppVersion.Type.Stable, 0),
        )
        for (i in list.indices) {
            for (j in list.indices) {
                if (i < j) {
                    assert(list[i] < list[j])
                    assert(list[j] > list[i])
                    assert(list[i] <= list[j])
                    assert(list[j] >= list[i])
                } else if (i == j) {
                    assert(list[i] == list[j])
                    assert(list[j] == list[i])
                    assert(list[i] <= list[j])
                    assert(list[j] <= list[i])
                    assert(list[i] >= list[j])
                    assert(list[j] >= list[i])
                } else {
                    assert(list[i] > list[j])
                    assert(list[j] < list[i])
                    assert(list[i] >= list[j])
                    assert(list[j] <= list[i])
                }
            }
        }
    }
}