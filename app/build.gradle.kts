import com.android.build.api.dsl.ApplicationBuildType

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.compose.compiler)
    alias(libs.plugins.kotlinx.serialization)
    alias(libs.plugins.dokka)
}

android {
    namespace = "net.novagamestudios.kaffeekasse"
    compileSdk = 34
    buildToolsVersion = "34.0.0"

    defaultConfig {
        applicationId = "net.novagamestudios.kaffeekasse"
        minSdk = 26
        targetSdk = 34
        versionCode = 1
        versionName = "1.2.4"

        // Check that the version in environment matches the version in build.gradle.kts
        System.getenv()["APP_VERSION"]?.let { versionFromEnv ->
            require(versionFromEnv == versionName) {
                "Version in environment ($versionFromEnv) does not match version in build.gradle.kts ($versionName)"
            }
            println("Version matches environment: $versionName")
        }

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    buildTypes {
        fun ApplicationBuildType.debugCredentials(
            portalUsername: Any?,
            portalPassword: Any?,
            kaffeekasseDeviceId: Any?,
            kaffeekasseApiKey: Any?
        ) {
            fun Any?.toStringField() = this?.let { "\"$it\"" } ?: "null"
            buildConfigField("String", "I11_PORTAL_DEBUG_USERNAME", portalUsername.toStringField())
            buildConfigField("String", "I11_PORTAL_DEBUG_PASSWORD", portalPassword.toStringField())
            buildConfigField("String", "I11_KAFFEEKASSE_DEBUG_DEVICEID", kaffeekasseDeviceId.toStringField())
            buildConfigField("String", "I11_KAFFEEKASSE_DEBUG_APIKEY", kaffeekasseApiKey.toStringField())
        }

        fun ApplicationBuildType.releaseConfig() {
//            isMinifyEnabled = true
//            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            debugCredentials(null, null, null, null)
        }

        fun ApplicationBuildType.debugConfig() {
            proguardFiles(
                "proguard-rules.pro"
            )
            com.android.build.gradle.internal.cxx.configure.gradleLocalProperties(rootDir).let { properties ->
                debugCredentials(
                    properties["i11.portal.debug.username"],
                    properties["i11.portal.debug.password"],
                    properties["i11.kaffeekasse.debug.deviceid"],
                    properties["i11.kaffeekasse.debug.apikey"]
                )
            }
        }

        release {
            releaseConfig()
        }
        create("releasePreview") {
            releaseConfig()
            applicationIdSuffix = ".debug"
            signingConfig = signingConfigs.getByName("debug")
        }
        debug {
            debugConfig()
            applicationIdSuffix = ".debug"
            signingConfig = signingConfigs.getByName("debug")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_19
        targetCompatibility = JavaVersion.VERSION_19
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
            excludes += "META-INF/DEPENDENCIES"
            excludes += "mozilla/*"
        }
    }
}

kotlin {
    compilerOptions {
        freeCompilerArgs.addAll(
            "-Xcontext-receivers",
            "-opt-in=androidx.compose.ui.ExperimentalComposeUiApi",
            "-opt-in=androidx.compose.foundation.ExperimentalFoundationApi",
            "-opt-in=androidx.compose.material3.ExperimentalMaterial3Api",
        )
    }
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.androidx.credentials)
    implementation(libs.androidx.datastore)
    implementation(libs.androidx.activity.compose)

    implementation(libs.compose.animation)
    implementation(libs.compose.foundation)
    implementation(libs.compose.material.icons.extended)
    implementation(libs.compose.material3)
    implementation(libs.compose.runtime)
    implementation(libs.compose.ui)
    implementation(libs.compose.ui.text.google.fonts)
    implementation(libs.compose.grid)

    implementation(libs.kotlinx.datetime)
    implementation(libs.kotlinx.serialization.json)

    implementation(libs.ktor.client.core)
    implementation(libs.ktor.client.okhttp)
    implementation(libs.ktor.client.contentnegotiation)
    implementation(libs.ktor.serialization.kotlinx.json)

    implementation(libs.skrapeit)
    compileOnly(libs.skrapeit.ktor)

    implementation(libs.vico.core)
    implementation(libs.vico.compose)
    implementation(libs.vico.compose.m3)

    implementation(libs.voyager.navigator)
    implementation(libs.voyager.screenmodel)
    implementation(libs.voyager.bottomsheetnavigator)
    implementation(libs.voyager.tabnavigator)
    implementation(libs.voyager.transitions)

    implementation(libs.coil)
    implementation(libs.coil.compose)

    implementation(libs.acra.mail)
    implementation(libs.acra.limiter)

    implementation(libs.commonutils.core)
    implementation(libs.commonutils.compose)
    implementation(libs.commonutils.voyager)

    dokkaPlugin(libs.dokka.android)

    testImplementation(libs.junit4)
    androidTestImplementation(libs.compose.ui.test.junit4)
    debugImplementation(libs.compose.ui.test.manifest)
}
